import { shallowMount } from '@vue/test-utils'
import view from '../../src/js/views/users-list'

describe('User\'s list', () => {
  let propsDefault

  before(function(){
    propsDefault = {
      labels: {
        authorizedEmails : ""
      },
      emails: ["one_email@gmail.com", "another_email@gmail.com"]
    }
  })
  
  it('shows a list of emails', function() {
    let wrapper = shallowMount(view, {propsData: propsDefault})

    expect( wrapper.findAll(".email").length ).to.eq(2)
  });
  
  it('handles list elements removes', function() {
    window.confirm = () => true
    const clickSpy = sinon.stub()
    const methods = { handleClick: clickSpy }
    let wrapper = shallowMount(view, { propsData: propsDefault, methods })

    wrapper.find(".remove-user").trigger('click');

    expect(clickSpy).to.be.calledOnce;
  });
  
});
