import { APIClient } from "../../../src/js/infrastructure/api_client"
import { Solicitude } from "../../../src/js/components/solicitude"
import { Bus } from "../../../src/js/bus"

describe('Solicitude', () => {
  var confirmation
  var busPublication
  var apiClient

  beforeEach(() => {
    confirmation = sinon.stub(window, 'confirm').returns(true)
    busPublication = sinon.stub(Bus, "publish")
    apiClient = sinon.stub(APIClient, 'hit')
  })

  afterEach(() => {
    confirmation.restore()
    busPublication.restore()
    apiClient.restore()
  })

  it('deletes a solicitude', () => {
    const solicitude =  createSolicitude()

    solicitude.deleteSolicitude()

    expect(busPublication).to.have.been.calledWith('delete.solicitude')
  })

  it('shows a confirmation when deletes a solicitude', () => {
    const solicitude = createSolicitude()

    solicitude.deleteSolicitude()

    expect(confirmation).to.have.been.calledWith("confirmRemoveSolicitudeWithoutSubject")
  })

  it('shows a confirmation for subjects when deletes a solicitude with subjects', () => {
    const subjects = ['aSubject']
    const solicitude = createSolicitudeWith(subjects)

    solicitude.deleteSolicitude()

    expect(confirmation).to.have.been.calledWith(`confirmRemoveSolicitudeWithSubject ${subjects.length}`)
  })

  function createSolicitudeWith(subjects) {
    const solicitude = new Solicitude()
    solicitude.translate({ key: "confirmRemoveSolicitudeWithSubject", label: "confirmRemoveSolicitudeWithSubject %s" })
    solicitude.data.values.subjects = subjects

    return solicitude
  }

  function createSolicitude() {
    const solicitude = new Solicitude()
    solicitude.translate({ key: "confirmRemoveSolicitudeWithoutSubject", label: "confirmRemoveSolicitudeWithoutSubject" })

    return solicitude
  }
})
