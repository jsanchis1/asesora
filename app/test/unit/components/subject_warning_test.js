import  { Subjects }  from "../../../src/js/components/subjects"
import { APIClient } from "../../../src/js/infrastructure/api_client"

describe('Subject', () => {
  var apiClient

  beforeEach(() => {
    apiClient = sinon.stub(APIClient, 'hit')
  })

  afterEach(() => {
    apiClient.restore()
  })

  it('is justifiable when required values are correctly filled', () => {
    const subject = createJustificableSubject()

    expect(subject.data.warningSubject).be.eq(false)
  })

  function createJustificableSubject(){
    const subject = new Subjects()

    const assignations = {
      selectedTopics: ['2'],
      proposals: ['101']
    }

    for (const key in assignations) {
      subject.data.values[key] = assignations[key]
    }

    subject.warningRequired()
    expect(subject.data.warningSubject).be.eq(false)
    return subject
  }
})
