import { shallowMount } from '@vue/test-utils'
import view from '../../src/js/views/login.vue'

describe('Login', () => {
  it('shows error when api responds error', () => {
    const login_error_message = 'text for error'
    const api_error_message = 'api error message'

    const wrapper = shallowMount(view, {
      mocks: {
        labels: {"unauthorizedEmail": login_error_message},
        values: {"error": api_error_message}
      }
    })

    expect(wrapper.find("#login-error").text()).to.eq(login_error_message)
  })

  it('don\'t shows error when api responds no errors', () => {
    const login_error_message = 'text for error'
    const api_error_message = ''

    const wrapper = shallowMount(view, {
      mocks: {
        labels: {"unauthorizedEmail": login_error_message},
        values: {"error": api_error_message}
      }
    })

    expect(wrapper.find("#login-error").exists()).to.eq(false)
  })
})
