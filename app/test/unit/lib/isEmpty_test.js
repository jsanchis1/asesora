import isEmpty from '../../../src/js/lib/isEmpty'

describe('isEmpty', () => {
  it('knows when a string is filled', () => {
    const filledString = 'filled'

    const result = isEmpty(filledString)

    expect(result).to.be.false
  })

  it('knows when a string is empty', () => {

    const result = isEmpty('')

    expect(result).to.be.true
  })

  it('knows when a value is not defined', () => {

    const result = isEmpty(undefined)

    expect(result).to.be.true
  })

  it('knows when a list is filled', () => {
    const filledList = ['filled']

    const result = isEmpty(filledList)

    expect(result).to.be.false
  })

  it('knows when a list is empty', () => {

    const result = isEmpty([])

    expect(result).to.be.true
  })

  it('knows when a dictionary is filled', () => {
    const filledDictionary = { 'some': 'thing' }

    const result = isEmpty(filledDictionary)

    expect(result).to.be.false
  })

  it('knows when a dictionary is empty', () => {

    const result = isEmpty({})

    expect(result).to.be.true
  })

  it('knows when a value is null', () => {

    const result = isEmpty(null)

    expect(result).to.be.true
  })
})
