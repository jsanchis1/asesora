import Component from '../infrastructure/component'
import Models from '../models/models'
import view from '../views/solicitude/solicitude'
import { Bus } from '../bus'
import { isValidCif, isValidEmail } from '../lib/validations'
import { askTranslationsFor } from "../infrastructure/translatable"
import userSession from '../infrastructure/user_session'
import Validations from '../services/validations'
import CompaniesService from '../services/companies'
import ApplicantsService from '../services/applicants'
import MaxEmployees from '../lib/MaxEmployees'
import MaxFileSize from '../lib/MaxFileSize'
import isEmpty from '../lib/isEmpty'
import Dictionary from './dictionary'
import Catalogs from './catalogs'

export class Solicitude extends Component {
  constructor() {
    super()

    new Validations()
    new CompaniesService()
    new ApplicantsService()
    new Catalogs()

    this.defaults = JSON.parse(JSON.stringify(this.data))
    this.initialValues = this.data.cloneValues()
    this.validateContact()
  }

  subscribe() {
    Bus.subscribe("solicitude.created", this.initComponent.bind(this))
    Bus.subscribe("solicitude.route.changed", this.initComponent.bind(this))
    Bus.subscribe("solicitude.destroyed", this.clearSubscriptions.bind(this))
    Bus.subscribe("checked.subject.justified", this.setWarningSubject.bind(this))
    Bus.subscribe("checked.solicitude.justified", this.setWarningSolicitude.bind(this))
    Bus.subscribe('deleted.attachment', this.deletedAttachment.bind(this))
    Bus.subscribe('fill.catalogs', this.fillCatalogs.bind(this))
    Bus.subscribe('checked.email.to.share', this.gotAuthorizedEmail.bind(this))
    Bus.subscribe('shared.subject', this.sharedSubject.bind(this))
    Bus.subscribe('show.solicitude', this.showSolicitude.bind(this))
    Bus.subscribe('show.shared.solicitude', this.showSharedSolicitude.bind(this))
  }

  collectSubscriptions() {
    this.addSubscription("changed.sharing.info", this.validateShareEmail.bind(this))
    this.addSubscription("clicked.share.subject", this.shareSubject.bind(this))
    this.addSubscription("ask.unsaved.changes", this.hasChanges.bind(this))
    this.addSubscription("clicked.stored.subject.list", this.editStoredSubject.bind(this))
    this.addSubscription("clicked.modify.subject", this.updateStoredSubject.bind(this))
    this.addSubscription("clicked.remove.subject.closed.status", this.removeSubjectClosedStatus.bind(this))
    this.addSubscription("clicked.modify.counseling", this.modifyCounseling.bind(this))
    this.addSubscription("clicked.subject.list", this.editSubject.bind(this))
    this.addSubscription('clicked.close.subject.edition', this.finishSubjectEdition.bind(this))
    this.addSubscription("clicked.delete.solicitude", this.deleteSolicitude.bind(this))
    this.addSubscription('clicked.company', this.fillCompany.bind(this))
    this.addSubscription('clicked.applicant', this.fillApplicant.bind(this))
    this.addSubscription('clicked.add.subject', this.addSubject.bind(this))
    this.addSubscription('subject.create.clicked', this.createSubjectNew.bind(this))
    this.addSubscription('clicked.delete.subject', this.deleteSubject.bind(this))
    this.addSubscription('clicked.delete.unsaved.subject', this.deleteUnsavedSubject.bind(this))
    this.addSubscription('clicked.close.counseling', this.closeCounseling.bind(this))
    this.addSubscription('clicked.close.unsaved.counseling', this.closeUnsavedCounseling.bind(this))
    this.addSubscription('clicked.discard.company.button', this.removeSubjectClosedStatus.bind(this))
    this.addSubscription('clicked.discard.button', this.discardAnimation.bind(this))
    this.addSubscription("got.translation.for.solicitude", this.translate.bind(this))
    this.addSubscription("got.company-matches", this.populateSuggestedCompanies.bind(this))
    this.addSubscription("got.solicitude", this.updateModel.bind(this))
    this.addSubscription("got.applicant.matches", this.populateSuggestedApplicants.bind(this))
    this.addSubscription('changed.ccaa', this.warningRequired.bind(this))
    this.addSubscription('changed.phone', this.runValidations.bind(this))
    this.addSubscription('changed.phone', this.setValidPhone.bind(this))
    this.addSubscription("changed.email", this.runValidations.bind(this))
    this.addSubscription("changed.email", this.setValidEmail.bind(this))
    this.addSubscription('changed.text', this.setButtonStatus.bind(this))
    this.addSubscription('changed.date', this.warningRequired.bind(this))
    this.addSubscription('changed.date', this.setButtonStatus.bind(this))
    this.addSubscription('changed.time', this.warningRequired.bind(this))
    this.addSubscription('changed.time', this.setButtonStatus.bind(this))
    this.addSubscription('changed.source', this.setButtonStatus.bind(this))
    this.addSubscription('changed.organization', this.setButtonStatus.bind(this))
    this.addSubscription('changed.source', this.warningRequired.bind(this))
    this.addSubscription('changed.subject', this.setModifySubjectButtonStatus.bind(this))
    this.addSubscription('changed.subject', this.warningSubjectRequired.bind(this))
    this.addSubscription("created.solicitude", this.createdSolicitude.bind(this))
    this.addSubscription('subject.created', this.subjectCreated.bind(this))
    this.addSubscription("updated.solicitude", this.updatedSolicitude.bind(this))
    this.addSubscription("subject.updated", this.subjectUpdated.bind(this))
    this.addSubscription('submit.solicitude', this.submit.bind(this))
    this.addSubscription('edit.solicitude', this.update.bind(this))
    this.addSubscription("deleted.solicitude", this.deletedSolicitude.bind(this))
    this.addSubscription("deleted.subject", this.deletedSubject.bind(this))
    this.addSubscription("verified.company.duplicate", this.showDuplicate.bind(this))
    this.addSubscription("subject.closed", this.subjectClosed.bind(this))
    this.addSubscription('fullfilled.solicitude', this.transitToList.bind(this))
    this.addSubscription('fullfilled.solicitude.to.add.subject', this.transitToAddSubject.bind(this))
    this.addSubscription('attached.files', this.attachFiles.bind(this))
    this.addSubscription('changed.ccaa', this.changedApplicantField.bind(this))
    this.addSubscription('changed.applicant.fields', this.changedApplicantField.bind(this))
    this.addSubscription('changed.company.name', this.changedCompanyName.bind(this))
    this.addSubscription('changed.company.cif', this.changedCompanyCif.bind(this))
    this.addSubscription('changed.company.cp', this.changedCompanyCp.bind(this))
    this.addSubscription('changed.company.employees', this.changedCompanyEmployees.bind(this))
    this.addSubscription('changed.company.cnae', this.changedCompanyCnae.bind(this))
    this.addSubscription('delete.stored.attachment', this.deleteStoredAttachment.bind(this))
    this.addSubscription('clicked.discard.close.subject', this.discardCloseSubject.bind(this))
  }

  discardCloseSubject() {
    this.data.values.reason = ''
    this.data.values.comments = ''
  }

  fillCatalogs(payload) {
    for (let [labelKey, valueKey] of Object.entries(payload)) {
      this.data.set(labelKey, valueKey)
    }
  }

  showSolicitude({ detail }){
    this.goTo(`/#/show-solicitude/${ detail }`)
  }

  showSharedSolicitude(id){
    this.goToAndReload(`/#/show-solicitude/${ id }`)
  }

  validateShareEmail() {
    if(isValidEmail(this.data.values.shareEmail) && !isEmpty(this.data.values.shareText) && this.isOwner() ){
      this.askIsAuthorizedEmailToShare()
    }
    else {
      this.data.isValidShareEmail = false
    }
  }

  isOwner(){
    const domain = this.retrieveDomain()

    return domain == this.data.labels.ownerDomain
  }

  retrieveDomain() {
    const email = this.data.values.shareEmail
    const domain = email.substring(email.lastIndexOf("@") +1)
    return domain
  }

 sharedSubject(payload){
   alert(this.data.labels.subjectSharedMessage)
   this.refreshModifiedSubject(payload)
   this.data.showSubjectEditModal = false
 }

  shareSubject() {
    const data = {
      'destination_email': this.data.values.shareEmail,
      'text': this.data.values.shareText,
      'origin_email': userSession.email(),
      'subject_id': this.data.values.subjectId
    }
    data.text = this.prepareTextToShare(data)

    Bus.publish('share.subject', data)

    this.data.showShareEmail = false
    this.cleanSharingData()
  }

  prepareTextToShare(data){
    let text = this.data.labels.textToShare.replace('%origin_email', data.origin_email).replace('%text', data.text)
    return text
  }

  askIsAuthorizedEmailToShare() {
    Bus.publish('check.email.to.share', this.data.values.shareEmail)
  }

  gotAuthorizedEmail(payload) {
    this.data.authorizedToShare = payload
    this.data.showShareEmail = true
    this.setEmailValidForShare()
  }

  setEmailValidForShare(){
    this.data.isValidShareEmail = false
    if(isValidEmail(this.data.values.shareEmail) && !isEmpty(this.data.values.shareText) && this.data.authorizedToShare) {
      this.data.isValidShareEmail = true
    }
  }

  cleanSharingData(){
    this.data.setValues('shareText', '')
    this.data.setValues('shareEmail', '')
  }

  searchForApplicants() {
    this.data.suggestedApplicants = []
    let criteria = {
      'applicantName': this.data.values.applicantName,
      'applicantSurname': this.data.values.applicantSurname,
      'applicantPhonenumber': this.data.values.applicantPhonenumber,
      'applicantEmail': this.data.values.applicantEmail,
      'applicantCcaa': this.data.values.applicantCcaa,
      'user_id': userSession.email()
    }
    if (this.evaluateCriterion(criteria)) {
      Bus.publish('get.applicant.matches', criteria)
    }
  }

  evaluateCriterion(criteria) {
    for (let field in criteria) {
      if (this.checkMinimunCriteria(criteria[field])) {
        return true
      }
    }
    return false
  }

  checkMinimunCriteria(field) {
    let minimunLength = 3
    return (field.length >= minimunLength)
  }

  changedApplicantField() {
    this.runValidations()
    this.searchForApplicants()
  }

  changedCompanyName() {
    this.searchCompanies()
    this.changedCompany()
  }

  changedCompanyCp() {
    this.changedCompany()
  }

  changedCompanyCnae() {
    this.searchCompanies()
    this.changedCompany()
  }

  changedCompanyCif() {
    this.ensureCif()
    this.verifyDuplicatedCif()
    this.changedCompany()
  }

  changedCompanyEmployees() {
    this.validateEmployees()
    this.changedCompany()
  }

  changedCompany(){
    this.checkCif()
    this.checkName()
    this.validateEmployees()
    this.setButtonStatus()
    this.warningRequired()
  }

  checkCif(){
    this.data.isValidCompanyIdentity = this.validateCif() || this.isCifEmpty()
  }

  checkName(){
    const invalidCompanyName = this.isCifFilled() && this.isNameEmpty()
    this.data.isValidCompanyName = !invalidCompanyName
  }

  isCifEmpty() {
    return isEmpty(this.data.values.companyCif)
  }

  isCifFilled() {
    return !isEmpty(this.data.values.companyCif)
  }

  validateCif() {
    return isValidCif(this.data.values.companyCif)
  }

  verifyDuplicatedCif() {
    if (this.data.isValidCif) {
      let cif = this.data.values.companyCif
      Bus.publish('verify.company.duplicate', cif)
    }
  }

  ensureCif() {
    if (this.isCifEmpty()) {
      this.data.isValidCif = true
      return
    }
    this.data.isValidCif = this.validateCif()
  }

  validateEmployees(){
    const numberEmployees = this.data.values.companyEmployees
    this.data.isValidEmployees = MaxEmployees.isBelow(numberEmployees)
  }

  setButtonStatus() {
    this.data.isSubmittable = false
    if (this.textIsFilled() && this.isValidContact() && this.isValidCompany()) {
      this.data.isSubmittable = true
    }
    this.showRequiredIncomplete()
  }

  initComponent(idSolicitude) {
    this.registerSubscriptions()
    askTranslationsFor({ labels: this.data.labels, scope: "solicitude" })
    this.initModel()
    if (idSolicitude) this.edit(idSolicitude)
  }

  saveCompanyInfo() {
    const { companyName, companyCif, companyEmployees, companyCnae, companyId, companyCp } = this.data.values
    Bus.publish('update.company', { companyName, companyCif, companyEmployees, companyCnae, companyId, companyCp })
    this.initialValues['companyName'] = companyName
    this.initialValues['companyEmployees'] = companyEmployees
    this.initialValues['companyCnae'] = companyCnae
    this.initialValues['companyId'] = companyId
    this.initialValues['companyCp'] = companyCp

    this.data.saveCompany = false
    this.data.suggestedCompanies = []
  }

  editStoredSubject(detail) {
    let reason = detail.reason

    this.data.editionSubject = detail.subjectId
    let valuesProposals = []
    for (const proposal of detail.proposal) {
      valuesProposals.push({ value: proposal, text: proposal })
    }

    let valuesTopics = []
    for (const topic of detail.topics) {
      valuesTopics.push({ value: {id: topic.id,
                                  name: topic.name},
                          text: topic.name })
    }

    let project = ''
    if (detail.project) {
      project = {value: detail.project.value, text: detail.project.text }
    }

    this.data.setValues('proposals', valuesProposals)
    this.data.setValues('description', detail.description)
    this.data.setValues('analysis', detail.analysis)
    this.data.setValues('subjectId', detail.subjectId)
    this.data.setValues('selectedTopics', valuesTopics)
    this.data.setValues('reason', reason)
    this.data.setValues('comments', detail.comments)
    this.data.setValues('closed', detail.closed)
    this.data.setValues('created', detail.created)
    this.data.setValues('createdTime', detail.createdTime)
    this.data.setValues('project', project)
    this.data.setValues('numeration', detail.numeration)

    this.resetAttachedFilesList()
    this.warningSubjectRequired()
    this.openEditSubjectModal()
  }

  createSubjectNew(detail){
    if(this.data.isEditionMode) {
      this.createSubject(detail)
    } else {
      this.storeSubject(detail)
    }
    this.setButtonStatus()
  }

  storeSubject(data) {
    const subject = Dictionary.subject(data)
    subject.subjectId = Date.now()

    this.data.values.subjects.push(subject)
    this.finishSubjectEdition()
  }

  createSubject(data) {
    const subject = Dictionary.subject(data)
    Bus.publish('create.subject', subject)
  }

  updateStoredSubject(detail) {
    const updatedSubject = Dictionary.subject(detail)

    for (let subject of this.data.values.subjects) {
      if (subject.subjectId == updatedSubject.subjectId) {
        subject.proposal = updatedSubject.proposal
        subject.description = updatedSubject.description
        subject.analysis = updatedSubject.analysis
        subject.topics = updatedSubject.topics
        subject.comments = updatedSubject.comments
        subject.reason = updatedSubject.reason
        subject.closed = updatedSubject.closed
        subject.created = updatedSubject.created
        subject.createdTime = updatedSubject.createdTime
        subject.project = updatedSubject.project
        subject.numeration = updatedSubject.numeration
        subject.files = subject.files.concat(updatedSubject.files)
      }
    }

    this.finishSubjectEdition()
  }

  closeUnsavedCounseling(subject) {
    if ( !subject.detail.subjectId ){
      this.storeSubject(subject.detail)
    } else {
      this.updateStoredSubject(subject.detail)
    }
  }

  deleteStoredAttachment(payload){
    for (let subject of this.data.values.subjects) {
      if(subject.subjectId == payload.subjectId){
        subject.files = this.removeStoredAttachmentFromSubject(subject.files, payload.filename)
      }
    }
  }

  modifyCounseling(payload) {
    const subject = Dictionary.subject(payload.detail)

    Bus.publish('update.subject', subject)

    this.data.editionSubject = false
    this.finishSubjectEdition()
  }

  closeCounseling(payload) {
    const subject = Dictionary.subject(payload)
    Bus.publish('close.subject', subject)
  }

  subjectCreated(payload) {
    this.clearValuesSubjectSolicitude()
    this.data.values.subjects.unshift(payload)
    this.finishSubjectEdition()
  }


  edit(id) {
    this.data.values.id = id
    this.data.isEditionMode = true
    this.data.isCreationMode = false
    Bus.publish('get.solicitude', { id })
  }

  initModel() {
    const keys = Object.keys(this.data)
    keys.forEach( key => {
      if (!key.match("labels|values|Catalog|user_id") && typeof(this.data[key]) != 'function') {
        this.data[key] = this.defaults[key]
      }
    })
    Object.assign(this.data.values, this.defaults.values)
    this.data.values.subjects = []
    this.initialValues = this.data.cloneValues()
    this.warningRequired()
    this.warningSubjectRequired()
  }

  updateModel(payload) {
    const owner = payload.data.user.user_id
    const solicitudeId = payload.data.creation_moment
    this.checkAutorizedToEdit(owner, solicitudeId)

    const dictionarySolicitude = Dictionary.solicitude(payload)
    Dictionary.charge(dictionarySolicitude, this.data)

    if (!isEmpty(payload.data.company)) {
      const dictionaryCompany = Dictionary.company(payload)
      Dictionary.charge(dictionaryCompany, this.data)
      this.setCompanyToEdit()
      Bus.publish('get.company.count', payload.data.company_cif)
    }
    this.warningRequired()
    this.data.isValidContact = true
    this.data.solicitudeNumeration = payload.data.numeration
    this.initialValues = this.data.cloneValues()
    this.data.requiredIncomplete = false
    this.resetAttachedFilesList()
  }

  setCompanyToEdit() {
    this.data.showEditCompanyButton = true
  }

  resetAttachedFilesList(){
    this.data.setValues('files', [])
  }

  deletedAttachment(attachment){
    this.data.values.subjects.forEach((subject)=>{
      if(subject.id == attachment.subject_id){
        subject.files = this.removeAttachmentFromSubject(subject.files, attachment.id)
      }
    })
  }

  attachFiles(files){
    for (let file of files) {
      if (this.isBelowMaxSize(file) ){
        this.readFile(file)
      }
      else {
        const message = this.data.labels.maxSized.replace('%file', file.name)
        alert(message)
      }
    }
  }

  isBelowMaxSize(file){
    return MaxFileSize.isBelow(file)
  }

  readFile(file) {
    const reader = new FileReader()
    reader.onload = ()=>{this.addFileToModel(reader, file.name)}
    reader.readAsDataURL(file)
  }

  addFileToModel(result, name){
    const rawFile = result.result.split(',')[1]

    const file = { file: rawFile, name: name }
    this.data.values.files.push(file)
  }

  removeAttachmentFromSubject(files, attachmentId){
    return files.filter(file => file.id != attachmentId)
  }

  removeStoredAttachmentFromSubject(files, filename){
    return files.filter(file => file.name != filename)
  }

  checkAutorizedToEdit(owner, solicitudeId) {
    if (!userSession.isCurrentUser(owner)) {
      const path = '#show-solicitude/' + solicitudeId

      this.goToAndReload(path)
    }
  }

  addSubject() {
    this.clearValuesSubjectSolicitude()
    this.warningSubjectRequired()
    this.openEditSubjectModal()
  }

  clearValuesSubjectSolicitude(){
    this.data.setValues('proposals', [])
    this.data.setValues('description', "")
    this.data.setValues('analysis', "")
    this.data.setValues('subjectId', "")
    this.data.setValues('selectedTopics', [])
    this.data.setValues('reason', "")
    this.data.setValues('comments', "")
    this.data.setValues('closed', null)
    this.data.setValues('created', "")
    this.data.setValues('createdTime', "")
    this.data.setValues('project', {})
    this.data.setValues('shared_destination', "")
    this.data.editionSubject = ''
    this.resetAttachedFilesList()
  }

  openEditSubjectModal(){
    this.data.showSubjectEditModal = true
  }

  finishSubjectEdition(){
    this.clearValuesSubjectSolicitude()
    this.setButtonStatus()
    this.data.showSubjectEditModal = false
    setTimeout(this.scrollToEnd, 300)
  }

  scrollToEnd(){
    let saveButton = document.getElementById("submit")
    saveButton.scrollIntoView({block: "start", behavior: "smooth"})
  }

  editSubject(event) {
    let reason = event.detail.reason

    if (isEmpty(reason)) {
      reason = this.data.reasonsCatalog[0]
    }

    this.data.editionSubject = event.detail.id
    let valuesProposals = []
    for (const proposal of event.detail.proposal) {
      valuesProposals.push({ value: proposal, text: proposal })
    }

    let valuesTopics = []
    for (const topic of event.detail.topics) {
      valuesTopics.push({ value: {id: topic.id,
                              name: topic.name},
                          text: topic.name})
    }

    let project = ''
    if (event.detail.project) {
      project = {value: event.detail.project.value, text: event.detail.project.text }
    }

    this.data.setValues('proposals', valuesProposals)
    this.data.setValues('description', event.detail.description)
    this.data.setValues('analysis', event.detail.analysis)
    this.data.setValues('subjectId', event.detail.id)
    this.data.setValues('selectedTopics', valuesTopics)
    this.data.setValues('reason', reason)
    this.data.setValues('comments', event.detail.comments)
    this.data.setValues('closed', event.detail.closed)
    this.data.setValues('created', event.detail.created)
    this.data.setValues('createdTime', event.detail.createdTime)
    this.data.setValues('project', project)
    this.data.setValues('numeration', event.detail.numeration)
    this.data.setValues('shared_destination', event.detail.shared_destination)

    this.resetAttachedFilesList()
    this.warningSubjectRequired()
    this.openEditSubjectModal()
  }

  setModifySubjectButtonStatus() {
    this.data.isSubmittable = false

    let analysis = !isEmpty(this.data.values.analysis)
    let createdAt = !isEmpty(this.data.values.created)
    let topics = !isEmpty(this.data.values.selectedTopics)
    if (createdAt && (analysis || topics)) {
      this.data.isSubmittable = true
    }
  }

  subjectUpdated(payload) {
    this.data.showAlert = false
    this.refreshModifiedSubject(payload)
    this.data.isSubmittable = false
    this.showSubjectModified()
    this.data.editionSubject = false
    this.finishSubjectEdition()
  }

  subjectClosed(payload) {
    this.data.showAlert = false
    this.refreshModifiedSubject(payload)
    this.data.isSubmittable = false
    this.showSubjectModified()
    this.data.editionSubject = false
    this.finishSubjectEdition()
  }

  showSubjectModified() {
    this.data.modifiedSubjectId = this.data.editionSubject
  }

  refreshModifiedSubject(updatedSubject) {
    for (let subject of this.data.values.subjects) {
      if (subject.id == updatedSubject.id) {
        subject.proposal = updatedSubject.proposal
        subject.description = updatedSubject.description
        subject.analysis = updatedSubject.analysis
        subject.topics = updatedSubject.topics
        subject.comments = updatedSubject.comments
        subject.reason = updatedSubject.reason
        subject.closed = updatedSubject.closed
        subject.created = updatedSubject.created
        subject.createdTime = updatedSubject.createdTime
        subject.project = updatedSubject.project
        subject.numeration = updatedSubject.numeration
        subject.files = updatedSubject.files
        subject.shared_destination = updatedSubject.shared_destination
      }
    }
  }

  hasChanges() {
    this.data.hasUnsavedChanges = false

    if (!this.data.isSubmittable ){
      for (let key in this.data.values) {
        if (typeof this.data.values[key] === 'object') { continue }
        if (this.data.values[key] != this.initialValues[key]){
          this.data.hasUnsavedChanges = true
        }
      }
      if(!this.data.isEditionMode && this.data.values['subjects'].length > 0){
        this.data.hasUnsavedChanges = true
      }
    }
  }

  submit() {
    this.data.setValues("user_id", userSession.email())
    Bus.publish('create.solicitude', this.data.values)
  }

  update() {
    this.data.setValues("user_id", userSession.email())
    this.saveCompanyInfo()
    Bus.publish('update.solicitude', this.data.values)
    Bus.publish('update.applicant', this.data.values)
  }

  deleteSolicitude() {
    const numberOfSubjects = this.data.values.subjects.length
    let text = this.data.labels.confirmRemoveSolicitudeWithoutSubject
    if (numberOfSubjects > 0) { text = this.data.labels.confirmRemoveSolicitudeWithSubject.replace('%s', numberOfSubjects) }

    const notConfirmed = !window.confirm(text)
    if (notConfirmed) { return }

    Bus.publish('delete.solicitude', this.data.values.id)
  }

  removeSubjectClosedStatus(event) {
    const subjectId = event.detail.subjectId
    let text = this.data.labels.confirmRemoveSubjectClosedStatus

    const notConfirmed = !window.confirm(text)
    if (notConfirmed) { return }

    Bus.publish('remove.subject.closed.status', subjectId)
  }

  deleteUnsavedSubject(item) {
    const notConfirmed = !confirm(this.data.labels.deleteSubjectMessage)
    if (notConfirmed) { return }

    if(this.data.values.subjects.includes(item)) {
      this.data.values.subjects.pop(item)
    }
  }

  deleteSubject(id) {
    const notConfirmed = !confirm(this.data.labels.deleteSubjectMessage)
    if (notConfirmed) { return }

    Bus.publish('delete.subject', id)
  }

  updatedSolicitude(response) {
    this.data.showAlert = false
    if (isEmpty(response)) {
      this.data.errors = true
    } else {
      this.data.fullfilled = true
    }
  }

  deletedSolicitude() {
    this.data.showAlert = false
    let element = document.querySelector('#solicitude')
    element.classList.add('discardCard')
    window.setTimeout(function () {
      window.location = "/#/solicitudes-list"
    }, 1250)
  }

  deletedSubject(payload) {
    this.data.showAlert = false
    let id = payload.data.id
    let subjects = this.data.values.subjects
    this.data.values.subjects = subjects.filter(function (subject) {
      return subject.id != id
    })
  }

  searchCompanies() {
    let companyName = this.data.values.companyName
    let companyCnae = this.data.values.companyCnae
    let user_id = this.data.values.user_id

    this.data.suggestedCompanies = []
    if (this.hasRequiredLength()) {
      Bus.publish('get.company.matches', { companyName, companyCnae, user_id })
    }
  }

  hasRequiredLength() {
    let minNumberOfChars = 3
    if (this.data.values.companyName.length >= minNumberOfChars) {
      return true
    }
    return false
  }

  populateSuggestedCompanies(payload) {
    this.data.suggestedCompanies = payload.data
  }

  showDuplicate(payload) {
    let duplicatedCompany = []
    if (!this.isObjectEmpty(payload)) {
      duplicatedCompany.push(payload)
    }
    this.data.suggestedCompanies = duplicatedCompany
  }

  isObjectEmpty(object) {
    return isEmpty(object)
  }

  fillCompany(item) {
    this.data.setValues('companyName', item.detail.name)
    this.data.setValues('companyCif', item.detail.cif)
    this.data.setValues('companyEmployees', item.detail.employees)
    this.data.setValues('companyCnae', item.detail.cnae)
    this.data.setValues('companyCp', item.detail.cp)

    this.setValidCompany()
  }

  setValidCompany() {
    this.data.isValidCif = true;
    this.data.isValidCompanyIdentity = true;
    this.data.isSubmittable = true;
    this.data.showEditCompanyButton = true;
  }

  fillApplicant(item) {
    this.data.setValues('applicantName', item.detail.name)
    this.data.setValues('applicantSurname', item.detail.surname)
    this.data.setValues('applicantEmail', item.detail.email)
    this.data.setValues('applicantPhonenumber', item.detail.phonenumber)
    this.data.setValues('applicantCcaa', item.detail.ccaa)
    this.data.setValues('applicantId', item.detail.id)

    this.runValidations()
  }

  isNameEmpty() {
    return isEmpty(this.data.values.companyName)
  }

  isCifEmpty() {
    return isEmpty(this.data.values.companyCif)
  }

  discardAnimation() {
    let url = "/#/solicitudes-list"
    this.data.isSubmittable = true
    this.data.showAlert = false

    this.moveCardAnimation('discardCard')
    this.setTimeToRelocateUrl(url)
  }

  transitToList() {
    let url = "/#/solicitudes-list"
    this.initialValues = this.data.cloneValues()
    this.moveCardAnimation('submitCard')
    this.setTimeToRelocateUrl(url)
  }

  transitToAddSubject() {
    const url = `/#/subjects/${this.data.values.id}`
    this.initialValues = this.data.cloneValues()
    this.moveCardAnimation('submitCard')
    this.setTimeToRelocateUrl(url)
  }

  moveCardAnimation(cssClass) {
    let element = document.querySelector('#solicitude')
    element.classList.add(cssClass)
  }

  setTimeToRelocateUrl(url) {
    window.setTimeout(function () {
      window.location = url
    }, 1250)
  }

  createdSolicitude() {
    this.data.fullfilled = true
  }

  textIsFilled() {
    return !isEmpty(this.data.values.text)
  }

  isValidCompany() {
    return this.data.isValidCompanyIdentity && this.data.isValidCompanyName
  }

  showRequiredIncomplete(){
    this.data.requiredIncomplete = !this.data.isSubmittable
  }

  isValidContact() {
    return this.data.isValidContact
  }

  validateContact() {
    const email = this.data.values.applicantEmail
    const phone = this.data.values.applicantPhonenumber
    this.data.isValidContact = this.validEmail && this.validPhonenumber
    if (this.validEmail && isEmpty(phone)) {
      this.data.isValidContact = true
    }
    if (this.validPhonenumber && isEmpty(email)) {
      this.data.isValidContact = true
    }
  }

  runValidations() {
    this.validEmail = this.validateEmail()
    this.validPhonenumber = this.validatePhonenumber()

    this.validateContact()
    this.setButtonStatus()
  }

  validateEmail() {
    const email = this.data.values.applicantEmail
    if (isEmpty(email)) { return false }

    const EMAIL_PATTERN = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/
    return EMAIL_PATTERN.test(email)
  }

  validatePhonenumber() {
    const phone = this.data.values.applicantPhonenumber
    if (isEmpty(phone)) { return false }
    return (phone.length == 9)
  }

  setValidEmail(event) {
    this.validEmail = event.detail.valid
    this.validateContact()
    this.data.isValidEmail = this.validateEmail()
    if (isEmpty(this.data.values.applicantEmail)) {
      this.data.isValidEmail = true
    }
  }

  setValidPhone(event) {
    this.validPhonenumber = event.detail.valid
    this.validateContact()
    this.data.isValidPhone = this.validatePhonenumber()
    if (isEmpty(this.data.values.applicantPhonenumber)) {
      this.data.isValidPhone = true
    }
  }

  populateSuggestedApplicants(payload) {
    this.data.suggestedApplicants = payload.data
  }

  warningRequired() {
    this.data.warning = true
    this.validateEmployees()
    Bus.publish('check.solicitude.justified', this.data.values)
  }

  setWarningSolicitude(warning){
    this.data.warning = warning
  }

  setWarningSubject(warning){
    this.data.warningSubject = warning
  }

  warningSubjectRequired() {
    this.data.warningSubject = true
    Bus.publish('check.subject.justified', this.data.values)
  }

  model() {
    if(!this.data) { this.data = Models.newSolicitude() }
    return this.data
  }

  view() {
    return view
  }
}

const component = new Solicitude()

export default Object.assign({}, component.view, { data: component.model })
