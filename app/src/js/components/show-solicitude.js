import Component from '../infrastructure/component'
import Models from '../models/models'
import view from '../views/show-solicitude'
import userSession from '../infrastructure/user_session'
import { Bus, Channel } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"
import CheckOwnerInfoAvailable from '../lib/checkOwnerAvailable'
import MaxEmployees from "../lib/MaxEmployees"
import { dateToDDMMYYYY } from '../lib/formatters'

class ShowSolicitude extends Component {
  constructor(){
    super()
    const channel = Channel("show-solicitude")
    this.channel = channel
    this.channelSubscribe()
  }

  subscribe(){
    Bus.subscribe('got.translation.for.show-solicitude', this.translate.bind(this))
    Bus.subscribe("show-solicitude.created", this.initComponent.bind(this))
    Bus.subscribe("show-solicitude.destroyed", this.clearSubscriptions.bind(this)) 
    Bus.subscribe('show.shared.solicitude', this.showSharedSolicitude.bind(this))

  }

  channelSubscribe(){
    this.channel.subscribe('got.solicitude', this.updateModel.bind(this))
    this.channel.subscribe('deleted.subject', this.deletedSubject.bind(this))
    this.channel.subscribe('got.is.coordinator', this.gotIsCoordinator.bind(this))

  }

  collectSubscriptions() {
    this.addSubscription('solicitude.edit.clicked', this.loadSolicitudeToEdit.bind(this))
    this.addSubscription('clicked.add.subject', this.addSubject.bind(this))
    this.addSubscription('clicked.delete.subject', this.deleteSubject.bind(this))
  }

  initComponent(id){
    this.registerSubscriptions()
    askTranslationsFor({ labels: this.data.labels, scope: "show-solicitude" })
    this.channel.publish('get.solicitude', id)
  }

  updateModel({ data }){

    const { creation_moment, subjects } = data
    const user_id = userSession.email()
    const employees = data.company_employees

    this.data.values.id = creation_moment
    this.data.hasSubjects = (subjects !== undefined && subjects.length > 0)
    this.data.warningEmployees = (!MaxEmployees.isBelow(employees))
    this.channel.publish('get.is.coordinator', userSession.email())

    if(data.user) {
      this.data.owner = (user_id == data.user.user_id)
      this.data.organization = userSession.belongsToOrganization(data.user.user_id)
    }

    let dictionary = this.dictionaryOfSolicitude(data)
    for (let [key, value] of Object.entries(dictionary)) {
      this.data.setValues(key, value)
    }
    this.data.values.user_id = data.user.user_id
    this.data.values.date = dateToDDMMYYYY(data.date)
    this.data.values.time = data.time
  }

  gotIsCoordinator(payload){
    const isCoordinator = payload.data
    const request = {
      owner: this.data.values.user_id,
      logged: userSession.email(),
      isCoordinator: isCoordinator
    }

    this.data.isUserLoggedCoordinator = CheckOwnerInfoAvailable.do(request)
  }

  isUserLoggedFromIstas(user_id){
    return user_id.includes("istas.ccoo.es")
  }

  showSharedSolicitude(id){
    this.goToAndReload(`/#/show-solicitude/${ id }`)
  }

  loadSolicitudeToEdit(){
    this.goTo(`/#/solicitude/${ this.data.values.id }`)
  }

  addSubject(event){
    this.goTo(`/#/subjects/${ this.data.values.id }`)
  }

  deleteSubject(id){
    if (!confirm(this.data.labels.confirmDeleteSubject)){
      return
    }
    this.channel.publish('delete.subject', id)
  }

  deletedSubject(payload){
    let id = payload.data.id
    let subjects = this.data.values.subjects
    this.data.values.subjects = subjects.filter(function(subject){
      return subject.id != id
    })
  }

  dictionaryOfSolicitude(data){
    return {
        'text': data.text,
        'date': data.date,
        'time': data.time,
        'source': data.source,
        'applicantName': data.applicant_name,
        'applicantSurname': data.applicant_surname,
        'applicantCcaa': data.applicant_ccaa,
        'applicantEmail': data.applicant_email,
        'applicantPhonenumber': data.applicant_phonenumber,
        'companyName': data.company_name,
        'companyCif': data.company_cif,
        'companyEmployees': data.company_employees,
        'companyCnae': data.company_cnae,
        'companyCp': data.company_cp,
        'companyId': data.company_id,
        'subjects': data.subjects,
        'numeration': data.numeration,
        'organization': data.organization,
        'shared_origin': data.shared_origin
        }
    }

  model() {
    if(!this.data) { this.data = Models.newShowSolicitude() }

    return this.data
  }

  view() {
    return view
  }
}

const component = new ShowSolicitude()

export default Object.assign({}, component.view, { data: component.model })
