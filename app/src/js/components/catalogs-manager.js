import Component from '../infrastructure/component'
import Models from '../models/models'
import view from '../views/catalogs-manager'
import { Bus } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"
import Catalogs from './catalogs'
import MapperCatalogs from '../lib/mapper'

class CatalogsManager extends Component{

  constructor(){
    new Catalogs()
    super();
  }

  subscribe(){
    Bus.subscribe("fill.catalogs", this.fillCatalogs.bind(this))
    Bus.subscribe("fill.catalogs_with_name", this.fillCatalogsWithName.bind(this))
    Bus.subscribe("fill.catalogs_with_name_and_id", this.fillCatalogsWithNameAndId.bind(this))
    Bus.subscribe("catalogs-manager.created", this.initComponent.bind(this))
    Bus.subscribe("catalogs-manager.destroyed", this.clearSubscriptions.bind(this))
    Bus.subscribe("updated.item.in.catalog", this.refreshCatalog.bind(this))
    Bus.subscribe("added.item.in.catalog", this.addedItemToCatalog.bind(this))
  }

  collectSubscriptions() {
    this.addSubscription("got.translation.for.catalogs-manager", this.translate.bind(this));
  }

  watchActions(){
    this.addSubscription("select.catalog", this.getCatalog.bind(this))
    this.addSubscription("update.item", this.updateItemInCatalog.bind(this))
    this.addSubscription("add.item", this.addItemToCatalog.bind(this))
    this.addSubscription("change.id", this.changeId.bind(this))

  }

  changeId() {
    this.data.validId = false
    for (var item of this.data.values.catalogsList) {
      if (item.id == this.data.values.newItemID) {
          this.data.validId = true
      }
    }
  }

  fillCatalogs(payload) {
    for (let [labelKey, valueKey] of Object.entries(payload)) {
      this.data.set(labelKey, valueKey)
    }
  }

  fillCatalogsWithName(payload) {
    const name = payload.name
    const catalog = payload.catalog
    const content = payload.content

    if (name == this.data.values.catalogName) {
      this.data.values.catalogsList = content.map(item => item.text)
    }
  }

  fillCatalogsWithNameAndId(payload) {
    const name = payload.name
    const catalog = payload.catalog
    const content = payload.content
    if (name == this.data.values.catalogName) {
      this.data.values.catalogsList = content.map((item) => {
        return {id: item.value.id, name: item.value.name }
      })
    }
  }

  initComponent(payload){
    this.registerSubscriptions()
    askTranslationsFor({ labels: this.data.labels, scope: "catalogs-manager" })
  }

  updateItemInCatalog(detail) {
    Bus.publish('update.item.in.catalog', detail)
  }

  addItemToCatalog(detail) {
    Bus.publish('add.item.to.catalog', detail)
  }

  refreshCatalog(payload) {
    Bus.publish('refresh.catalogs')
  }

  addedItemToCatalog() {
    alert(this.data.labels.itemAdded)
    Bus.publish('refresh.catalogs')
  }

  getCatalog(payload) {
    const mapa = {
      "source_formats": new MapperCatalogs('source_formats', this.data.sourceCatalog),
      "ccaa": new MapperCatalogs('ccaa', this.data.ccaaCatalog),
      "topics": new MapperCatalogs('topics', this.data.topicsCatalog),
      "proposals": new MapperCatalogs('proposals', this.data.proposalsCatalog),
      "close_reasons": new MapperCatalogs('close_reasons', this.data.reasonsCatalog),
      "projects": new MapperCatalogs('projects', this.data.projectsCatalog),
      "cnae": new MapperCatalogs('cnae', this.data.cnaeCatalog),
      "organizations": new MapperCatalogs('organizations', this.data.organizationsCatalog)
    }

    if(mapa[payload] !== undefined) {
      this.data.values = mapa[payload].do()
    }

    this.data.values.selectedTitle = this.catalogsDictionary(this.data.values.catalogName)
  }

  catalogsDictionary(key){
    let catalogsDictionary = {
      "source_formats": this.data.labels.source,
      "ccaa": this.data.labels.ccaa,
      "organizations": this.data.labels.organizations,
      "topics": this.data.labels.topics,
      "proposals": this.data.labels.proposals,
      "close_reasons": this.data.labels.reasons,
      "projects": this.data.labels.projects,
      "cnae": this.data.labels.cnae
    }

    return catalogsDictionary[key]
  }

  model() {
    if(!this.data) { this.data = Models.newCatalogsManager() }

    return this.data
  }

  view() {
    return view
  }
}

const component = new CatalogsManager()

export default Object.assign({}, component.view, { data: component.model })
