
class Dictionary {
  static solicitude(payload) {
    const solicitude = {
      'text': payload.data.text,
      'source': payload.data.source,
      'date': payload.data.date,
      'time': payload.data.time,
      'applicantId': payload.data.applicant,
      'applicantName': payload.data.applicant_name,
      'applicantSurname': payload.data.applicant_surname,
      'applicantEmail': payload.data.applicant_email,
      'applicantPhonenumber': payload.data.applicant_phonenumber,
      'applicantCcaa': payload.data.applicant_ccaa,
      'creation_moment': payload.data.creation_moment,
      'numeration': payload.data.numeration,
      'organization': payload.data.organization,
      'subjects': payload.data.subjects,
      'shared_origin': payload.data.shared_origin
    }
    return solicitude
  }

  static company(payload) {
    const company = {
      'companyName': payload.data.company_name,
      'companyCif': payload.data.company_cif,
      'companyEmployees': payload.data.company_employees,
      'companyCnae': payload.data.company_cnae,
      'companyId': payload.data.company_id,
      'companyCp' : payload.data.company_cp
    }
    return company
  }

  static subject(detail) {
    const subject = {
      'subjectId': detail.subjectId,
      'solicitudeId': detail.solicitudeId,
      'proposal': detail.proposals.map(item => item.value),
      'description': detail.description,
      'analysis': detail.analysis,
      'topics': detail.topics.map(item => item.value),
      'comments': detail.comments,
      'reason': detail.reason,
      'closed': detail.closed,
      'created': detail.created,
      'createdTime': detail.createdTime,
      'project': detail.project,
      'numeration': detail.numeration,
      'files': detail.files
    }
    return subject
  }

  static charge(dictionary, data) {
    for (let [labelKey, valueKey] of Object.entries(dictionary)) {
      data.setValues(labelKey, valueKey)
    }
  }
}

export default Dictionary
