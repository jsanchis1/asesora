import Solicitude from '../models/solicitude'
import Component from '../infrastructure/component'
import CatalogsService from '../services/catalogs'
import { Bus } from '../bus'

class Catalogs extends Component {
  constructor() {
    super()
    this.data = new Solicitude
    new CatalogsService()
  }

  subscribe(){
    Bus.subscribe("got.cnae-catalog", this.gotCnaeCatalog.bind(this))
    Bus.subscribe("got.topics-catalog", this.gotTopicsCatalog.bind(this))
    Bus.subscribe("got.ccaa-catalog", this.gotCcaaCatalog.bind(this))
    Bus.subscribe("got.organizations-catalog", this.gotOrganizationsCatalog.bind(this))
    Bus.subscribe("got.proposals-catalog", this.gotProposalsCatalog.bind(this))
    Bus.subscribe("got.projects-catalog", this.gotProjectsCatalog.bind(this))
    Bus.subscribe("got.reasons-catalog", this.gotReasonsCatalog.bind(this))
    Bus.subscribe("got.source-formats-catalog", this.gotSourceFormatsCatalog.bind(this))
  }

  fill(payload) {
    Bus.publish('fill.catalogs', payload)
  }

  fillWithName(payload) {
    Bus.publish('fill.catalogs_with_name', payload)
  }

  fillWithNameAndId(payload) {
    Bus.publish('fill.catalogs_with_name_and_id', payload)
  }

  gotCnaeCatalog(payload) {
    let catalog = []
    for (const cnae of payload.data.content) {
      catalog.push({ value: cnae.id, text: cnae.name })
    }
    catalog.sort((a, b) => a.value.localeCompare(b.value));
    this.data.cnaeCatalog = catalog
    this.fill({cnaeCatalog: catalog})
    this.fillWithName({catalog: 'cnaeCatalog', name: payload.data.name, content: catalog})
  }

  gotTopicsCatalog(payload) {
    let catalog = []
    for (const topic of payload.data.content) {
      catalog.push({ value: {id: topic.id, name: topic.name}, text: topic.name })
    }
    catalog.sort((a, b) => a.value.id.localeCompare(b.value.id));
    this.data.topicsCatalog = catalog
    this.fill({topicsCatalog: catalog})
    this.fillWithNameAndId({catalog: 'topicsCatalog', name: payload.data.name, content: catalog})
  }

  gotCcaaCatalog(payload) {
    let catalog = []
    for (const ccaa of payload.data.content) {
      catalog.push({ value: ccaa.id, text: ccaa.name })
    }
    catalog.sort((a, b) => a.text.localeCompare(b.text));

    this.data.ccaaCatalog = catalog
    this.fill({ccaaCatalog: catalog})
    this.fillWithName({catalog: 'ccaaCatalog', name: payload.data.name, content: catalog})
  }

  gotOrganizationsCatalog(payload) {
    let catalog = []
    for (const organization of payload.data.content) {
      catalog.push({ value: organization.name, text: organization.name })
    }
    catalog.sort((a, b) => a.value.localeCompare(b.value));
    this.data.organizationsCatalog = catalog
    this.fill({organizationsCatalog: catalog})
    this.fillWithName({catalog: 'organizationsCatalog', name: payload.data.name, content: catalog})
  }

  gotProposalsCatalog(payload) {
    let catalog = []
    for (const proposal of payload.data.content) {
      catalog.push({ value: proposal, text: proposal })
    }
    catalog.sort((a, b) => a.value.localeCompare(b.value));
    this.data.proposalsCatalog = catalog
    this.fill({proposalsCatalog: catalog})
    this.fillWithName({catalog: 'proposalsCatalog', name: payload.data.name, content: catalog})
  }

  gotProjectsCatalog(payload) {
    let catalog = []
    for (const project of payload.data.content) {
      catalog.push({ value: project, text: project })
    }
    catalog.sort((a, b) => a.value.localeCompare(b.value));
    this.data.projectsCatalog = catalog
    this.fill({projectsCatalog: catalog})
    this.fillWithName({catalog: 'projectsCatalog', name: payload.data.name, content: catalog})
  }

  gotReasonsCatalog(payload) {
    let catalog = []
    for (const reason of payload.data.content) {
      catalog.push({ value: reason.id, text: reason.name })
    }
    catalog.sort((a, b) => a.value.localeCompare(b.value));
    this.data.reasonsCatalog = catalog
    this.fill({reasonsCatalog: catalog})
    this.fillWithName({catalog: 'reasonsCatalog', name: payload.data.name, content: catalog})
  }

  gotSourceFormatsCatalog(payload) {
    let catalog = []
    for (const source of payload.data.content) {
      catalog.push({ value: source.id, text: source.name })
    }
    catalog.sort((a, b) => a.text.localeCompare(b.text));
    this.data.sourceCatalog = catalog
    this.fill({sourceCatalog: catalog})
    this.fillWithName({catalog: 'sourceCatalog', name: payload.data.name, content: catalog})
  }
}

export default Catalogs
