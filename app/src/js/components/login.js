import Component from '../infrastructure/component'
import view from '../views/login'
import Models from '../models/models'
import { Bus } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"
import userSession from '../infrastructure/user_session'

class Login extends Component{
  constructor(){
    super()
  }

  subscribe(){
    Bus.subscribe("got.translation.for.login", this.translate.bind(this))
    Bus.subscribe("got.user.authorization", this.handleAuthorization.bind(this))
  }

  handleAuthorization(payload) {
    const {token, error} = payload
    this.data.values.error = error
    this.data.values.token = token
    if (token) {
      userSession.token(token)
      window.location.href = '/'
    }
  }

  askTranslations() {
    askTranslationsFor({ labels: this.data.labels, scope: "login" })
  }

  model(){
    if(!this.data) { this.data = Models.newLogin() }

    return this.data
  }

  view(){
    return view
  }

}

const component = new Login()

export default Object.assign({}, component.view, { data: component.model })
