import Component from '../infrastructure/component'
import MaxEmployees from '../lib/MaxEmployees'
import service from '../services/subjects'
import Models from '../models/models'
import view from '../views/subjects-list'
import userSession from '../infrastructure/user_session'
import { Bus } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"
import CheckOwnerInfoAvailable from '../lib/checkOwnerAvailable'
import { jsonToExcel } from '../infrastructure/json_to_excel'
import { getFilename, getCounselingsFields, getFEPRLCounselingsFields } from '../lib/counselings'
import isEmpty from '../lib/isEmpty'

export class SubjectsList extends Component {
  constructor(){
    service()
    super()
    this.data.userSessionEmail = userSession.email()
    this.buildSelectOptions()

    this.checkAuthorizedEmail()
  }

  subscribe(){
    Bus.subscribe("subjects-list.view.created", this.retrieve.bind(this))
    Bus.subscribe("got.subjects-list", this.populateSubjectsList.bind(this))
    Bus.subscribe("got.translation.for.subjects-list", this.translate.bind(this))
    Bus.subscribe("got.counselings", this.downloadCounselings.bind(this))
    Bus.subscribe("got.JustifyCounselings",this.downloadJustifyCounsolings.bind(this))
    Bus.subscribe("filtered.my.subjects", this.populateSubjectsList.bind(this))
    Bus.subscribe("filtered.domain.less.mine.subjects", this.populateSubjectsList.bind(this))
    Bus.subscribe("filtered.domain.subjects", this.populateSubjectsList.bind(this))
    Bus.subscribe("filtered.all.subjects.less.mine", this.populateSubjectsList.bind(this))
    Bus.subscribe("filtered.all.subjects", this.populateSubjectsList.bind(this))
    Bus.subscribe("filtered.subjects.by.text", this.populateSubjectsByText.bind(this))
    Bus.subscribe("checked.authorized.email", this.checkedAuthorizedEmail.bind(this))
    Bus.subscribe("got.is.coordinator", this.gotIsCoordinator.bind(this))
  }

  watchActions(){
    Bus.subscribe('load.solicitude', this.editSolicitude.bind(this))
    Bus.subscribe('show.solicitude', this.showSolicitude.bind(this))
    Bus.subscribe('download.clicked', this.getCounselings.bind(this))
    Bus.subscribe('download.for.justification', this.getCounselingsForJustification.bind(this))
    Bus.subscribe('filter.subjects', this.filterSubjects.bind(this))
    Bus.subscribe('filter.text', this.filterSubjectsByText.bind(this))
  }

  retrieve(){
    const user_id = userSession.email()
    Bus.publish("get.subjects-list", user_id)
  }

  editSolicitude(event){
    this.goTo(`/#/solicitude/${ event.detail }`)
  }

  showSolicitude(event){
    this.goTo(`/#/show-solicitude/${ event.detail }`)
  }

  populateSubjectsList({ data }){
    let subjects = []
    data.forEach(element => {
      subjects.push(this.createDataSubjecView(element))
    })
    this.data.subjectsList = subjects
    this.data.hasSubjects = !isEmpty(subjects)
    this.setOwner(subjects)
    this.checkAuthorizedEmail()

    let user = userSession.email()
    Bus.publish('get.is.coordinator', user)
  }

  populateSubjectsByText({ data }) {
    let subjects = []

    data.forEach(element => {
      subjects.push(this.createDataSubjecView(element))
    })
    this.data.subjectsList = subjects
    this.data.hasSubjects = !isEmpty(subjects)

    Bus.publish('get.is.coordinator', user)
  }

  gotIsCoordinator(payload){
    this.data.subjectsList = this.data.subjectsList.map((subject) => {
      let request = {
        owner: subject['user_id'],
        logged: userSession.email(),
        isCoordinator: payload.data
      }
      subject['showOwner'] = CheckOwnerInfoAvailable.do(request)
      return subject
    })
  }

  setOwner(subjects){
    this.data.subjectsList = subjects.map((subject) => {
      subject['owner'] = userSession.isCurrentUser(subject['user_id'])
      subject['isMyDomain'] = userSession.belongsToOrganization(subject['user_id'])

      return subject
    })
  }

  filterSubjectsByText() {
    if(this.data.values.filter.length >= 3){
      Bus.publish('filter.subjects.by.text', this.data.values.filter)
    } else {
      this.filterSubjects()
    }
  }

  filterSubjects(){
    const user_id = userSession.email()
    const option = this.data.values.selectedList.value

    if(option == 0) Bus.publish('filter.my.subjects', user_id )
    if(option == 1) Bus.publish('filter.domain.less.mine.subjects', user_id)
    if(option == 2) Bus.publish('filter.domain.subjects', user_id)
    if(option == 3) Bus.publish('filter.all.subjects.less.mine', user_id)
    if(option == 4) Bus.publish('filter.all.subjects', user_id)

    this.checkAuthorizedEmail()
  }

  buildAllSubjectsFilter(user_id) {
    const response = Bus.publish('filter.all.subjects.less.mine', user_id) + Bus.publish('filter.domain.subjects', user_id)
    return response
  }

  buildSelectOptions(){
    const domain = this.retrieveDomain()
    const mySubjects = this.data.labels.mySubjects
    const myDomainSubjects = this.data.labels.myDomainSubjects + domain
    const allSubjects = this.data.labels.allSubjects + domain
    const allDomains = this.data.labels.allDomains
    const all = this.data.labels.all
    this.data.listOptions =[
      {"value": 0, "text": mySubjects },
      {"value": 1, "text": myDomainSubjects},
      {"value": 2, "text": allSubjects},
      {"value": 3, "text": allDomains},
      {"value": 4, "text": all}]

    this.data.values.selectedList =  this.data.listOptions[0]
  }

  checkedAuthorizedEmail(payload) {
    const isCoordinator = payload.data
    const option = this.data.values.selectedList.value
    this.data.showFEPRLButton = this.showFEPRL(isCoordinator, option)
  }

  showFEPRL(isCoordinator, option){
    if(option == 0) return true
    if(isCoordinator && this.isJustificableOption(option)) return true
    return false
  }

  isJustificableOption(option){
    const justificableOptions = [ {"value": 1, "text": "myDomainSubjects"},
                                  {"value": 2, "text": "allSubjects"}]

    for(let item of justificableOptions){
      if(item['value'] == option) return true
    }
    return false
  }

  checkAuthorizedEmail() {
    const email = userSession.email()

    Bus.publish('check.authorized.email', email)
  }

  retrieveDomain() {
    const email = userSession.email()
    const domain = email.substring(email.lastIndexOf("@") +1)
    return domain
  }

  createDataSubjecView(data) {
    return {
      "subject_id": data.subject_id,
      "solicitude_id": data.solicitude_id,
      "solicitudeDate": data.date,
      "companyName": data.company_name,
      "project": data.project,
      "topics": this.cleanTopics(data.topics),
      "created" : data.created,
      "createdTime": data.createdTime,
      "numeration": data.numeration,
      "record": {
        "analysis": data.analysis,
        "description": data.description,
        "proposal": data.proposal,
        "closed" : data.closed
      },
      "isSolicitudeJustified": this.isSolicitudeJustified(data.date,
        data.source,
        data.ccaa,
        data.employees,
        data.cnae),
      "isSubjectJustified": this.isSubjectJustified(data.topics, data.proposal),
      "solicitude_numeration": data.solicitude_numeration,
      "user_id": data.user_id,
      "hasAttachments": this.hasAttachments(data.files),
      "shared_destination": data.shared_destination
    }
  }

  hasAttachments(items){
    return !isEmpty(items)
  }

  isSolicitudeJustified(date, source, ccaa, employees, cnae) {
    return (!isEmpty(date)  &&
            !isEmpty(source)  &&
            !isEmpty(ccaa)  &&
            !isEmpty(employees)  &&
            !isEmpty(cnae)  &&
            MaxEmployees.isBelow(employees))
  }

  isSubjectJustified(topics, proposal) {
    return (!isEmpty(topics)  &&
            !isEmpty(proposal) )
  }

  cleanTopics(topics){
    const cleanedTopics = [];

    topics.forEach(element => {
      cleanedTopics.push(element["name"])
    })
    return cleanedTopics
  }

  getCounselings() {
    const option = this.data.values.selectedList.value
    const user_id = userSession.email()

    if(option == 0) Bus.publish('filter.my.subjects.csv', user_id )
    if(option == 1) Bus.publish('filter.domain.less.mine.subjects.csv', user_id)
    if(option == 2) Bus.publish('filter.domain.subjects.csv', user_id)
    if(option == 3) Bus.publish('filter.all.subjects.less.mine.csv', user_id)
    if(option == 4) Bus.publish('filter.all.subjects.csv', user_id)
  }

  getCounselingsForJustification(){
    const option = this.data.values.selectedList.value
    const user_id = userSession.email()

    if(option == 0) Bus.publish('justification.my.subjects.csv', user_id )
    if(option == 1) Bus.publish('justification.domain.less.mine.subjects.csv', user_id)
    if(option == 2) Bus.publish('justification.domain.subjects.csv', user_id)
  }

  downloadCounselings({ data: counselings }) {
    const filename = getFilename(userSession.email())
    const counselingsFields = getCounselingsFields(counselings)
    return jsonToExcel(filename, counselingsFields)
  }

  downloadJustifyCounsolings({data: counselings}) {
    const filename = getFilename(userSession.email())
    const counselingsFields = getFEPRLCounselingsFields(counselings)
    return jsonToExcel(filename, counselingsFields)
  }

  askTranslations() {
    askTranslationsFor({ labels: this.data.labels, scope: "subjects-list" });
  }

  model(){
    if(!this.data) { this.data = Models.newSubjectsList() }

    return this.data
  }

  view() {
    return view
  }
}

const component = new SubjectsList()

export default Object.assign({}, component.view, { data: component.model })
