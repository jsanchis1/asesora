import { Bus } from '../bus'
import { APIClient } from '../infrastructure/api_client'
import buildCallback from '../infrastructure/buildCallback'

export default function() {
  Bus.subscribe("get.subjects-list", user_id => getMySubjects(user_id))
  Bus.subscribe('filter.my.subjects.csv', user_id => getMySubjectsCSV(user_id))
  Bus.subscribe('filter.domain.less.mine.subjects.csv', user_id => getByDomainSubjectsLessCSV(user_id))
  Bus.subscribe('filter.domain.subjects.csv', user_id => getByDomainSubjectsCSV(user_id))
  Bus.subscribe('filter.all.subjects.less.mine.csv', user_id => getAllSubjectsLessMyDomainCSV(user_id))
  Bus.subscribe('filter.all.subjects.csv', user_id => getAllSubjectsCSV(user_id))

  Bus.subscribe('justification.my.subjects.csv', user_id => getJustificationMySubjectsCSV(user_id))
  Bus.subscribe('justification.domain.less.mine.subjects.csv', user_id => getJustificationByDomainSubjectsLessCSV(user_id))
  Bus.subscribe('justification.domain.subjects.csv', user_id => getJustificationByDomainSubjectsCSV(user_id))


  Bus.subscribe('filter.my.subjects', user_id => getMySubjects(user_id))
  Bus.subscribe('filter.domain.less.mine.subjects', user_id => getByDomainSubjectsLess(user_id))
  Bus.subscribe('filter.domain.subjects', user_id => getByDomainSubjects(user_id))
  Bus.subscribe('filter.all.subjects.less.mine', user_id => getAllSubjectsLessMyDomain(user_id))
  Bus.subscribe('filter.all.subjects', user_id => getAllSubjects(user_id))
  Bus.subscribe('filter.subjects.by.text', payload => filterSubjectsByText(payload))
  Bus.subscribe('check.authorized.email', email => checkAuthorizedEmail(email))
  Bus.subscribe("get.is.coordinator", user_id => getIsCoordinator(user_id))
}

const getIsCoordinator = user_id => {
  const callback = buildCallback('got.is.coordinator')
  const url = 'is-user-coordinator'
  APIClient.hit(url, {user_id}, callback)
}

const checkAuthorizedEmail = user_id => {
  const callback = buildCallback('checked.authorized.email')
  const url = 'is-user-coordinator'
  APIClient.hit(url, { user_id }, callback)
}

const getMySubjectsCSV = user_id => {
  const callback = buildCallback('got.counselings')
  const url = 'retrieve-subjects-by-user-csv'
  APIClient.hit(url, { user_id }, callback)
}
const getByDomainSubjectsLessCSV = user_id => {
  const callback = buildCallback('got.counselings')
  const url = 'retrieve-subjects-by-domain-less-csv'
  APIClient.hit(url, { user_id }, callback)
}

const getJustificationMySubjectsCSV = user_id => {
  const callback = buildCallback('got.JustifyCounselings')
  const url = 'retrieve-subjects-by-user-csv'
  APIClient.hit(url, { user_id }, callback)
}

const getJustificationByDomainSubjectsLessCSV = user_id => {
  const callback = buildCallback('got.JustifyCounselings')
  const url = 'retrieve-subjects-by-domain-less-csv'
  APIClient.hit(url, { user_id }, callback)
}

const getJustificationByDomainSubjectsCSV = user_id => {
  const callback = buildCallback('got.JustifyCounselings')
  const url = 'retrieve-subjects-by-domain-csv'
  APIClient.hit(url, { user_id }, callback)
}

const getByDomainSubjectsCSV = user_id => {
  const callback = buildCallback('got.counselings')
  const url = 'retrieve-subjects-by-domain-csv'
  APIClient.hit(url, { user_id }, callback)
}
const getAllSubjectsLessMyDomainCSV = user_id => {
  const callback = buildCallback('got.counselings')
  const url = 'retrieve-subjects-less-my-domain-csv'
  APIClient.hit(url, { user_id }, callback)
}
const getAllSubjectsCSV = user_id => {
  const callback = buildCallback('got.counselings')
  const url = 'retrieve-all-subjects-csv'
  APIClient.hit(url, { user_id }, callback)
}

const getMySubjects = user_id => {
  const callback = buildCallback('filtered.my.subjects')
  const url = 'retrieve-subjects-by-user'
  APIClient.hit(url, { user_id }, callback)
}

const getByDomainSubjectsLess = user_id => {
  const callback = buildCallback('filtered.domain.less.mine.subjects')
  const url = 'retrieve-subjects-by-domain-less'
  APIClient.hit(url, { user_id }, callback)
}

const getByDomainSubjects = user_id => {
  const callback = buildCallback('filtered.domain.subjects')
  const url = 'retrieve-subjects-by-domain'
  APIClient.hit(url, { user_id }, callback)
}

const getAllSubjectsLessMyDomain = user_id => {
  const callback = buildCallback('filtered.all.subjects.less.mine')
  const url = 'retrieve-subjects-less-my-domain'
  APIClient.hit(url, { user_id }, callback)
}

const getAllSubjects = user_id => {
  const callback = buildCallback('filtered.all.subjects')
  const url = 'retrieve-all-subjects'
  APIClient.hit(url, { user_id }, callback)
}

const filterSubjectsByText = payload => {
  const callback = buildCallback('filtered.subjects.by.text')
  const url = 'retrieve-subjects-by-text'
  APIClient.hit(url, { payload }, callback)
}
