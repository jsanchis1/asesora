import { Bus } from '../bus'
import { APIClient } from '../infrastructure/api_client'
import buildCallback from '../infrastructure/buildCallback'

export default class Subjects {
  constructor() {
    this.client = APIClient

    this.subscriptions()
  }

  subscriptions() {
    Bus.subscribe("get.solicitudes.list", this.getSolicitudesList.bind(this))
    Bus.subscribe("filter.my.solicitudes", this.getSolicitudesList.bind(this) )
    Bus.subscribe("filter.domain.less.mine.solicitudes", this.getSolicitudesByDomain.bind(this))
    Bus.subscribe("filter.domain.solicitudes", this.getAllSolicitudes.bind(this))
    Bus.subscribe("get.is.coordinator", this.getIsCoordinator.bind(this))
  }

  getIsCoordinator(user_id){
    const callback = buildCallback('got.is.coordinator')
    const url = 'is-user-coordinator'
    APIClient.hit(url, {user_id}, callback)
  }

  getSolicitudesList(user_id) {
    const callback = buildCallback('filtered.my.solicitudes')
    const url = 'retrieve-solicitudes-by-user'
    APIClient.hit(url, { user_id }, callback)
  }

  getSolicitudesByDomain(user_id) {
    const callback = buildCallback('filtered.domain.less.mine.solicitudes')
    const url = 'retrieve-solicitudes-by-domain-less-mine'
    APIClient.hit(url, { user_id }, callback)
  }

  getAllSolicitudes(user_id) {
    const callback = buildCallback('filtered.domain.solicitudes')
    const url = 'retrieve-solicitudes-by-domain'
    APIClient.hit(url, { user_id }, callback)
  }
}
