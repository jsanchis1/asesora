import { Bus } from '../bus'

export default class Component {

  constructor(){
    this.setModelToData()
    this.model = this.model.bind(this)
    this.subscriptions = []
    this.view = this.view()
    this.subscribe()
    this.collectSubscriptions()
    this.askTranslations()
    this.watchActions()
  }

  setModelToData() {
    this.model()
  }

  subscribe(){
    console.log('subscribe must be overwritten')
  }

  collectSubscriptions(){
    console.log('collectSubscriptions must be overwritten')
  }

  registerSubscriptions() {
    this.clearSubscriptions()
    this.subscriptions.map(({ topic, message }) => Bus.subscribe(topic, message))
  }

  clearSubscriptions() {
    this.subscriptions.map(({ topic, message }) => Bus.unsubscribe(topic, message))
  }

  askTranslations() {}

  translate({ key, label }) {
    this.data.labels[key] = label
  }

  addSubscription(topic, message){
    this.subscriptions.push({ topic, message })
  }

  goTo(path) {
    window.location.href = path
  }

  goToAndReload(path) {
    this.goTo(path)
    window.location.reload()
  }

  watchActions(){}

  model(){}

  view(){}
}
