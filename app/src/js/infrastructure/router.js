import VueRouter from 'vue-router'

const navigator = _ => {
  const router = new VueRouter()

  return {
    back: _ => router.go(-1)
  }
}
export default navigator