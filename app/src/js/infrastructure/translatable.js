import { Bus } from "../bus"

const askTranslationsFor = ({ labels, scope }) => {
  Object.keys(labels).forEach(key => {
    Bus.publish('ask.translation', { for: scope, key: key })
  })
}

module.exports = { askTranslationsFor }