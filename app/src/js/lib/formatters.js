export const dateToDDMMYYYY = (dateString = "") => {
  const date = new Date(dateString)
  const day = date.getDate()
  const month = date.getMonth() + 1
  const year = date.getFullYear()

  return `${ day }/${ month }/${ year }`
}

export const dateForFilename = (date = new Date()) => {
  const day = date.getUTCDate()
  const month = date.getUTCMonth() + 1
  const year = date.getUTCFullYear()

  return `${ day }-${ month }-${ year }`
}