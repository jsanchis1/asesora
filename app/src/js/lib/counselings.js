import { dateToDDMMYYYY, dateForFilename } from './formatters'

export const getFilename = userEmail => {
  const dateFormatted = dateForFilename(new Date())

  return `asesoramientos_${ userEmail }_${ dateFormatted}`
}

export const getCounselingsFields = counselings => {
  
  const selectFields = item => ({
    propietario: item.owner,
    solicitud_id: item.solicitude_numeration,
    solicitud_fecha: dateToDDMMYYYY(item.solicitude_date),
    solicitud_hora: item.solicitude_time || "",
    solicitud_texto: item.solicitude_text,
    solicitud_fuente: getText(item.solicitude_source),
    solicitante_nombre: item.applicant_name,
    solicitante_apellidos: item.applicant_surname,
    solicitante_email: item.applicant_email,
    solicitante_telefono: item.applicant_phonenumber,
    solicitante_ccaa: getText(item.applicant_ccaa),
    empresa_nombre: item.company_name || "",
    empresa_cif: item.company_cif || "",
    empresa_cnae: item.company_cnae || "",
    empresa_codigo_postal: item.company_cp || "",
    empresa_num_trabajadores: item.company_employees || "",
    caso_id: item.subject_numeration,
    caso_fecha_apertura: dateToDDMMYYYY(item.subject_created),
    caso_hora_apertura: item.subject_createdTime || "",
    caso_temas: serializeTopicNames(item.subject_topics),
    caso_propuestas: item.subject_proposal.join('; '),
    caso_descripcion_propuestas: item.subject_description,
    caso_analisis: item.subject_analysis,
    caso_motivo_cierre: getText(item.subject_reason),
    caso_comentarios_cierre: item.subject_comments || "",
    proyecto: getText(item.subject_project)
  })

  const getText = item => item == null ? "" : item.text || ""

  const serializeTopicNames = topics => (
    topics.map(({ name }) => name).join('; ')
  )

  return counselings.map(selectFields)
}

export const getFEPRLCounselingsFields = counselings => {
  const selectFields = item => ({
    solicitud_id: item.solicitude_numeration,
    caso_id: item.subject_numeration,
    caso_fecha_apertura: dateToDDMMYYYY(item.subject_created),
    solicitud_fuente: getText(item.solicitude_source),
    caso_temas: serializeTopicNames(item.subject_topics),
    caso_propuestas: item.subject_proposal.join('; '),
    empresa_nombre: item.company_name || "",
    empresa_num_trabajadores: item.company_employees || "",
    empresa_cnae: item.company_cnae_feprl || "",
    solicitante_ccaa: item.applicant_ccaa_feprl || "",
    proyecto: getText(item.subject_project),
    email_asesor: item.solicitude_user.user_id
  })

  const getText = item => item == null ? "" : item.text || ""

  const serializeTopicNames = topics => (
    topics.map(({ name }) => name).join('; ')
  )

  return counselings.map(selectFields)
}
