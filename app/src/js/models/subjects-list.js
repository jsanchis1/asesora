import Model from './model'
import {Bus} from '../bus'

export default class SubjectsList extends Model {
  constructor() {
    super()
    this.labels = {
      "caseNumber": "",
      "solicitudeId": "",
      "createdAt": "",
      "companyName": "",
      "topics": "",
      "register": "",
      "isJustified": "",
      "show": "",
      "edit": "",
      "title": "",
      "created": "",
      "createdTime": "createdTime",
      "notApply": "",
      "registerInfoA": "",
      "registerInfoD": "",
      "registerInfoP": "",
      "registerInfoC": "",
      "subjectWarning": "",
      "subjectCheck": "",
      "download": "",
      "project": "",
      "mySubjects": "",
      "myDomainSubjects": "",
      "allSubjects": "",
      "allDomains": "",
      "hasAttachments": "",
      "all": "",
      "downloadForJustification": "",
      "shared": "",
      "owner": ""
    }
    this.values = {
      "selectedList": "",
      "filter": ""
    }
    this.subjectsList = []
    this.hasSubjects = false
    this.showFEPRLButton = false
    this.listOptions = []
    this.bus = Bus
  }
}
