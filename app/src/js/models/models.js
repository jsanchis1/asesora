import LinkBar from './linkbar'
import Login from './login'
import Navbar from './navbar'
import ShowSolicitude from './show-solicitude'
import Solicitude from './solicitude'
import SolicitudesList from './solicitudes-list'
import SubjectsList from './subjects-list'
import SubjectsLot from './subjects-lot'
import Subjects from './subjects'
import UsersManager from './users-manager'
import CatalogsManager from './catalogs-manager'

class Models {
  static newUsersManager() {
    return new UsersManager()
  }

  static newCatalogsManager() {
    return new CatalogsManager()
  }

  static newSubjects() {
    return new Subjects()
  }

  static newSubjectLot() {
    return new SubjectsLot()
  }

  static newSubjectsList() {
    return new SubjectsList()
  }

  static newSolicitudesList() {
    return new SolicitudesList()
  }

  static newSolicitude() {
    return new Solicitude()
  }

  static newShowSolicitude() {
    return new ShowSolicitude()
  }

  static newNavbar() {
    return new Navbar()
  }

  static newLogin() {
    return new Login()
  }

  static newLinkBar() {
    return new LinkBar()
  }
}

export default Models
