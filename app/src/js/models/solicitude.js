import Model from './model'

export default class Solicitude extends Model {
  constructor() {
    super()

    this.labels = {
      "max200Words": "",
      "confirmRemoveSolicitudeWithoutSubject": "",
      "confirmRemoveSolicitudeWithSubject": "",
      "confirmRemoveSubjectClosedStatus": "",
      "deleteSubject": "",
      "analysis": "",
      "applicant": "",
      "date": "",
      "time": "",
      "applicantEmail": "",
      "applicantPhonenumber": "",
      "applicantName": "",
      "applicantSurname": "",
      "text": "",
      "noDate": "",
      "addSubject": "",
      "editingSolicitude": "",
      "company": "",
      "companyName": "",
      "companyCif": "",
      "companyEmployees": "",
      "companyCnae": "",
      "companyId": "",
      "noContact": "",
      "incompleteCompanyIdentity": "",
      "maxEmployees": "",
      "noCompanyName": "",
      "suggestions" : "",
      "submit" : "",
      "submitting" : "",
      "editiondiscard" : "",
      "editionsubmit" : "",
      "editionsubmitting" : "",
      "deleteSolicitude" : "",
      "errorPhone": "",
      "errorEmail": "",
      "proposals": "",
      "project": "",
      "topics": "",
      "sent": "",
      "subjectsList": "",
      "subjectsSolicitude": "",
      "companyInfo": "",
      "notApply": "",
      "subject": "",
      "modifySubject": "",
      "subjectModified": "",
      "modify": "",
      "ccaa": "",
      "personalData": "",
      "contactData": "",
      "solicitudeData": "",
      "solicitude": "",
      "identification": "",
      "information": "",
      "description": "",
      "placeholderdescription": "",
      "comments": "",
      "reason": "",
      "created": "",
      "createdTime": "",
      "closeCounseling": "",
      "closedSubject": "",
      "source": "",
      "discard": "",
      "deleteSubjectMessage": "",
      "required": "",
      "notJustifiable": "",
      "removeSubjectClosedStatus": "",
      "placeholderAnalysis": "",
      "placeholderdescription": "",
      "analisisInstructions": "",
      "sharedData": "",
      "sharedDataInfo": "",
      "registrationInstructions": "",
      "sharedCompanyData": "",
      "suggestChanges": "",
      "incompleteRecord": "",
      "organization": "",
      "caseNumber": "",
      "createdAt": "",
      "register": "",
      "isJustified": "",
      "subjectWarning": "",
      "edit": "",
      "registerInfoA": "",
      "registerInfoD": "",
      "registerInfoP": "",
      "registerInfoC": "",
      "subjectCheck": "",
      "hasAttachments": "",
      "removeConfirmAttachment": "",
      "maxSized": "",
      "createSubject":"",
      "editingSubject": "",
      "cancel": "",
      "newSubject": "",
      "ownerDomain": "",
      "requiredToShare": "",
      "infoToShare": "",
      "involve": "",
      "send": "",
      "subjectSharedMessage": "",
      "shared": "",
      "solicitudeInvolved": "",
      "alreadyInvolved": "",
      "textToShare": "",
      "istasEmail": "",
      "text": "",
      "requiredText": "",
      "companyCp": ""
    }

    this.values = {
      "text": "",
      "user_id": "",
      "date": "",
      "time": "",
      "id": "",
      "applicantName": "",
      "applicantSurname": "",
      "applicantEmail": "",
      "applicantPhonenumber": "",
      "applicantId": "",
      "applicantCcaa": "",
      "companyName": "",
      "companyCif": "",
      "companyCp": "",
      "companyEmployees": "",
      "companyCnae": "",
      "companyId": "",
      "suggestions" : "",
      "subjects": [],
      "proposals": [],
      "analysis": "",
      "selectedTopics": [],
      "subjectId": "",
      "description": "",
      "comments": "",
      "reason": "",
      "created": "",
      "createdTime": "",
      "closed": "",
      "source": [],
      "numeration": "",
      "project": {},
      "organization": "",
      "files": [],
      "shareEmail": "",
      "shareText": "",
      "shared_origin": ""
    }
    this.sourceCatalog = []
    this.ccaaCatalog = []
    this.organizationsCatalog = []
    this.topicsCatalog = []
    this.proposalsCatalog = []
    this.reasonsCatalog = []
    this.projectsCatalog = []
    this.cnaeCatalog =[]
    this.suggestedCompanies = []
    this.suggestedApplicants = []
    this.fullfilled = false
    this.fullfilledToAddSubject = false
    this.isValidCif = true
    this.isValidCompanyIdentity = true
    this.isValidEmployees = true
    this.isValidCompanyName = true
    this.isValidContact = false
    this.isSubmittable = false
    this.requiredIncomplete = true
    this.showAlert = true
    this.isEditionMode = false
    this.isCreationMode = true
    this.showEditCompanyButton = false
    this.isValidPhone = true
    this.isValidEmail = true
    this.editionSubject = false
    this.modifiedSubjectId = 0
    this.warning = true
    this.warningSubject = true
    this.hasUnsavedChanges = false
    this.owner = true
    this.showSubjectEditModal = false
    this.downloadUrl = this.generateDownloadUrl()
    this.showShareEmail = false
    this.isValidShareEmail = false
    this.authorizedToShare = false
    this.showCloseSubjectModal = false
  }

  cloneValues(){
    let clone = Object.assign({}, this.values)
    return clone
  }

  generateDownloadUrl() {
    const host = process.env.API_HOST
    const port = process.env.API_PORT
    const endpoint = '/api/download/'

    return 'https://' + host + ':' + port + endpoint
  }
}
