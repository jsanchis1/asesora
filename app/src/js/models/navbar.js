import Model from './model'

export default class Navbar extends Model {
  constructor() {
    super()
    this.labels = {
      "name": "",
      "createSolicitude": "",
      "solicitudesList": "",
      "subjectsList": "",
      "usersManager": ""
    }
    this.values = {
      "name": "",
      "imageUrl": ""
    }
    this.control = {
      isAdmin: false
    }
  }
}
