import Model from './model'

export default class SolicitudesList extends Model {
  constructor() {
    super()
    this.labels = {
      "code": "",
      "date": "",
      "time": "",
      "applicant": "",
      "company": "",
      "topics": "",
      "listTitle": "",
      "notApply": "",
      "edit": "",
      "show": "",
      "subjects": "",
      "feprl": "",
      "zeroSubjects": "",
      "titleWithoutSubject": "",
      "of": "",
      "justifiedSubjects": "",
      "mySolicitudes": "",
      "onlyDomain": "",
      "allSolicitudes": "",
      "shared": "",
      "owner": ""
    }
    this.values = {
      "selectedList": ""
    }
    this.solicitudes = []
    this.listOptions = []
  }
}
