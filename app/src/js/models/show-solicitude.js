import Model from './model'

export default class ShowSolicitude extends Model {
  constructor() {
    super()
    this.labels = {
      "proposals": "",
      "analysis": "",
      "topics": "",
      "edit": "",
      "addSubject": "",
      "subjectsData": "",
      "summary": "",
      "applicant": "",
      "company": "",
      "applicantName": "",
      "applicantSurname": "",
      "applicantCcaa": "",
      "applicantEmail": "",
      "applicantPhonenumber": "",
      "date": "",
      "created": "",
      "createdTime": "",
      "text": "",
      "companyName": "",
      "companyCif": "",
      "companyCp": "",
      "companyEmployees": "",
      "companyCnae": "",
      "subjectsList": "",
      "notApply": "",
      "subject": "",
      "description": "",
      "placeholderdescription": "",
      "comments": "",
      "reason": "",
      "source": "",
      "deleteSubject": "",
      "required": "",
      "notJustifiable": "",
      "project": "",
      "confirmDeleteSubject": "",
      "files": "",
      "incompleteRecord": "",
      "organization": "",
      "closedSubject": "",
      "solicitudeInvolved": "",
      "subjectInvolved": ""
    }
    this.values = {
      "id": "",
      "applicantName": "",
      "applicantSurname": "",
      "applicantCcaa": "",
      "applicantEmail": "",
      "applicantPhonenumber": "",
      "date": "",
      "text": "",
      "companyName": "",
      "companyCif": "",
      "companyCp": "",
      "companyEmployees": "",
      "companyCnae": "",
      "companyId": "",
      "subjects": [],
      "reason": "",
      "source": "",
      "numeration": "",
      "organization": ""
    }
    this.isUserLoggedCoordinator = false
    this.buttonsPresent = true
    this.hasSubjects = false
    this.owner = false
    this.organization = false
    this.warningEmployees = false
    this.downloadUrl = this.generateDownloadUrl()
  }

  generateDownloadUrl() {
    const host = process.env.API_HOST
    const port = process.env.API_PORT
    const endpoint = '/api/download/'

    return 'https://' + host + ':' + port + endpoint
  }
}
