import Model from './model'

export default class Subjects extends Model {
  constructor() {
    super()
    this.labels = {
      "proposals": "",
      "analysis": "",
      "edit": "",
      "addSubject": "",
      "createSubject": "",
      "subjectsSolicitude": "",
      "subjectsList": "",
      "applicant": "",
      "company": "",
      "topics": "",
      "notApply": "",
      "placeholderAnalysis": "",
      "max200Words": "",
      "discardButtonSubject": "",
      "applicantName": "",
      "applicantSurname": "",
      "applicantCcaa": "",
      "applicantEmail": "",
      "applicantPhonenumber": "",
      "date": "",
      "time": "",
      "created": "",
      "createdTime": "",
      "text": "",
      "companyName": "",
      "companyCif": "",
      "companyEmployees": "",
      "companyCnae": "",
      "subject": "",
      "description": "",
      "project": "",
      "placeholderdescription": "",
      "comments": "",
      "reason": "",
      "closeCounseling": "",
      "source": "",
      "discard": "",
      "deleteSubject": "",
      "required": "",
      "notJustifiable": "",
      "newSubject": "",
      "requiredForSubject": "",
      "sharedDataInfo": "",
      "analisisInstructions": "",
      "sharedData": "",
      "confirmDeleteSubject": "",
      "files": "",
      "suggestChanges": "",
      "incompleteRecord": "",
      "organization": "",
      "maxSized": "",
      "requiredText": ""
    }
    this.values = {
      "solicitudeId": "",
      "proposals": [],
      "analysis": "",
      "subjectId":"",
      "topics": "",
      "selectedTopics": [],
      "id": "",
      "applicantName": "",
      "applicantSurname": "",
      "applicantCcaa": "",
      "applicantEmail": "",
      "applicantPhonenumber": "",
      "date": "",
      "time": "",
      "text": "",
      "companyName": "",
      "companyCif": "",
      "companyEmployees": "",
      "companyCnae": "",
      "subjects": [],
      "description": "",
      "comments": "",
      "reason": "",
      "created": "",
      "createdTime": "",
      "closed": "",
      "source": "",
      "project": {},
      "numeration": "",
      "user_id": "",
      "files": [],
      "shared_destination": ""
    }
    this.isSubmittable = false
    this.topicsCatalog = []
    this.reasonsCatalog = []
    this.proposalsCatalog = []
    this.projectsCatalog = []
    this.origin = "none"
    this.warningSubject = true
    this.owner = true
    this.organization = true
    this.warningEmployees = false
    this.isEditionMode = true
    this.showCloseSubjectModal = false
  }
}
