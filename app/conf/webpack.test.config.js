const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const PermissionsOutputPlugin = require('webpack-permissions-plugin')
const syntaxDynamicImportPlugin = require('@babel/plugin-syntax-dynamic-import')
const path = require('path')
const nodeEnv = process.env.NODE_ENV || 'production'

module.exports = {
  mode: 'development',
  devtool: 'inline-cheap-module-source-map',
  entry: {
    main: [
        '@babel/polyfill', 
        'core-js/modules/es6.promise',
        'core-js/modules/es6.array.iterator',
        './src/js/main.js',
    ],
    login: [
      './src/js/login.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, '../public/dist'),
    publicPath: '/dist/',
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    libraryTarget: 'var',
    library: 'Asesora'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          plugins: [syntaxDynamicImportPlugin.default]
        }
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.EnvironmentPlugin({
      'API_HOST': 'api',
      'API_PORT': '4567'
    }),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
    }),
    new PermissionsOutputPlugin({
      buildFolders: [
        {
          path: path.resolve(__dirname, '../public/dist/'),
          fileMode: '777',
          dirMode: '666'
        }
      ]
    })
  ],
  node: {
    process: false,
    Buffer: false
  }
}