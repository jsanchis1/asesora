const FIXTURES_URI = 'http://api:4567/fixtures/'

class Fixtures {
  constructor(){
    this.fixtures = {}
    this.totalSubjects = 3
    this.myOrganizationSubjects = 3
    this.otherOrganizationsSubjects = this.totalSubjects - this.myOrganizationSubjects
    this.mySubjects = 2
    this.myOrganizationSubjectsLessMine = this.myOrganizationSubjects - this.mySubjects
    this.mySolicitudes = 3
    this.myOrganizationSolicitudesLessMine = 1
    this.totalSolicitudes = 4
  }

  static pristine() {
    this.hit('pristine')
  }

  static clean() {
    this.hit('clean')
  }

  solicitudes(index){
    return this.fixtures[index]
  }

  static hit(endpoint) {
    cy.request('POST', FIXTURES_URI + endpoint).then((response) => {
      expect(response.status).to.eq(200)
    })
  }

  retrieve(){
    const endpoint = 'pristine'
    cy.request('POST', FIXTURES_URI + endpoint).then((response) => {
      expect(response.status).to.eq(200)
      this.fixtures = response.body.fixtures
    })
  }
}

export default Fixtures
