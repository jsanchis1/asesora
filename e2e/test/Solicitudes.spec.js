import Solicitudes from './pages/Solicitudes'
import Fixtures from './Fixtures'

describe('The solicitudes list', () => {
  const fixtures = new Fixtures()
  beforeEach(() => {
    Fixtures.pristine()
  })

  after(() => {
    Fixtures.clean()
  })

  xit('can filter my solicitudes', () => {
    const solicitudes = new Solicitudes()

    expect(solicitudes.count(fixtures.mySolicitudes)).to.eq.true
  })

  xit('can filter my organization solicitudes less mine', () => {
    const solicitudes = new Solicitudes().filterByMyOrganizationLessMine()

    expect(solicitudes.count(fixtures.myOrganizationSolicitudesLessMine)).to.eq.true
  })

  xit('can filter my organization solicitudes with mine', () => {
    const solicitudes = new Solicitudes().filterByMyOrganizationWithMine()

    expect(solicitudes.count(fixtures.totalSolicitudes)).to.eq.true
  })
})
