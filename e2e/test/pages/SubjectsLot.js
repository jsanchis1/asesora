const SUBJECTS_PATH = '/#/subjects-lot'
const SHOW_SUBJECT_CLASS = '.solicitude-show-button'

export default class SubjectsLot {
  constructor() {
    cy.visit(SUBJECTS_PATH)
    cy.server()
    cy.route('POST', '**/retrieve-solicitude').as('retrieveSolicitude')
  }

  showFirst() {
    cy.get(SHOW_SUBJECT_CLASS).first().click({force: true})
    cy.wait('@retrieveSolicitude')
    return this
  }

  includes(text) {
    cy.contains(text)

    return true
  }

  isSummaryShown(solicitude_id) {
    cy.hash().should('eq', '#/show-solicitude/' + solicitude_id)

    return true
  }

}