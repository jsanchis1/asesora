import SubjectsLot from './pages/SubjectsLot'
import Fixtures from './Fixtures'

const fixtures = new Fixtures()

describe('Subjects Lot', () => {
  beforeEach(() => {
    fixtures.retrieve()
  })

  after(() => {
    Fixtures.clean()
  })

  it('displays a list of subjects', () => {
    const subjectsLot = new SubjectsLot()
    const company_name = fixtures.solicitudes(0).company_name

    expect(subjectsLot.includes(company_name)).to.eq.true
  })

  it('opens sumary view', () => {
    const subjectsLot = new SubjectsLot()
    const solicitude_id = fixtures.solicitudes(1).creation_moment

    subjectsLot.showFirst()
    expect(subjectsLot.isSummaryShown(solicitude_id)).to.eq.true
  })

})
