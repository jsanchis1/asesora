import Solicitudes from './pages/Solicitudes'
import Solicitude from './pages/Solicitude'
import Fixtures from './Fixtures'

const fixtures = new Fixtures()

describe('Companies', () => {
  beforeEach(() => {
    fixtures.retrieve()
  })

  after(() => {
    Fixtures.clean()
  })

  it('can by filled with a suggestion by CIF', () => {
    const companyName = fixtures.solicitudes(0).company_name
    const companyCIF = fixtures.solicitudes(0).company_cif

    const solicitude = new Solicitude()
      .fill().companyCif(companyCIF)
      .selectFirstCompanySuggestion()

    expect(solicitude.hasCompanyName(companyName)).to.eq.true
  })

  it('can by filled with a suggestion by partial name', () => {
    const companyName = fixtures.solicitudes(0).company_name
    const partialName = companyName.substring(0,4)

    const solicitude = new Solicitude()
      .fill().companyName(partialName)
      .selectFirstCompanySuggestion()

    expect(solicitude.hasCompanyName(companyName)).to.eq.true
  })
})
