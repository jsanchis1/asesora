import Solicitudes from './pages/Solicitudes'
import Solicitude from './pages/Solicitude'
import Fixtures from './Fixtures'

const fixtures = new Fixtures()

describe('A solicitude', () => {
  beforeEach(() => {
    Fixtures.pristine()
    fixtures.retrieve()
  })

  after(() => {
    Fixtures.clean()
  })

  it('can be created with required fields', () => {
    const solicitude = new Solicitude().fillRequired().submit()

    expect(solicitude.hasConfirmationMessage()).to.eq.true
  })

  it('can be created with all fields', () => {
    const company = 'Sample name'
    const solicitude = new Solicitude()
      .companyName(company)
      .fillAll()
      .submit()

    expect(solicitude.includes(company)).to.eq.true
  })

  xit('can created a closed subject', () => {
    const solicitudes = new Solicitudes()
      .editFirst()
      .startSubject()
      .fillBasicSubject()
      .closeSubject()

    expect(solicitudes.includes('Motivo de cierre')).to.eq.true
  })

  it('can be removed', () => {
    const solicitudes = new Solicitudes()
      .editFirst()
      .remove()

    expect(solicitudes.hasBeenDecreased()).to.eq.true
  })

  it('can be not justified', () => {
    const solicitudes = new Solicitudes()

    expect(solicitudes.notContaisJustified()).to.eq.true
  })

  it('can be justified', () => {
    new Solicitudes()
      .editFirst()
      .makeItJustifable()

    const solicitudes = new Solicitudes()
    cy.reload(true)
    expect(solicitudes.contaisJustified()).to.eq.true
  })

  it('can be edited', () => {
    const editedText = 'Edited text'

    const solicitudes = new Solicitudes()
      .editFirst()
      .fill().description(editedText)
      .confirmChanges()
      .showFirst()

    expect(solicitudes.includes(editedText)).to.eq.true
  })

  it('shows validation alerts when has invalid information', () => {
    const solicitude = new Solicitude()
      .applicantPhonenumber('1234')
    expect(solicitude.isShowingPhoneAlert()).to.eq.true

      solicitude.companyCif('1234')
    expect(solicitude.isShowingCompanyAlerts()).to.eq.true
  })

  it('can not be created without information', () => {

    const solicitude = new Solicitude()

    expect(solicitude.canNotBeCreated()).to.eq.true
  })

  it('dont show edit button when is not my solicitude', () => {
    const solicitudes = new Solicitudes()
      .filterByMyOrganizationLessMine()

    expect(solicitudes.hasNotEditButton()).to.eq.true
  })

  it('allow only to the owner edit the solicitude', () => {
    const showSolicitudeTitle = 'Datos de la solicitud nº:'
    const otherUserSolicitude = fixtures.solicitudes(3)

    cy.visit('/#/solicitude/' + otherUserSolicitude.creation_moment)

    const solicitude = new Solicitude()
    expect(solicitude.includes(showSolicitudeTitle))
  })
})
