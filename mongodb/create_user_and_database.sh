#!/bin/bash
echo "Creating an user with privileges in the new database"
mongo -u $MONGO_INITDB_ROOT_USERNAME \
      -p $MONGO_INITDB_ROOT_PASSWORD \
      --authenticationDatabase $MONGO_INITDB_DATABASE \
      --eval  "db.getSiblingDB('$DDBB_NAME')
                 .createUser(
                    {user: '$MONGODB_USER',
                     pwd:'$MONGODB_PSSW',
                     roles:
                        [
                          {role:'readWrite',db:'$DDBB_NAME'}
                        ]
                });"
