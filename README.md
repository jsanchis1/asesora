# Asesora

## Start project

~~~
make start
~~~
To access the `API` use `localhost:4567`, and to acces the `APP` use `localhost:8080`.

## Start project with graphic cypress linux

We have a differen docker-compose for linux because we can run the tests e2e in a graphic way using `make run-test-e2e-graphic`

~~~
make start-linux
~~~

### Build-watch

To see the changes in app:

~~~
make build-watch-app
~~~

## Launch API tests

~~~
make run-test-api
~~~

## Launch APP tests

~~~
make run-test-app
~~~

## Launch Cypress E2E tests

~~~
make run-test-e2e
~~~

> If you are running the project with the `make start-linux` and are using linux. You can run the e2e test with a graphic cypress `make run-test-e2e-graphic`. WARNING: This only works in linux.

# Templates

## Styles

Demo template available at the project in folder `/app/templates/medialot`

## Favicon

Template in format svg at `/app/templates/favicon`


# Connect with the server

If you are not sure if you have a public key into droplet of Digital Ocean (4), try to enter:

~~~
ssh root@ip.droplet
~~~

This create a .ssh folder and know_hosts in your computer (client side).

Or you try to create the folder with:

~~~
mkdir ~/.ssh
~~~


## Create new key for enter with ssh

Enter in the .ssh folder and create a new pair of keys (public and private) for ssh connection to droplet:

~~~
cd ~/.ssh
ssh-keygen -t rsa
~~~

And change de permission of the folder and file:

~~~
chmod 700 ~/.ssh
chmod 600 ~/.ssh/id_rsa
~~~

Remember: only change the permission of the private key (this not have a _.pub_ extension).


## Copy your public key to the droplet

You need to have access rights as an administrator in order to upload your key to the server.


### With root password

~~~
cd ~/.ssh
ssh-copy-id -i ~/.ssh/id_rsa root@ip.droplet
~~~

In this moment we urge you to indicate the password of root in droplet.


### Or, send your public key to an administrator
The administrator must copy with 'scp' the key:
~~~
scp -i ~/.ssh/id_rsa new_admin_user_key.pub root@ip.droplet:/root/new_admin_user_key.pub
~~~
Then, with 'ssh', the administrator add the key to authorized keys:
~~~
ssh -i ~/.ssh/id_rsa root@ip.droplet "cat /root/new_admin_user_key.pub >> /root/.ssh/authorized_keys"
~~~
And finally, the administrator removes the uploaded file:
~~~
ssh -i ~/.ssh/id_rsa root@ip.droplet "rm /root/new_admin_user_key.pub"
~~~

## Connect into droplet with ssh and a public key
~~~
ssh -i ~/.ssh/id_rsa root@ip.droplet
~~~

# Managing the server
The server is managed with recipes made with Ansible. All recipes are launched from the "deploy" folder.

You can test if you have a connection by performing the following test:
~~~
ansible -i ./host_digitalocean digitalocean -m ping
~~~

If the name of the key is not the default one (~/.ssh/id_rsa) you must indicate the correct one in the file ```host_digitalocean```.

## Secrets
You must provide secrets_staging.yml, and secrets_production files for each environment in `provision` folder containing:
~~~
---
- mongo_user: 'an_user_for_the_database_access'
- mongo_pssw: 'the_password_for_the_user'
- mongo_admin: 'administrator'
- mongo_admin_pssw: 'administrator_password'
- secret: 'a_secret_word_for_tokenizers'
~~~
You must complete the information necessary for the implementation to perform the tasks correctly.

## Server installation
There is a recipe for server provisioning:
~~~
ansible-playbook -i ./host_digitalocean install_server.yml
~~~
Server host must solve dependencies **before** it can run ansible
Dependencies: python, python-apt

## Deploy application
To build and deploy, run the recipe limiting it to desired host:
~~~
ansible-playbook ./deploy.yml -l staging
ansible-playbook ./deploy.yml -l production
~~~

## Backups
Manual full backup to local:
~~~
ansible-playbook backup.yml -l staging
ansible-playbook backup.yml -l production
~~~

To recover it:
~~~
ansible-playbook -i ./host_digitalocean restore_ddbb_asesora.yml
~~~

### MongoDB remote host administration
To start MongoDB CL, log into remote host with ssh and type the command below:
~~~
mongo --port 27017 -u "your_admin_user" -p "your_admin_password" --authenticationDatabase "admin"
~~~

To drop a database:
~~~
mongo "ddbb_to_drop" --port 27017 -u "your_admim_user" -p "your_admin_password" --authenticationDatabase "admin" --eval "db.dropDatabase()"
~~~

# Notes
  1. https://www.docker.com/
  2. https://docs.docker.com/compose/
  3. https://www.ansible.com/
  4. It is interesting that you read the Digital Ocean documentation regarding the use of services in Debian:
https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units
