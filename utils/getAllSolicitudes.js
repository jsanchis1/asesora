// Helper script to create a csv report of solicitudes collection
// sample of shell execution in remote host (assumes .mongorc.js is setted up with
// valid credentials on user's home directory in remote server)
// ssh user@server 'mongo --quiet ' < ./getAllSolicitudes.js > report.csv

const headers = [
  'solicitude_date',
  'user_id'
]
const header = [headers.join('\t')]
// const solicitudes = db.solicitudes.find().aggregate([
//   {
//     $lookup: {
//       from: applicant,
//       localField: applicant,
//       foreingFields: id,
//       as: applicant_data
//     }
//   }
// ])

const solicitudes = db.solicitudes.find({}, { date: 1, user: 1 })
const serializedSolicitudes = solicitudes.map(
  solicitude => `${solicitude.date}\t${solicitude.user.user_id}`
)

([header, ...serializedSolicitudes].forEach( row => print(row)))

// Raw json output of same query
// printjson( solicitudes.toArray() );
