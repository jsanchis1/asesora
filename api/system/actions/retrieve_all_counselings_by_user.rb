require_relative '../services/solicitudes/service'
require_relative '../services/organizations/service'
require_relative '../services/catalogs/service'
require_relative '../services/validations/service'
require_relative './mergeable'
require_relative './subjects_mapper'

module Actions
  class RetrieveAllCounselingsByUser
    extend Mergeable
    extend SubjectsMapper

    def self.by_user(user_id)
      solicitudes = ::Solicitudes::Service.all_by_user(user_id)

      extract_subjects_from(solicitudes, user_id)
    end

    def self.by_domain_less(user_id)
      solicitudes = ::Solicitudes::Service.all_by_domain_less(user_id)

      subjects = extract_subjects_from(solicitudes, user_id)
      subjects
    end

    def self.by_domain(user_id)
      domain = ::Organizations::Service.domain_in(user_id)
      solicitudes_by_domain = ::Solicitudes::Service.all_by_domain(domain)
      subjects = extract_subjects_from(solicitudes_by_domain, user_id)

      subjects
    end

    def self.less_my_domain(user_id)
      solicitudes = ::Solicitudes::Service.all_less_my_domain_less(user_id)
      subjects = extract_subjects_from(solicitudes, user_id)

      SubjectsMapper.all_less_my_domain_less(subjects)
    end

    def self.all_subjects(user_id)
      domain = ::Organizations::Service.domain_in(user_id)

      solicitudes_by_domain = ::Solicitudes::Service.all_by_domain(domain)
      all_solicitudes = ::Solicitudes::Service.all_less_my_domain_less(user_id)
      my_subjects = extract_subjects_from(solicitudes_by_domain, user_id)

      all_subjects = extract_subjects_from(all_solicitudes, user_id)
      all_subjects_less_domain = SubjectsMapper.all_less_my_domain_less(all_subjects)

      subjects = my_subjects.push(all_subjects_less_domain)

      subjects.flatten
    end

    def self.extract_subjects_from(solicitudes, logged)
      isCoordinator = ::Catalogs::Service::user_coordinator?(logged)
      result = []
      solicitudes.map do |solicitude|
        applicant = ::Applicant::Service.retrieve(solicitude['applicant'])
        company = ::Companies::Service.retrieve(solicitude['company'], solicitude['edition_moment'])
        subjects = ::Subjects::Service.all_by(solicitude['creation_moment'])

        id_ccaa = applicant['ccaa']['value']
        applicant['ccaa_feprl'] = get_FEPRL_ccaa(id_ccaa)

        id_cnae = ""

        if company['cnae']
          id_cnae = company['cnae'].split(' ')[0]
        end

        company['cnae_feprl'] = get_FEPRL_cnae(id_cnae)

        owner = solicitude["user"]["user_id"]

        subjects.map do |subject|
          item = {}
          item = add_with_prefix(item, solicitude, "solicitude")
          item = add_with_prefix(item, applicant, "applicant")
          item = add_with_prefix(item, company, "company")
          item = add_with_prefix(item, subject, "subject")
          item["owner"] = addOwner(owner, logged, isCoordinator)
          result << item
        end
      end
      result
    end

    private

    def self.get_FEPRL_ccaa(id)
      ::Catalogs::Service.get_FEPRL_catalog("ccaa", id)
    end

    def self.get_FEPRL_cnae(id)
      ::Catalogs::Service.get_FEPRL_catalog("cnae", id)
    end

    def self.addOwner(owner, logged, isCoordinator)
        result = ""
        if ::Validations::Service.show_owner?(owner, logged, isCoordinator)
          result = owner
        end
        result
    end
  end
end
