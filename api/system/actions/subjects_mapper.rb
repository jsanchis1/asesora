module SubjectsMapper
  def self.all_less_my_domain_less(subjects)
    subjects.map do |subject|
      {
        solicitude_numeration: subject['solicitude_numeration'],
        solicitude_date: subject['solicitude_date'],
        solicitude_text: subject['solicitude_text'],
        company_employees: subject['company_employees'],
        company_cnae: subject['company_cnae'],
        subject_created: subject['subject_created'],
        subject_createdTime: subject['subject_createdTime'],
        subject_topics: subject['subject_topics'],
        subject_analysis: subject['subject_analysis'],
        subject_proposal: subject['subject_proposal'],
        subject_project: subject['subject_project'],
        subject_description: subject['subject_description'],
        subject_reason: subject['subject_reason'],
        subject_numeration: subject['subject_numeration'],
        subject_comments: subject['subject_comments'],
        owner: subject['owner']
      }
    end
  end
end
