require_relative '../services/solicitudes/service'
require_relative '../services/companies/service'

module Actions
  class CountCompanyInSolicitudes
    NO_MATCHES = 0

    def self.do(cif:'')
      quantity = NO_MATCHES
      companies = ::Companies::Service.all_by({'cif' => cif})

      unless companies.empty?
        company = companies.first
        quantity = ::Solicitudes::Service.times_company(company['id'])
      end

      {data: quantity}
    end
  end
end
