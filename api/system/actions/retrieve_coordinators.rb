require_relative '../services/catalogs/service'

module Actions
  class RetrieveCoordinators
    def self.do(*_)
      ::Catalogs::Service.retrieve_coordinators
    end
  end
end
