require_relative '../services/catalogs/service'

module Actions
  class UpdateCatalog
    def self.do(new_value:, old_value:, catalog:)
      ::Catalogs::Service.update_catalog(new_value, old_value, catalog)
    end
  end
end
