require_relative '../services/organizations/service'
require_relative '../services/solicitudes/service'
require_relative '../services/companies/service'
require_relative '../services/applicant/service'
require_relative '../services/subjects/service'
require_relative '../services/attachments/service'

module Actions
  class CreateSolicitude
    class << self
      def do(date:, time:, text:, source:, name:, surname:, email:, phonenumber:, ccaa:, id:, company_name:, company_cif:, company_employees:, company_cnae:, company_cp:, user_id:, domain:, company_id:, organization:, subjects:)
        if ( id == "" )
          applicant = create_applicant(name, surname, email, phonenumber, ccaa)
        else
          applicant = retrieve_applicant(name, surname, email, phonenumber, ccaa, id)
        end

        company = create_company(company_name, company_cif, company_employees, company_cnae, company_cp, company_id)

        create_organization_for(user_id)
        origin_subject_id = nil

        solicitude = create_solicitude(date, time, text, source, applicant, company, user_id, domain, organization, origin_subject_id)

        solicitude['subjects'] = create_subjects(solicitude['creation_moment'], subjects, user_id)

        solicitude
      end

      private

      def create_organization_for(email)
        ::Organizations::Service.create(email)
      end

      def create_applicant(name, surname, email, phonenumber, ccaa)
        ::Applicant::Service.create(
          name,
          surname,
          email,
          phonenumber,
          ccaa
        )
      end

      def retrieve_applicant(name, surname, email, phonenumber, ccaa, id)
        ::Applicant::Service.retrieve_with_id(
          name,
          surname,
          email,
          phonenumber,
          ccaa,
          id
        )
      end

      def create_company(name, cif, employees, cnae, cp, id)
        ::Companies::Service.create(
          name,
          cif,
          employees,
          cnae,
          cp,
          id
        )
      end

      def create_solicitude(date, time, text, source, applicant, company, user_id, domain, organization, origin_subject_id)
        company_id = company["id"]
        applicant_id = applicant["id"]

        numeration = next_numeration(user_id)

        ::Solicitudes::Service.create(
          date,
          time,
          text,
          source,
          applicant_id,
          company_id,
          user_id,
          domain,
          numeration,
          organization,
          origin_subject_id
        )
      end

      def next_numeration(email)
        numeration = ::Organizations::Service.next_solicitude_numeration(email)

        numeration
      end

      def create_subjects(id, subjects, email)
        return [] unless subjects
        subjects.map do | subject |
          create_subject(
            solicitude_id: id,
            proposal: subject['proposal'],
            description: subject['descripion'],
            analysis: subject['analysis'],
            topics: subject['topics'],
            created: subject['created'],
            createdTime: subject['createdTime'],
            project: subject['project'],
            files: subject['files'],
            email: email,
            reason: subject['reason'],
            comments: subject['comments']
          )
        end
      end

      def create_subject(solicitude_id:, proposal:, description:, analysis:, topics:, created:, createdTime:, project:, files:, email:, reason:, comments:)
        numeration = next_subject_numeration(email)
        subject = ::Subjects::Service.create(solicitude_id, proposal, description, analysis, topics, created, createdTime, project, numeration)

        if (!reason.empty? || !comments.empty? )
          subject = close_subject(subject, reason, comments)
        end

        subject['files'] = add_files(subject['id'], files)

        subject
      end

      def add_files(subject_id, files)
        return [] unless files

        files.map do |file|
          ::Attachments::Service.create(file, subject_id)
        end
      end

      def close_subject(subject, reason, comments)
        closed = nil
        ::Subjects::Service.close(subject, subject['id'], reason, comments, closed, subject['created'], subject['createdTime'])
      end

      def next_subject_numeration(email)
        numeration = ::Organizations::Service.next_subject_numeration(email)

        numeration
      end

    end
  end
end
