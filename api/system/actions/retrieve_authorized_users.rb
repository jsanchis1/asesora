require_relative '../services/catalogs/service'

module Actions
  class RetrieveAuthorizedUsers
    def self.do(*_)
      ::Catalogs::Service.authorized_users
    end
  end
end
