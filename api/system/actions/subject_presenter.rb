require_relative './mergeable'

module Actions
  class SubjectPresenter

    include Mergeable

    def initialize(solicitude, subject, applicant, company)
      @solicitude = solicitude
      @subject = subject
      @applicant = applicant
      @company = company
    end

    def serialize
      prepared_solicitude = add_with_prefix(@solicitude, @company,'company')
      {
        solicitude_id: prepared_solicitude["creation_moment"],
        date: prepared_solicitude["date"],
        company_name: prepared_solicitude["company_name"],
        subject_id: @subject["id"],
        topics: @subject["topics"],
        proposal: @subject["proposal"],
        description: @subject["description"],
        analysis: @subject["analysis"],
        ccaa: @applicant["ccaa"],
        source: prepared_solicitude["source"],
        employees: prepared_solicitude["company_employees"],
        cnae: prepared_solicitude["company_cnae"],
        cp: prepared_solicitude["company_cp"],
        project: @subject["project"],
        user_id: @solicitude["user"]["user_id"],
        numeration: @subject["numeration"],
        solicitude_numeration: @solicitude["numeration"],
        closed: @subject["closed"],
        created: @subject["created"],
        createdTime: @subject["createdTime"],
        reason: @subject["reason"],
        files: @subject['files'],
        shared_destination: @subject['shared_destination']
      }
    end
  end
end
