require_relative '../services/subjects/service'
require_relative '../services/attachments/service'

module Actions
  class DeleteSubject
    def self.do(id:)
      subject = ::Subjects::Service.retrieve(id)
      return "500" if subject == {}

      ::Attachments::Service.delete_for(subject["id"])

      ::Subjects::Service.delete(id)
    end
  end
end
