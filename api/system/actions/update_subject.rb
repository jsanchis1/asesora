require_relative '../services/subjects/service'
require_relative '../services/attachments/service'

module Actions

  class UpdateSubject
    class << self
      def do(solicitude_id:, id:, proposal:, description:, analysis:, topics:, created:, createdTime:, project:, numeration:, files:)
        subject = ::Subjects::Service.update(solicitude_id, id, proposal, description, analysis, topics, created, createdTime, project, numeration)
        add_files(subject['id'], files)
        subject['files'] = attachments_for(subject['id'])

        subject
      end

      private

      def add_files(subject_id, files)
        if files
          files.each do |file|
            ::Attachments::Service.create(file, subject_id)
          end
        end
      end

      def attachments_for(subject_id)
        ::Attachments::Service.retrieve(subject_id)
      end
    end
  end
end
