require_relative '../services/catalogs/service'

module Actions
  class RetrieveProjects
    def self.do(*_)
      ::Catalogs::Service.projects
    end
  end
end
