require_relative '../services/catalogs/service'

module Actions
  class AddItemToCatalog
    def self.do(new_value:, catalog:)
      ::Catalogs::Service.add_item_to_catalog(new_value, catalog)
    end
  end
end
