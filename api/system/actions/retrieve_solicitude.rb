require_relative '../services/solicitudes/service'
require_relative '../services/companies/service'
require_relative '../services/applicant/service'
require_relative './solicitude_presenter'

module Actions
  class RetrieveSolicitude
    def self.do(id:)
      solicitude = ::Solicitudes::Service.retrieve(id)
      return {} if solicitude.empty?

      company = ::Companies::Service.retrieve(solicitude['company'], solicitude['edition_moment'])
      applicant = ::Applicant::Service.retrieve(solicitude['applicant'])
      subjects = ::Subjects::Service.all_by(id)
      subjects.map do |subject|
        attachments = ::Attachments::Service.retrieve(subject['id'])
        subject["files"] = attachments
      end
      meetsFeprl = ::Solicitudes::Service.checksFeprl(solicitude["date"], company, applicant)

      return SolicitudePresenter.new(solicitude, company, applicant, subjects, meetsFeprl).serialize
    end
  end
end
