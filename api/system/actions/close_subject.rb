require_relative '../services/subjects/service'
require_relative '../services/attachments/service'

module Actions
  class CloseSubject
    class << self
      def do(id:, solicitude_id:, proposal:, description:, analysis:, topics:, reason:, comments:, closed:, created:, createdTime:, project:, numeration:'', files:)
        if id.empty?
          subject = create(solicitude_id, proposal, description, analysis, topics, created, createdTime, project)
        else
          subject = update(solicitude_id, id, proposal, description, analysis, topics, created, createdTime, project, numeration)
        end

        subject = ::Subjects::Service.close(subject, subject['id'], reason, comments, closed, created, createdTime)
        add_files(subject['id'], files)
        subject['files'] = attachments_for(subject['id'])

        subject
      end

      private

      def add_files(subject_id, files)
        return unless files

        files.each do |file|
          ::Attachments::Service.create(file, subject_id)
        end
      end

      def attachments_for(subject_id)
        ::Attachments::Service.retrieve(subject_id)
      end

      def is_not?(closed)
        closed.nil? || closed.empty?
      end

      def create(solicitude_id, proposal, description, analysis, topics, created, createdTime, project)
        email = user_email(solicitude_id)
        numeration = next_numeration(email)
        ::Subjects::Service.create(
          solicitude_id,
          proposal,
          description,
          analysis,
          topics,
          created,
          createdTime,
          project,
          numeration
        )
      end

      def update(solicitude_id, id, proposal, description, analysis, topics, created, createdTime, project, numeration)
        ::Subjects::Service.update(
          solicitude_id,
          id,
          proposal,
          description,
          analysis,
          topics,
          created,
          createdTime,
          project,
          numeration
        )
      end

      def user_email(solicitude)
        solicitude = ::Solicitudes::Service.retrieve(solicitude)
        email = solicitude['user']['user_id']

        email
      end

      def next_numeration(email)
        numeration = ::Organizations::Service.next_subject_numeration(email)

        numeration
      end
    end
  end
end
