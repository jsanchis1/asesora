require_relative '../services/subjects/service'
require_relative '../services/attachments/service'

module Actions
  class RemoveClosedStatus
    class << self
      def do(subjectId:)
        subject = ::Subjects::Service.remove_closed_status(subjectId)

        subject['files'] = attachments_for(subject['id'])

        subject
      end

      private

      def attachments_for(subject_id)
        ::Attachments::Service.retrieve(subject_id)
      end
    end
  end
end
