ARRAY_CATALOGS = ["projects", "proposals"]
PAIR_OF_DICTIONARY_CATALOG = ["cnae", "organizations"]
HASH_CATALOG = ["ccaa", "close_reasons", "source_formats"]
TOPICS_CATALOG = ["topics"]

module Catalogs
  class Service
    class << self
      def update_catalog(new_value, old_value, catalog_name, dry_run = false)
        catalog = read(catalog_name + '.json')

        if read_array(catalog_name)
          catalog = update_array_catalogs(new_value, old_value, catalog)
        end
        if read_hash(catalog_name)
          catalog = update_hash_catalog(new_value, old_value, catalog)
        end

        if read_pair_of_dictionary(catalog_name)
          catalog = update_dictionary_catalog(new_value, old_value, catalog)
        end

        if read_topics(catalog_name)
          catalog = update_topics_catalog(new_value, old_value, catalog)
        end

        write(catalog_name + '.json', catalog) unless dry_run

        result = {name: catalog_name, content: catalog}
        result
      end

      def add_item_to_catalog(new_value, catalog_name, dry_run = false)
        catalog = read(catalog_name + '.json')

        if read_topics(catalog_name)
          id = next_numeration(catalog.size)
          item = {"id" => new_value['id'], "name" => new_value['name']}
          catalog << item unless includes_in?(catalog, item)
        end

        if read_hash(catalog_name)
          id = next_numeration(catalog.size)
          item = {"id" => '%02d' % id, "name" => new_value}
          catalog << item unless includes_in?(catalog, item)
        end

        if read_array(catalog_name)
          item = new_value
          catalog << item unless catalog.include?(item)
        end

        if read_pair_of_dictionary(catalog_name)
          item = {"name" => new_value}
          catalog << item unless includes_in?(catalog, item)
        end
        
        write(catalog_name + '.json', catalog) unless dry_run

        result = {name: catalog_name, content: catalog}
        result
      end

      def next_numeration(number)
        number += 1
      end

      def get_FEPRL_catalog(catalog_name, id)
        result = ""
        catalog = read(catalog_name + '.json')
        if read_hash(catalog_name)
          result = get_FEPRL_hash_catalog(catalog, id)
        end

        if read_pair_of_dictionary(catalog_name)
          result = get_FEPRL_dictionary_catalog(catalog, id)
        end

        result
      end

      def close_reasons
        {
          name: 'close_reasons',
          content: read("close_reasons.json")
        }
      end

      def cnae
        {
          name: 'cnae',
          content: read("cnae.json")
        }
      end

      def topics
        {
          name: 'topics',
          content: read("topics.json")
        }
      end

      def ccaa
        {
          name: 'ccaa',
          content: read("ccaa.json")
        }
      end

      def proposals
        {
          name: 'proposals',
          content: read("proposals.json")
        }
      end

      def source_formats
        {
          name: 'source_formats',
          content: read("source_formats.json")
        }
      end

      def authorized_users
        {
          name: 'authorized_users',
          content: read("authorized_users.json")
        }
      end

      def retrieve_coordinators
        coordinators
      end

      def coordinators
        {
          name: 'coordinators',
          content: read("coordinators.json")
        }
      end

      def projects
        {
          name: 'projects',
          content: read("projects.json")
        }
      end

      def organizations
        {
          name: 'organizations',
          content: read("organizations.json")
        }
      end

      def add_authorized_user(user)
        new_authorized_users = authorized_users[:content] | [user]
        update("authorized_users.json", new_authorized_users)
      end

      def add_coordinator(user)
        coordinators_list = coordinators[:content] | [user]
        update("coordinators.json", coordinators_list)
      end

      def remove_authorized_user(user)
        new_authorized_users = authorized_users[:content] - [user]
        update("authorized_users.json", new_authorized_users)
      end

      def remove_coordinator(user)
        coordinators_list = coordinators[:content] - [user]
        update("coordinators.json", coordinators_list)
      end

      def update_authorized_users(users)
        update("authorized_users.json", users)
      end

      def user_coordinator?(user_id)
        coordinators = read("coordinators.json")
        coordinators.include?(user_id)
      end
      private

      def update_array_catalogs(new_value, old_value, catalog)
        catalog.each_with_index do |element, key|
          if element == old_value
            catalog[key] = new_value
          end
        end
        catalog
      end

      def update_hash_catalog(new_value, old_value, catalog)
        catalog.each do |element|
          if element["name"] == old_value
            element["name"] = new_value
          end
        end
        catalog
      end

      def update_dictionary_catalog(new_value, old_value, catalog)
        catalog.each_with_index do |element, index|
          if element["name"] == old_value
            catalog[index]["name"] = new_value
          end
        end
        catalog
      end

      def update_topics_catalog(new_value, old_value, catalog)
        catalog.each_with_index do |element, index|
          if element["id"] == old_value["id"]
            catalog[index]["name"] = new_value
          end
        end
        catalog
      end

      def read_array(catalog_name)
        ARRAY_CATALOGS.include?(catalog_name)
      end

      def read_pair_of_dictionary(catalog_name)
        PAIR_OF_DICTIONARY_CATALOG.include?(catalog_name)
      end

      def read_topics(catalog_name)
        TOPICS_CATALOG.include?(catalog_name)
      end

      def read_hash(catalog_name)
        HASH_CATALOG.include?(catalog_name)
      end

      def write(file, content)
        from_path = File.dirname(__FILE__)
        collection_file = File.join(from_path, "collections", file)
        File.open(collection_file, 'w') do |file|
          file.write content.to_json
        end
      end

      def read(name)
        from_path = File.dirname(__FILE__)
        collection_file = File.join(from_path, "collections", name)
        catalog = File.read(collection_file, encoding: 'UTF-8')
        JSON.parse(catalog)
      end

      def update(file, value)
        from_path = File.dirname(__FILE__)
        collection_file = File.join(from_path, "collections", file)
        File.open(collection_file, 'w') do |file|
          file.puts value.to_json
        end
      end

      def get_FEPRL_hash_catalog(catalog, id)
        catalog.each_with_index do |element, index|
          if element["id"] == id
            return catalog[index]["feprl_name"]
          end
        end
        return ""
      end

      def get_FEPRL_dictionary_catalog(catalog, id)
        catalog.each_with_index do |element, index|
          if element["id"] == id
            return catalog[index]["feprl_name"]
          end
        end
        return ""
      end

      def includes_in?(catalog, new_item)
        matches = catalog.select do |item|
          item["name"] == new_item["name"]
        end

        return matches.size > 0
      end
    end
  end
end
