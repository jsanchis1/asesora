require_relative '../../infrastructure/clients'

module Organizations
  class Collection
    class << self
      def create(organization)
        MongoClient.insert(organization.serialize)
      end

      def retrieve(domain)
        document = MongoClient.find(domain)
        return Domain::Organization.as_null if document.nil?

        organization = Domain::Organization.from_document(document)

        organization
      end

      def update(organization)
        MongoClient.update(organization.serialize)
      end
    end
  end

  class MongoClient
    class << self
      def insert(document)
        collection.insert_one(document)
      end

      def find(domain)
        documents = collection.find({ 'domain' => domain })

        documents.first
      end

      def update(organization)
        collection.find_one_and_replace({ "id": organization['id'] }, organization)
      end

      private

      def collection
        client[:organizations]
      end

      def client
        @client ||= Infrastructure::Clients.mongo

        @client
      end
    end
  end
end
