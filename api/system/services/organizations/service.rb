require_relative '../../domain/organization'
require_relative '../../libraries/email'
require_relative 'collection'

module Organizations
  class Service
    class << self
      def create(email)
        domain = domain_in(email)
        organization = Collection.retrieve(domain)

        if organization.nil?
          organization = Domain::Organization.with(domain)

          Collection.create(organization)
        end

        organization.serialize
      end

      def next_subject_numeration(email)
        domain = domain_in(email)
        organization = Collection.retrieve(domain)

        numeration = organization.next_subject_numeration
        Collection.update(organization)

        numeration
      end

      def next_solicitude_numeration(email)
        domain = domain_in(email)
        organization = Collection.retrieve(domain)

        numeration = organization.next_solicitude_numeration
        Collection.update(organization)

        numeration
      end


      def domain_in(email)
        Email.extract_domain(email)
      end
    end
  end
end
