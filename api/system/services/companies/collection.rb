require_relative '../../infrastructure/clients'
require_relative '../../domain/company'
require_relative '../../domain/list'
require 'i18n'

module Companies
  class Collection
    class << self
      def create(company)
        memento = company.memento
        serialized = company.serialize()

        document = MongoClient.create(serialized)
        MongoClient.store(memento)
        Domain::Company.from_document(document)
      end

      def retrieve(id, timestamp = Time.now.to_i)
        document = MongoClient.retrieve(id)
        return Domain::Company.nullified if document.nil?
        company = Domain::Company.from_document(document)
        memento = MongoClient.retrieve_state_at(timestamp, company.identify)
        company.remind(memento) unless memento.nil?
        company
      end

      def delete(id)
        MongoClient.delete(id)
      end

      def all(criteria)
        return Domain::NullCompany.new if criteria[:name].nil?
        return Domain::NullCompany.new if criteria[:name].length < 3

        name = criteria[:name]
        cnae = criteria[:cnae]

        list = MongoClient.all(name)

        unless cnae.nil? || cnae.empty?
          list = list.select do |company|
            company["cnae"].include?(cnae)
          end
        end

        companies = list.select do |company|
          !company["cif"].nil? && !company["cif"].empty?
        end

        Domain::List.from_document(companies, Domain::Company)
      end

      def all_by(criteria)
        minimun_length = 3
        list = criteria.select do |field, value|
          !value.nil? && value.length >= minimun_length
        end
        return [] if list.empty?

        result = MongoClient.all_by(list)

        companies = result.map do |company|
          Domain::Company.from_document(company)
        end
        companies
      end

      def update(id, company)
        serialized = company.serialize()
        memento = company.memento

        document = MongoClient.update(id, serialized)

        return if document.nil?
        MongoClient.store(memento)
        Domain::Company.from_document(document)
      end

      private

      class MongoClient
        class << self

          def create(descriptor)
            client[:companies].insert_one(descriptor)
            descriptor
          end

          def store(memento)
            client[:company_memento].insert_one(memento)
          end

          def retrieve(id)
            documents = client[:companies].find({"id": id})
            documents.first
          end

          def retrieve_state_at(timestamp, id)
            memento = client[:company_memento]
                            .find({"timestamp":{"$lte": timestamp}, "signature": id})
                            .sort({"timestamp": -1})
                            .first
            memento
          end

          def delete(id)
            client[:companies].find_one_and_delete({"id": id})
          end

          def all(text)
            documents = client[:companies].find()

            selected = documents.select do |document|
              company_name = simplify(document['name'])
              simple_text = simplify(text)

              company_name.include?(simple_text)
            end

            selected
          end

          def all_by(criteria)
            list = {}
            criteria.each do |key, value|
              regex = /#{value}/i
              chain = { key => {"$regex": regex}}
              list.merge!(chain)
            end
            documents = client[:companies].find(list)
          end

          def update(id, company)
            document = client[:companies].find_one_and_replace({ "id": id }, company, :return_document => :after)

            document
          end

          private

          def client
            @client ||= Infrastructure::Clients.mongo
          end

          def simplify(text)
            downcased = text.downcase
            transliterated = I18n.transliterate(downcased)

            transliterated
          end
        end
      end
    end
  end
end
