require_relative '../../domain/company'
require_relative 'collection'

module Companies
  class Service
    def self.create(name, cif, employees, cnae, cp, id = nil)
      cif = cif.upcase if cif
      company = Domain::Company.with(name, cif, employees, cnae, cp, id)
      create_or_update(id, company)
    end

    def self.retrieve(id, edition_moment = nil)
      retrieved = Collection.retrieve(id, edition_moment)
      retrieved.serialize
    end

    def self.delete(id)
      Collection.delete(id)
    end

    def self.all(criteria)
      Collection.all(criteria).serialize
    end

    def self.all_by(criteria)
      companies = Collection.all_by(criteria)
      companies.map do |company|
        company.serialize
      end
    end

    def self.update(name, cif, employees, cnae, cp, id)
      cif = cif.upcase if cif
      company = Domain::Company.with(name, cif, employees, cnae, cp, id)
      create_or_update(id, company)
    end

    def self.create_or_update(id, company)
      retrieved = Collection.retrieve(id)

      unless retrieved.nil?
        Collection.update(id, company).serialize
      else
        Collection.create(company).serialize
      end
    end
  end
end
