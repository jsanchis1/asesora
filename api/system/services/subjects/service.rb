require_relative '../../domain/subject'
require_relative 'collection'

module Subjects
  class Service
    def self.create(solicitude_id, proposal, description,analysis, topics, created, createdTime, project, numeration)
      subject = Domain::Subject.with(solicitude_id, proposal, description, analysis, topics, created, createdTime, project, numeration)

      Collection.create(subject).serialize
    end

    def self.update(solicitude_id, id, proposal, description, analysis, topics, created, createdTime, project, numeration)
      subject = Collection.retrieve(id)
      subject.proposal = proposal
      subject.description = description
      subject.analysis = analysis
      subject.topics = topics
      subject.created = created
      subject.createdTime = createdTime
      subject.project = project
      subject.numeration = numeration

      collection = Collection.update(subject, id)
      return {} if collection.nil?

      collection.serialize
    end

    def self.retrieve(id)
      subject = Collection.retrieve(id)
      return {} if subject.nil?
      subject.serialize
    end

    def self.filter_by(criterial)
      subjects = Collection.filter_by(criterial)

      subjects.serialize
    end

    def self.close(subject, id, reason, comments, closed, createdTime)
      return Collection.retrieve(id).serialize if !(closed.nil? || closed.empty?)

      subject = Collection.retrieve(id)
      subject.reason = reason
      subject.comments = comments
      subject.closed = DateTime.now.strftime("%m/%d/%Y")
      subject.created = created
      subject.createdTime = createdTime

      updated_subject = Collection.update(subject, id)
      updated_subject.serialize
    end


    def self.add_destination(id, destination_solicitude_id, destination_solicitude_numeration)
      subject = Collection.retrieve(id)
      destination_solicitude = {id: destination_solicitude_id, numeration: destination_solicitude_numeration}
      subject.shared_destination = destination_solicitude

      updated_subject = Collection.update(subject, id)

      updated_subject.serialize
    end

    def self.delete(id)
      Collection.delete(id)
    end

    def self.all_by(solicitude_id)
      Collection.all_by(solicitude_id).serialize
    end

    def self.remove_all_by_solicitude(id)
      Collection.remove_all_by_solicitude(id)
    end

    def self.remove_closed_status(id)
      subject = Collection.retrieve(id)
      subject.reason = nil
      subject.comments = nil
      subject.closed = nil

      updated_subject = Collection.update(subject, id)
      updated_subject.serialize
    end
  end
end
