require 'jwt'
require_relative 'collection'

module Authorization
  class Service
    def self.token_for(payload)
      token = ''

      if Collection.exists?(payload['email'])
        token = generate_token_for(payload)
      end

      token
    end

    def self.valid_email(payload)
      Collection.exists?(payload['email'])
    end

    def self.check_token(payload)
      new_token = ''

      if valid_token?(payload["token"])
        key = {
          email: payload["email"],
          exp: payload["exp"]
        }
        new_token = self.generate_token_for(key)
      end

      new_token
    end

    private

    def self.generate_token_for(payload)
      JWT.encode payload, ENV['SECRET'], 'HS256'
    end

    def self.valid_token?(token)
      begin
        JWT.decode token, ENV['SECRET'], true, { algorithm: 'HS256' }
      rescue JWT::ExpiredSignature
        return false
      rescue JWT::DecodeError
        return false
      end

      true
    end

  end
end
