require_relative '../catalogs/service'

module Authorization
  class Collection
    class << self
      def exists?(email)
        catalog = ::Catalogs::Service.authorized_users
        catalog[:content].include?(email)
      end

    end
  end
end
