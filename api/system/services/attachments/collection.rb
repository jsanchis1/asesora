require_relative '../../infrastructure/clients'
require_relative '../../domain/attachment'

module Attachments
  class Collection
    class << self
      def create(attachment, file)
        serialized = attachment.serialize
        attachment_name = attachment.sftp_filename
        attachment_file = file['file']

        Sftp.upload(attachment_name, attachment_file)
        document = MongoClient.create(serialized)
        Domain::Attachment.from_document(document)
      end

      def retrieve(subject)
        document = MongoClient.retrieve(subject)
        Domain::List.from_document(document, Domain::Attachment)
      end

      def delete_for(subject)
        attachments = retrieve(subject)

        attachments.each do |attachment|
          delete(attachment)
        end
      end

      def download(id)
        attachment = find_by_id(id)

        Sftp.download(attachment.path ,attachment.sftp_filename)

        attachment
      end

      def delete(attachment)
        file = attachment.sftp_filename
        Sftp.delete(file)

        MongoClient.delete(attachment.identify)
      end

      def find_by_id(id)
        document = MongoClient.find_by_id(id)
        attachment = Domain::Attachment.from_document(document)

        attachment
      end

      private

      class MongoClient
        class << self
          def create(descriptor)
            client[:attachments].insert_one(descriptor)
            descriptor
          end

          def retrieve(id)
            document = client[:attachments].find({"subject_id": id})
            document
          end

          def find_by_id(id)
            document = client[:attachments].find({"id": id})

            document.first
          end

          def delete(id)
            client[:attachments].delete_one({"id": id})
          end

          private

          def client
            @client ||= Infrastructure::Clients.mongo
          end
        end
      end

      class Sftp
        class << self
          def upload(name, file)
            client.upload(name, file)
          end

          def delete(file)
            client.delete(file)
          end

          def download(path, filename)
            client.download(path, filename)
          end

          private

          def client
            @client ||= Infrastructure::Clients.sftp
          end
        end
      end
    end
  end
end
