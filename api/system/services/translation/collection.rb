module Translation
  class Collection
    DEFAULT_LOCALE = 'es'

    class << self
      def retrieve(locale)
        locale = DEFAULT_LOCALE unless Dictionary.has?(locale)

        Dictionary.retrieve(locale)
      end
    end

    class Dictionary
      class << self
        def has?(locale)
          dictionary.has_key?(locale)
        end

        def retrieve(locale)
          dictionary[locale]
        end

        private

        def dictionary
          {
            "es" => {
              "navbar": {
                "name": "Asesora",
                "solicitudesList": "Solicitudes",
                "createSolicitude": "Nueva solicitud",
                "subjectsList": "Casos",
                "usersManager": "Gestión acceso"
              },

              "linkbar": {
                "feedbackForm": "Enviar feedback"
              },

              "catalogs-manager": {
                "notValidID":"Este ID ya existe",
                "id": "ID",
                "title": "Catalogos",
                "source": "Medio de entrada",
                "ccaa": "Comunidad Autónoma",
                "organizations": "Organización vinculada",
                "topics": "Temas",
                "proposals": "Propuestas",
                "reasons": "Motivo de cierre",
                "projects": "Proyecto",
                "cnae": "CNAE",
                "addItem": "Añadir item",
                "save": "Guardar",
                "text": "Texto",
                "itemAdded": "Item añadido"
              },

              "solicitude": {
                "requiredText": "Debes proporcionar al menos un tema o el análisis para guardar el caso",
                "max200Words": "Máximo 200 palabras",
                "confirmRemoveSolicitudeWithoutSubject": "¿Estás seguro/a de que quieres eliminar esta solicitud?",
                "confirmRemoveSolicitudeWithSubject": "Esta solicitud tiene asociados %s casos que se eliminarán junto a ella ¿Estás seguro/a de que quieres eliminar esta solicitud? los datos no se podrán recuperar",
                "deleteSubjectMessage": "¿Estas seguro/a de que quieres eliminar el caso asociado? Ten en cuenta que esta operación no se puede deshacer",
                "applicant": "Solicitante",
                "date": "Fecha",
                "time": "Hora",
                "editingSolicitude": "Editando la solicitud nº",
                "editingSubject": "Editando el caso nº",
                "applicantName": "Nombre",
                "applicantSurname": "Apellidos",
                "applicantEmail": "Correo Electrónico",
                "applicantPhonenumber": "Teléfono",
                "noContact": "Debes proporcionar al menos email o teléfono para guardar la solicitud",
                "text": "Texto",
                "noDate": "Si no indicas fecha de la solicitud, ésta se registrará con la fecha de hoy",
                "incompleteCompanyIdentity": "Debes proporcionar un CIF válido, ejemplo: '12345678Z'",
                "maxEmployees": "Aviso, has excedido el número máximo de trabajadores (32.767) que permite la FEPRL, por lo que este caso no será justificable.",
                "noCompanyName": "Si se pone CIF se debe poner nombre de empresa para que la solicitud se pueda guardar",
                "company": "Empresa",
                "companyName": "Nombre empresa",
                "companyCif": "CIF",
                "companyEmployees": "Número trabajadoras y trabajadores",
                "companyCnae": "CNAE",
                "companyCp": "Código Postal del centro de trabajo",
                "addSubject": "Añadir Caso",
                "submit": "Guardar",
                "subjectsList": "Casos asociados",
                "subjectsSolicitude": "Listado de Casos",
                "submitting": "Solicitando",
                "editionsubmit": "Guardar",
                "editionsubmitting": "Guardando",
                "editiondiscard": "Descartar",
                "edited": "Todo Ok! Guardado!",
                "alertBackgroundDanger": "Lo sentimos, ha habido un error",
                "suggestions": "Sugerencias",
                "deleteSolicitude": "Eliminar solicitud",
                "errorPhone": "El teléfono debe tener nueve números para ser válido",
                "errorEmail": "El email no tiene un formato correcto, ejemplo: 'nombre@gmail.es'",
                "sent": "Todo Ok! Enviado!",
                "addedEmployeesValueMessage": "Se ha agregado información actualizada de número de empleados a la empresa",
                "addedNameValueMessage": "Se ha agregado información actualizada del nombre de la empresa",
                "proposals": "Propuestas de actuación",
                "analysis": "Análisis de la solicitud",
                "topics": "Temas",
                "notApply": "N/A",
                "created": "Fecha de apertura",
                "createdTime": "Hora",
                "subject": "Caso",
                "modifySubject": "Guardar cambios",
                "subjectModified": "Se han guardado los cambios del asesoramiento",
                "modify": "Modificar",
                "ccaa": "Comunidad autónoma",
                "personalData": "Datos personales",
                "contactData": "Datos de contacto",
                "solicitudeData": "Solicitud",
                "solicitude": "Solicitud nº: ",
                "identification": "Identificación",
                "information": "Información",
                "description": "Descripción de las propuestas",
                "project": "Proyecto",
                "comments": "Comentarios sobre el asesoramiento",
                "closeCounseling": "Cerrar caso",
                "closedSubject": "Cierre",
                "reason": "Motivo de cierre",
                "source": "Medio de entrada",
                "discard": "Descartar",
                "deleteSubject": "Eliminar",
                "required": "Necesario para justificar",
                "notJustifiable": "Solicitud no justificable",
                "removeSubjectClosedStatus": "Eliminar cierre",
                "confirmRemoveSubjectClosedStatus": "¿Estás seguro/a de que quieres eliminar el cierre asociado a este caso? Ten en cuenta que esta operación no se puede deshacer",
                "placeholderdescription": "Escribe con tus palabras en que consisten las propuestas que has realizado en este asesoramiento. Recuerda que esta información será compartida con el resto de asesoras y asesores, por lo que debería respetar la LOPD.",
                "placeholderAnalysis": "Describe brevemente tu análisis técnico de la solicitud para este caso. Puedes utilizar este campo para extender la información de la temática si lo necesitas, o proporcionar información relevante que hayas extraído del contexto de la solicitud. Recuerda que esta información será compartida con el resto de asesoras y asesores, por lo que debería respetar la LOPD.",
                "analisisInstructions":"Recuerda que esta información será compartida con el resto de asesoras y asesores, por lo que debería respetar la LOPD.",
                "sharedData": "Datos Compartidos:",
                "sharedDataInfo": "Recuerda que los campos 'fecha' y 'texto' de esta sección son compartidos con el resto de asesores y asesoras.",
                "registrationInstructions": "Pon aquí el texto de la consulta que has recibido con la mayor fidelidad posible, pegando el texto de un correo si lo ves necesario. Recuerda que esta información será compartida con el resto de asesoras y asesores, por lo que debería respetar la LOPD.",
                "sharedCompanyData": "Recuerda que los campos 'número de trabajadores y cnae' de esta sección son compartidos con el resto de asesores y asesoras.",
                "suggestChanges": "Sugerir cambios",
                "incompleteRecord": "Revisa que los campos marcados con un asterisco cumplen los requisitos mínimos para poder guardar",
                "organization": "Organización vinculada",
                "caseNumber": "Caso",
                "createdAt": "Apertura",
                "register": "Registro",
                "isJustified": "FEPRL",
                "subjectWarning": "La solicitud no es justificable",
                "edit": "Editar",
                "registerInfoA": "A: Análisis",
                "registerInfoD": "D: Descripción",
                "registerInfoP": "P: Propuestas",
                "registerInfoC": "C: Cerrado",
                "subjectCheck": "El caso es justificable",
                "hasAttachments": "Adjuntos",
                "removeConfirmAttachment": "Has seleccionado eliminar un archivo asociado al caso, esta acción no se podrá deshacer. ¿Estás segura o seguro de que quieres continuar?",
                "maxSized": "El archivo %file que intentas subir es mayor que el tamaño permitido por la aplicación(50Mb), o no tiene contenido. Prueba a reducir su tamaño y ha reintentar la operación. Si es imprescindible para tu trabajo manejar archivos de mayor tamaño que este límite, ponte en contacto con el soporte de Aserora y buscaremos una solución.",
                "cancel": "Cancelar",
                "createSubject": "Guardar caso",
                "newSubject": "Nuevo caso",
                "ownerDomain": "istas.ccoo.es",
                "requiredToShare": "Email no válido o texto vacio. El email debe ser de Istas autorizado.",
                "infoToShare": "Para completar esta acción tienes que introducir un mail con dominio '...@istas.ccoo.es' que esté dado de alta en esta aplicación",
                "involve": "Implicar a Istas",
                "send": "Enviar",
                "subjectSharedMessage": "Su caso ha sido compartido",
                "shared": "Compartido",
                "solicitudeInvolved": "Esta solicitud tiene como origen el caso: ",
                "alreadyInvolved": "Ya has implicado a Istas en este caso. La referencia es: ",
                "textToShare": "El usuario %origin_email ha compartido este caso: %text",
                "istasEmail": "Correo electrónico de ISTAS"
              },

              "solicitudes-list": {
                "listTitle": "Mis solicitudes",
                "mySolicitudes": "Mis solicitudes",
                "onlyDomain": " Sólo ",
                "allSolicitudes": "Mías + ",
                "code": "Número de solicitud",
                "date": "Fecha de solicitud",
                "applicant": "Solicitante",
                "company": "Empresa",
                "topics": "Temas del caso",
                "notApply": "n/a",
                "edit": "Editar",
                "show": "Mostrar",
                "description": "Descripción de las propuestas",
                "placeholderdescription": "Escribe con tus palabras en que consisten las propuestas que has realizado en este asesoramiento",
                "comments": "Comentarios sobre el asesoramiento",
                "subjects": "Casos",
                "feprl": "FEPRL",
                "zeroSubjects": "0/0",
                "titleWithoutSubject": "No contiene ningún caso",
                "of": " de ",
                "justifiedSubjects": " casos justificables",
                "shared": "Compartido",
                "owner": "Propietario"
              },

              "show-solicitude": {
                "summary": "Datos de la solicitud nº: ",
                "edit": "Editar",
                "addSubject": "Añadir Caso",
                "date": "Fecha",
                "time": "Hora",
                "text": "Texto",
                "applicant": "Solicitante",
                "applicantName": "Nombre",
                "applicantSurname": "Apellidos",
                "applicantEmail": "Correo Electrónico",
                "applicantPhonenumber": "Teléfono",
                "company": "Empresa",
                "companyName": "Nombre empresa",
                "companyCif": "CIF",
                "companyEmployees": "Número trabajadoras y trabajadores",
                "companyCnae": "CNAE",
                "companyCp": "Código Postal del centro de trabajo",
                "proposals": "Propuestas de actuación",
                "analysis": "Análisis de la solicitud",
                "topics": "Temas del caso",
                "subjectsList": "Listado de casos",
                "notApply": "N/A",
                "subject": "Caso",
                "applicantCcaa": "Comunidad autónoma",
                "description": "Descripción de las propuestas",
                "project": "Proyecto",
                "placeholderdescription": "Escribe con tus palabras en que consisten las propuestas que has realizado en este asesoramiento",
                "comments": "Comentarios sobre el asesoramiento",
                "created": "Fecha de apertura",
                "createdTime": "Hora",
                "reason": "Motivo de cierre",
                "source": "Medio de entrada",
                "deleteSubject": "Eliminar",
                "required": "Necesario para justificar",
                "notJustifiable": "Solicitud no justificable",
                "confirmDeleteSubject": "¿Estas seguro/a de que quieres eliminar el caso asociado? Ten en cuenta que esta operación no se puede deshacer",
                "files": "Archivos Adjuntos",
                "incompleteRecord": "Revisa que los campos marcados con un asterisco cumplen los requisitos mínimos para poder guardar",
                "organization": "Organización vinculada",
                "closedSubject": "Cierre",
                "solicitudeInvolved": "Esta solicitud tiene como origen el caso: ",
                "subjectInvolved": "Ya has implicado a Istas en este caso. La referencia es: "
              },

              "subjects": {
                "requiredText": "Debes proporcionar al menos un tema o el análisis para guardar el caso",
                "proposals": "Propuestas de actuación",
                "analysis": "Análisis de la solicitud",
                "subjectsSolicitude": "Datos de la solicitud",
                "applicant": "Solicitante",
                "company": "Empresa",
                "edit": "Editar",
                "addSubject": "Añadir caso",
                "createSubject": "Guardar caso",
                "topics": "Temas del caso",
                "created": "Fecha de apertura",
                "createdTime": "Hora",
                "placeholderAnalysis": "Describe brevemente tu análisis técnico de la solicitud para este caso. Puedes utilizar este campo para extender la información de la temática si lo necesitas, o proporcionar información relevante que hayas extraído del contexto de la solicitud. Recuerda que esta información será compartida con el resto de asesoras y asesores, por lo que debería respetar la LOPD.",
                "max200Words": "Máximo 200 palabras",
                "discardButtonSubject": "Descartar",
                "date": "Fecha",
                "time": "Hora",
                "text": "Texto",
                "applicantName": "Nombre",
                "applicantSurname": "Apellidos",
                "applicantEmail": "Correo Electrónico",
                "applicantPhonenumber": "Teléfono",
                "companyName": "Nombre empresa",
                "companyCif": "CIF",
                "companyEmployees": "Número trabajadoras y trabajadores",
                "companyCnae": "CNAE",
                "applicantCcaa": "Comunidad autónoma",
                "subjectsList": "Listado de casos",
                "notApply": "N/A",
                "subject": "Caso",
                "description": "Descripción de las propuestas",
                "project": "Proyecto",
                "placeholderdescription": "Escribe con tus palabras en que consisten las propuestas que has realizado en este asesoramiento. Recuerda que esta información será compartida con el resto de asesoras y asesores, por lo que debería respetar la LOPD.",
                "comments": "Comentarios sobre el asesoramiento",
                "closeCounseling": "Guardar y cerrar caso",
                "reason": "Motivo de cierre",
                "source": "Medio de entrada",
                "discard": "Descartar",
                "deleteSubject": "Eliminar",
                "required": "Necesario para justificar",
                "notJustifiable": "Solicitud no justificable",
                "newSubject": "Nuevo caso",
                "sharedData": "Datos Compartidos:",
                "requiredForSubject": "Debes proporcionar al menos un tema o un texto de análisis para poder guardar el caso",
                "sharedDataInfo": "Recuerda que todos los campos de esta sección son compartidos con el resto de asesores y asesoras.",
                "analisisInstructions":"Recuerda que esta información será compartida con el resto de asesoras y asesores, por lo que debería respetar la LOPD.",
                "confirmDeleteSubject": "¿Estas seguro/a de que quieres eliminar el caso asociado? Ten en cuenta que esta operación no se puede deshacer",
                "files": "Archivos Adjuntos",
                "suggestChanges": "Sugerir cambios",
                "incompleteRecord": "Revisa que los campos marcados con un asterisco cumplen los requisitos mínimos para poder guardar",
                "organization": "Organización vinculada",
                "maxSized": "El archivo %file que intentas subir es mayor que el tamaño permitido por la aplicación(50Mb), o no tiene contenido. Prueba a reducir su tamaño y ha reintentar la operación. Si es imprescindible para tu trabajo manejar archivos de mayor tamaño que este límite, ponte en contacto con el soporte de Aserora y buscaremos una solución."
              },

              "subjects-list": {
                "title": "Mis casos",
                "mySubjects": "Mis casos",
                "myDomainSubjects": "Sólo ",
                "allSubjects": "Míos + ",
                "allDomains": "De otras organizaciones",
                "caseNumber": "Número de caso",
                "solicitudeId": "Número de solicitud",
                "createdAt": "Apertura",
                "companyName": "Nombre de empresa",
                "topics": "Temas del caso",
                "register": "Registro",
                "isJustified": "FEPRL",
                "show": "Mostrar",
                "edit": "Editar",
                "notApply": "N/A",
                "registerInfoA": "A: Análisis",
                "registerInfoD": "D: Descripción",
                "registerInfoP": "P: Propuestas",
                "registerInfoC": "C: Cerrado",
                "subjectWarning": "La solicitud no es justificable",
                "subjectCheck": "El caso es justificable",
                "download": "Descargar en excel",
                "project": "Proyecto",
                "hasAttachments": "Contiene adjuntos",
                "all": "Todos",
                "downloadForJustification": "Descargar para justificación",
                "shared": "Compartido",
                "owner": "Propietario"
              },

              "login": {
                "title": "Acceso a Asesora",
                "instructions": "Pulsa el botón para acceder con tu cuenta de correo de CCOO",
                "unauthorizedEmail": "Tu cuenta no está en la lista de usuarias y usuarios de Asesora. Si necesitas acceder, por favor, ponte en contacto con Istas. Gracias."
              },

              "users-manager": {
                "title": "Gestionar acceso de usuarias y usuarios",
                "email": "Correo Electrónico",
                "errorEmail": "El email no tiene un formato correcto, ejemplo: 'nombre@gmail.es'",
                "emailAdd": "Añadir",
                "authorizedEmails": "Autorizados",
                "remove": "Eliminar",
                "removeConfirmMessage": "¿Estás Seguro/a que quieres eliminar el acceso este usuario/a de Asesora?\n Esta acción no eliminará sus casos, sino que permanecerán guardados"
              },

              "subjects-lot": {
                "title": "Mis lotes de casos",
                "project": "Proyecto",
                "subjectNumber": "Numero caso",
                "subjectDate": "Fecha caso",
                "ownerName": "Asesor/a",
                "companyName": "Nombre empresa",
                "topics": "Temas",
                "register": "Registro",
                "isJustified": "FEPRL",
                "show": "Mostrar",
                "registerInfoA": "A: Análisis",
                "registerInfoD": "D: Descripción",
                "registerInfoP": "P: Propuestas",
                "registerInfoC": "C: Cerrado",
                "subjectInfoWarning": "La solicitud no es justificable",
                "subjectInfoCheck": "El caso es justificable",
                "notApply": "-"
              }
            },

            "en" => {
              "navbar": {
                "name": "Asesora",
                "solicitudesList": "List of solicitudes",
                "createSolicitude": "New solicitude"
              }
            }
          }
        end
      end
    end
  end
end
