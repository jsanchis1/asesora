require_relative './solicitude'
require_relative './company'

  module Domain
    class List
      def initialize(collection, type)
        @list = []
        collection.each do |element|
          @list << type.from_document(element)
        end
      end

      def self.from_document(collection, type)
        List.new(collection, type)
      end

      def select(&block)
        result = []
        @list.each do |item|
          result.push(item) if block.call(item)
        end
        result
      end

      def serialize
        @list.map{|item| item.serialize()}
      end

      def each(&block)
        @list.each do |item|
          block.call(item)
        end
      end
    end
  end
