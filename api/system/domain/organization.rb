require 'securerandom'

module Domain
  class NullOrganization
    def serialize
      {}
    end

    def next_subject_numeration
      ''
    end

    def next_solicitude_numeration
      ''
    end

    def nil?
      true
    end
  end

  class Organization
    DEFAULT_NUMBER_OF_SOLICITUDES = 0
    DEFAULT_NUMBER_OF_SUBJECTS = 0

    def self.from_document(document)
      organization = new(document['domain'])
      organization.id = document['id']
      organization.created_solicitudes = document['created_solicitudes']
      organization.created_subjects = document['created_subjects']

      organization
    end

    def self.with(domain)
      organization = new(domain)
    end

    def self.as_null
      NullOrganization.new
    end

    def initialize(domain)
      @id = generate_id
      @domain = domain
      @created_subjects = DEFAULT_NUMBER_OF_SUBJECTS
      @created_solicitudes = DEFAULT_NUMBER_OF_SOLICITUDES
    end

    private_class_method :new

    def serialize
      {
        'id' => @id,
        'domain' => @domain,
        'created_subjects' => @created_subjects,
        'created_solicitudes' => @created_solicitudes
      }
    end

    def next_subject_numeration
      @created_subjects += 1

      numeration = build_subject_numeration

      numeration
    end

    def next_solicitude_numeration
      @created_solicitudes += 1

      numeration = build_solicitude_numeration

      numeration
    end

    def id=(value)
      @id = value
    end

    def created_solicitudes=(value)
      @created_solicitudes = value
    end

    def created_subjects=(value)
      @created_subjects = value
    end

    private

    def build_solicitude_numeration
      @domain + '-' + @created_solicitudes.to_s
    end

    def build_subject_numeration
      @domain + '-' + @created_subjects.to_s
    end

    def generate_id
      SecureRandom.uuid
    end
  end
end
