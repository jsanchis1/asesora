require_relative 'user'
require 'date'

module Domain
  class NullSolicitude
    def serialize
      {
        "date" => nil,
        "time" => nil,
        "text" => nil,
        "source" => nil,
        "applicant" => nil,
        "creation_moment" => nil,
        "edition_moment" => nil,
        "company" => nil,
        "user" => { "user_id" => nil, "domain" => nil },
        "numeration" => nil,
        "organization" => nil,
        "shared_origin" => nil
      }
    end

    def nil?
      true
    end
  end

  class Solicitude
    def self.as_null
      NullSolicitude.new
    end

    def self.from_document(document)
      solicitude = new(
        document['applicant'],
        document['company'],
        document['creation_moment']
      )
      solicitude.edition_moment = document['edition_moment']
      solicitude.solicited_at(document['date'])
      solicitude.solicited_at_time(document['time'])
      solicitude.text = document['text']
      solicitude.source = document['source']
      solicitude.user = Domain::User.with(document['user']['user_id'], document['user']['domain']) unless document['user'].nil?
      solicitude.numeration = document['numeration']
      solicitude.organization = document['organization']
      solicitude.shared_origin = document['shared_origin']

      solicitude
    end

    def self.with(date, time, text, source, applicant, company, user, numeration, organization, shared_origin = nil, creation_moment = nil, edition_moment = nil )
      solicitude = new(
        applicant,
        company,
        creation_moment
      )
      solicitude.edition_moment = Time.now.to_i
      solicitude.solicited_at(date)
      solicitude.solicited_at_time(time)
      solicitude.text = text
      solicitude.source = source
      solicitude.user = user
      solicitude.numeration = numeration
      solicitude.organization = organization
      solicitude.shared_origin = shared_origin

      solicitude
    end

    attr_writer :text, :date, :time, :source, :user, :numeration, :organization, :shared_origin

    def initialize(applicant, company, creation_moment = nil)
      @applicant = applicant
      @company=company
      @creation_moment = creation_moment || DateTime.now.strftime("%Q")
    end
    private_class_method :new

    def solicited_at(date)
      @date = parse(date)
    end
    def solicited_at_time(time)
      @time = time
    end

    def edition_moment=(value)
      @edition_moment = value
    end

    def serialize
      {
        "date" => @date.to_s,
        "time" => @time.to_s,
        "text" => @text,
        "source" => @source,
        "applicant" => @applicant,
        "creation_moment" => @creation_moment,
        "edition_moment" => @edition_moment,
        "company" => @company,
        "user" => @user.nil? ? {} : @user.serialize,
        "numeration" => @numeration,
        "organization" => @organization,
        "shared_origin" => @shared_origin
      }
    end

    def belongs?(user)
      return false if @user.nil?

      @user.is?(user)
    end

    def belongs_organization?(domain)
      return false if @user.nil?

      @user.belongs?(domain)
    end


    private

    def parse(date)
      return default_date if date.empty?

      return Date.parse(date)
    end

    def default_date
      return Date.today
    end
  end
end
