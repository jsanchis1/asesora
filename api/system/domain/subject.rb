module Domain
    class Subject
      def self.from_document(document)
        subject = new(
          document['solicitude_id'],
          document['id'],
          document['created'])

        subject.createdTime = document['createdTime']
        subject.proposal = document['proposal']
        subject.project = document['project']
        subject.description = document['description']
        subject.analysis = document['analysis']
        subject.topics = document['topics']
        subject.reason = document['reason']
        subject.comments = document['comments']
        subject.closed = document['closed']
        subject.numeration = document['numeration']
        subject.shared_destination = document['shared_destination']

        subject
      end

      def self.with(solicitude_id, proposal, description, analysis, topics, id=nil, created, createdTime, project, numeration)
        subject = new(solicitude_id, id, created)
        subject.createdTime = createdTime
        subject.proposal = proposal
        subject.project = project
        subject.description = description
        subject.analysis = analysis
        subject.topics = topics
        subject.numeration = numeration

        subject
      end

      attr_writer :proposal, :description, :analysis, :topics, :reason, :comments, :closed, :created, :createdTime, :project, :numeration, :shared_destination

      def initialize(solicitude_id, id = nil, created = nil)
        @solicitude_id = solicitude_id
        @id = id || DateTime.now.strftime("%Q")

        @created = created || parse_to_date(@id)
      end

      private_class_method :new


      def serialize
        {
          "proposal" => @proposal,
          "project" => @project,
          "description" => @description,
          "analysis" => @analysis,
          "topics" => @topics,
          "solicitude_id" => @solicitude_id,
          "reason" => @reason,
          "comments" => @comments,
          "closed" => @closed,
          "id" => @id,
          "created" => @created,
          "createdTime" => @createdTime,
          "numeration" => @numeration,
          "shared_destination" => @shared_destination
        }
      end

      def parse_to_date(id)
        date_in_seconds = (id.to_f / 1000).to_s
        parsed_date = Date.strptime(date_in_seconds, '%s')

        parsed_date.strftime('%F')
      end
    end
  end
