module Domain
    class User
      def self.from_document(document)
        user = new(
          document['user_id'],
          document['domain'])

        user
      end

      def self.with(user_id, domain)
        user = new(user_id, domain)

        user
      end

      def initialize(user_id, domain)
        @user_id = user_id
        @domain = domain
      end


      private_class_method :new

      def belongs?(domain)
        @user_id.include?(domain)
      end

      def is?(id)
        @user_id == id
      end

      def serialize
        {
          "user_id" => @user_id,
          "domain" => @domain
        }
      end
    end
  end
