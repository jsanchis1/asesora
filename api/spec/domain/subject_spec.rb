require_relative '../../system/domain/subject'

describe 'Subject' do
  it 'has a defined fields' do
    subject_keys = ['solicitude_id', 'id', 'created', 'createdTime', 'proposal', 'project', 'description', 'analysis', 'topics', 'reason', 'comments', 'closed', 'numeration']
    document = {
      'solicitude_id' => '',
      'id' => '',
      'created' => '',
      'createdTime' => '',
      'proposal' => '',
      'project' => '',
      'description' => '',
      'analysis' => '',
      'topics' => '',
      'reason' => '',
      'comments' => '',
      'closed' => '',
      'numeration' => ''
    }

    subject = Domain::Subject.from_document(document)

    expect_include_keys_in(subject.serialize, subject_keys)
  end

  def expect_include_keys_in(dictionary, keys)
    keys.each do |key|
      expect(dictionary).to have_key(key)
    end
  end
end
