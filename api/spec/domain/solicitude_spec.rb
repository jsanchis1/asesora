require_relative '../../system/domain/solicitude'

describe 'Solicitude' do
  it 'has a defined fields' do
    solicitude_keys = ['date', 'text', 'source', 'applicant', 'creation_moment', 'edition_moment', 'company', 'user', 'numeration']
    document = {
      'date' => '',
      'text' => '',
      'source' => '',
      'applicant' => '',
      'creation_moment' => '',
      'edition_moment' => '',
      'company' => '',
      'user' => '',
      'numeration' => ''
    }

    solicitude = Domain::Solicitude.from_document(document)

    expect_include_keys_in(solicitude.serialize, solicitude_keys)
  end

  def expect_include_keys_in(dictionary, keys)
    keys.each do |key|
      expect(dictionary).to have_key(key)
    end
  end
end
