require_relative '../../system/domain/organization'
require 'securerandom'

describe 'Organization' do
  it 'has defined attributes' do
    keys = ['id', 'domain', 'created_subjects', 'created_solicitudes']

    organization = Domain::Organization.with('domain')

    expect_has_keys_in(organization.serialize, keys)
  end

  it 'is created with a solicitude' do

    organization = Domain::Organization.with('domain')

    serialized = organization.serialize
    expect(serialized['created_solicitudes']).to be_zero
  end

  it 'is created without subjects' do

    organization = Domain::Organization.with('domain')

    serialized = organization.serialize
    expect(serialized['created_subjects']).to be_zero
  end

  it 'has an secure random as id' do
    secure_random = 'secure_random'
    allow(SecureRandom).to receive(:uuid).and_return(secure_random)

    organization = Domain::Organization.with('domain')

    serialized = organization.serialize
    expect(serialized['id']).to eq(secure_random)
  end

  def expect_has_keys_in(dictionary, keys)
    keys.each do |key|
      expect(dictionary).to have_key(key)
    end
  end
end
