require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'


describe 'Organizations Catalog' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  it 'retrieves complete catalog' do
    post '/api/organizations-catalog', {}
    catalog = JSON.parse(last_response.body)

    expect(catalog['data']['name']).to eq('organizations')
    expect(catalog['data']['content'].size).to eq(28)
  end
end
