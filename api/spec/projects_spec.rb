require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'


describe 'Projects' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  it 'retrieves complete catalog' do
    post '/api/projects', {}
    catalog = JSON.parse(last_response.body)

    expect(catalog['data']['name']).to eq('projects')
    expect(catalog['data']['content'].size).to eq(4)
  end
end
