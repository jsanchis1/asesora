require 'net/http'
require 'rspec'
require 'json'
require 'rack/test'
require 'date'

require_relative  '../system/services/solicitudes/service'
require_relative  '../system/services/attachments/service'
require_relative  '../system/services/subjects/service'
require_relative  '../system/services/companies/service'
require_relative  '../system/services/applicant/service'
require_relative './fixtures/asesora_with_fixtures'
require_relative './fixtures/fixtures'
require_relative './fixtures/solicitude_builder'

describe 'Solicitude Api' do
  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  after(:each) do
    post 'fixtures/clean'
  end

  after(:all) do
    Fixtures.new.drop_sftp
  end

  context 'delete solicitude by endpoint' do
    it 'returns error when solicitude not exists'  do
      id = {id: 1528913921360}.to_json

      post '/api/delete-solicitude', id
      response = last_response.status

      expect(response).to eq(500)
    end

    it 'deletes solicitude' do
      solicitude = create_solicitude_one()
      id = {id: solicitude['creation_moment']}.to_json

      post_delete_solicitude(id)

      expect_solicitude_deleted(id)
    end

    context 'when it leaves orphans' do
      it 'deletes the applicant' do
        solicitude = create_solicitude_one()
        id = {id: solicitude['creation_moment']}.to_json
        applicant_id = solicitude['applicant']

        post_delete_solicitude(id)
        applicant = Applicant::Service.retrieve(applicant_id)

        expect(applicant['id']).to be_nil
      end

      it 'deletes the company' do
        solicitude = create_solicitude_one()
        id = {id: solicitude['creation_moment']}.to_json
        company_id = solicitude['company']

        post_delete_solicitude(id)
        company = Companies::Service.retrieve(company_id)

        expect(company['cif']).to be_nil
      end

      it 'deletes its subjects' do
        subject = create_subject_with_solicitude
        id = {"id" => subject['solicitude_id']}

        post_delete_solicitude(id.to_json)

        subjects = Subjects::Service.retrieve(subject['id'])
        expect(subjects).to be_empty
      end

      it "deletes its subject's attachments" do
        Fixtures.new.drop_sftp
        subject = create_solicitude_with_attachments
        id = {"id" => subject['solicitude_id']}

        post_delete_solicitude(id.to_json)

        attachments = Attachments::Service.retrieve(subject['id'])
        expect(attachments).to be_empty
      end
    end

    context 'when not leaves orphans' do
      it 'not deletes the applicant' do
        solicitude = create_solicitude_one()
        id = {id: solicitude['creation_moment']}.to_json
        applicant_id = solicitude['applicant']
        solicitude_two = create_solicitude_two(applicant_id)

        post_delete_solicitude(id)
        applicant = Applicant::Service.retrieve(applicant_id)

        expect(applicant['id']).not_to be_nil
      end

      it 'not deletes the company' do
        solicitude = create_solicitude_one()
        id = {id: solicitude['creation_moment']}.to_json
        company_id = solicitude['company']
        applicant_id = solicitude['applicant']
        solicitude_two = create_solicitude_two(applicant_id)

        post_delete_solicitude(id)

        company = Companies::Service.retrieve(company_id)
        expect(company['id']).not_to be_nil
      end
    end


    def create_solicitude_two(applicant_id)
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'text': Fixtures::TEXT_2,
        'date': "",
        'applicantId': applicant_id,
        "companyName": Fixtures::COMPANY_NAME,
        "companyCif": Fixtures::COMPANY_CIF,
        "companyId": Fixtures::COMPANY_ID
      }.to_json

      post_create_solicitude(body)
      created_solicitude = JSON.parse(last_response.body)
      created_solicitude
    end

    def expect_solicitude_deleted(id)
      post '/api/retrieve-solicitude', id
      solicitude = JSON.parse(last_response.body)
      expect(solicitude['data']).to eq({})
    end
  end

  context 'create solicitude' do

    before(:each) do
      post 'fixtures/clean'
    end

    it 'returns a brand new solicitude' do
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'applicantCcaa': Fixtures::APPLICANT_CCAA,
        'text': Fixtures::TEXT,
        'date': Fixtures::DATE,
        'source': Fixtures::SOURCE,
        'applicantId': "",
        'companyCif': ""
      }.to_json

      post_create_solicitude(body)

      created_solicitude = JSON.parse(last_response.body)

      expect(created_solicitude['applicant']).not_to be_empty
      expect(created_solicitude['text']).to eq(Fixtures::TEXT)
      expect(created_solicitude['source']).to eq(Fixtures::SOURCE)
      expect(created_solicitude['date']).to eq(Fixtures::DATE)
      expect(created_solicitude['creation_moment']).not_to be_nil
    end

    it 'returns a new solicitude with existent applicant id' do
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'applicantCcaa': Fixtures::APPLICANT_CCAA,
        'text': Fixtures::TEXT,
        'date': Fixtures::DATE,
        'applicantId': "",
        'companyCif': ""
      }.to_json

      post_create_solicitude(body)
      created_solicitude = JSON.parse(last_response.body)
      applicant_id = created_solicitude['applicant']
      expect(applicant_id).not_to be_empty

      body_with_id = {
        'applicantName': Fixtures::APPLICANT_NAME_2,
        'applicantSurname': Fixtures::APPLICANT_SURNAME_2,
        'applicantEmail': Fixtures::APPLICANT_EMAIL_2,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER_2,
        'text': Fixtures::TEXT,
        'date': Fixtures::DATE,
        'applicantId': applicant_id,
        'companyCif': ""
      }.to_json

      post_create_solicitude(body_with_id)

      matches_body = {
        'applicantName': '',
        'applicantSurname': Fixtures::APPLICANT_SURNAME_2,
        'applicantEmail': '',
        'applicantPhonenumber': '',
        'applicantCcaa': ''
      }.to_json
      post '/api/applicant-matches', matches_body
      response = JSON.parse(last_response.body)
      expect(response['data'].length).to eq(0)
    end

    it 'generate creation moment in milliseconds' do
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'text': Fixtures::TEXT,
        'date': '',
        'applicantId': "",
        'companyCif': "",
        'numeration': Fixtures::NUMERATION
      }.to_json
      previous_moment = DateTime.now.strftime(in_microseconds)
      post_create_solicitude(body)
      later_moment = DateTime.now.strftime(in_microseconds)

      created_solicitude = JSON.parse(last_response.body)

      expect(created_solicitude['creation_moment']).to be_between(previous_moment, later_moment).inclusive

    end

    it 'new solicitude has today date' do
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'text': Fixtures::TEXT,
        'applicantId': "",
        'date': '',
        'companyCif': ""
      }.to_json
      post_create_solicitude(body)
      today = Date.today.strftime(in_english_format)

      created_solicitude = JSON.parse(last_response.body)

      expect(created_solicitude['applicant']).not_to be_empty
      expect(created_solicitude['text']).to eq(Fixtures::TEXT)
      expect(created_solicitude['date']).to eq(today)
    end

    it 'has incremental numeration' do
      solicitude = create_solicitude_one()
      another_solicitude = create_solicitude_one()

      expect(solicitude['numeration']).to eq('domain.com-1')
      expect(another_solicitude['numeration']).to eq('domain.com-2')
    end

    it 'also create subjects' do
      payload = solicitude_with_subjects_as_payload()

      id = create_solicitude(payload)

      solicitude = retrieve_solicitude(id)
      expect(solicitude['data']['subjects'].count).to eq(2)
    end

    context "memento" do
      it 'saves company in latest state' do
        first_solicitude = {
          'applicantName': Fixtures::APPLICANT_NAME,
          'applicantSurname': Fixtures::APPLICANT_SURNAME,
          'applicantEmail': Fixtures::APPLICANT_EMAIL,
          'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
          'text': Fixtures::TEXT,
          'date': Fixtures::DATE,
          'applicantId': "",
          'companyName': Fixtures::COMPANY_NAME,
          'companyCif': Fixtures::COMPANY_CIF
        }
        first_creation_moment = create_solicitude(first_solicitude)

        wait_for_new_edition_moment

        second_solicitude = {
          'applicantName': Fixtures::APPLICANT_NAME_2,
          'applicantSurname': Fixtures::APPLICANT_SURNAME,
          'applicantEmail': Fixtures::APPLICANT_EMAIL,
          'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
          'text': Fixtures::TEXT,
          'date': Fixtures::DATE,
          'applicantId': "",
          'companyName': Fixtures::COMPANY_NAME_2,
          'companyCif': Fixtures::COMPANY_CIF
        }
        second_creation_moment = create_solicitude(second_solicitude)

        first_solicitude = retrieve_solicitude(first_creation_moment)
        second_solicitude = retrieve_solicitude(second_creation_moment)

        expect(first_solicitude['data']['company_name']).to eq(Fixtures::COMPANY_NAME)
        expect(second_solicitude['data']['company_name']).to eq(Fixtures::COMPANY_NAME_2)
      end
    end
  end

  context 'retrieve solicitudes' do
    before(:each) do
      post 'fixtures/pristine'
    end

    it 'returns all solicitudes' do
      post '/fixtures/pristine'

      post '/api/retrieve-solicitudes'

      solicitudes = JSON.parse(last_response.body)
      solicitudes_count = solicitudes['data'].count

      expect(solicitudes_count).to eq(Fixtures::SOLICITUDES_COUNT)
    end

    it 'when date is the same sorts solicitudes in descendent creation moment' do
      post '/fixtures/clean'

      same_date = Fixtures::DATE

      first_solicitude = SolicitudeBuilder.default.with.text('Solicitude 1').date(same_date).build
      post_create_solicitude(first_solicitude.to_json)

      second_solicitude = SolicitudeBuilder.default.with.text('Solicitude 2').date(same_date).build
      post_create_solicitude(second_solicitude.to_json)

      post '/api/retrieve-solicitudes'
      response = JSON.parse(last_response.body)
      solicitudes = response['data']

      expect(solicitudes.count).to eq(2)
      expect(solicitudes[0]['text']).to eq('Solicitude 2')
      expect(solicitudes[1]['text']).to eq('Solicitude 1')
    end

    it 'knows how many times is one company in solicitudes' do
      post 'fixtures/clean'
      params = {"id": Fixtures::COMPANY_CIF}.to_json
      post '/api/count-company-in-solicitudes', params

      response = JSON.parse(last_response.body)
      expect(response['data']).to eq(0)

      solicitude = {
        "applicantPhonenumber": Fixtures::APPLICANT_PHONENUMBER,
        "text": Fixtures::TEXT,
        'date': Fixtures::DATE,
        'applicantId': "",
        "companyName": Fixtures::COMPANY_NAME,
  			"companyCif": Fixtures::COMPANY_CIF,
        "companyId": Fixtures::COMPANY_ID
      }.to_json

      post '/api/create-solicitude', solicitude

      params = {"id": Fixtures::COMPANY_CIF}.to_json
      post '/api/count-company-in-solicitudes', params

      response = JSON.parse(last_response.body)
      expect(response['data']).to eq(1)
    end

    context 'by user' do
      before(:each) do
        post 'fixtures/clean'
      end

      it "returns user's solicitudes" do
        solicitude_body = SolicitudeBuilder.default.build
        created_solicitude_id = create_solicitude(solicitude_body)

        user = { "user_id": Fixtures::USER_ID }.to_json
        post '/api/retrieve-solicitudes-by-user', user
        user_solicitudes = JSON.parse(last_response.body)["data"]

        expect(user_solicitudes[0]["creation_moment"]).to eq(created_solicitude_id)
      end

      it "don't returns not user's solicitudes" do
        owner_user_id = "owner@domain.com"
        solicitude_body = SolicitudeBuilder.default.with.user_id(owner_user_id).build
        create_solicitude(solicitude_body)

        not_owner_user_id = "not.owner@domain.com"
        user = { "user_id": not_owner_user_id }.to_json
        post '/api/retrieve-solicitudes-by-user', user
        user_solicitudes = JSON.parse(last_response.body)["data"]

        expect(user_solicitudes).to be_empty
      end
    end

  end

  context 'retrieve solicitude' do
    before(:each) do
      post 'fixtures/clean'
    end

    it 'returns one solicitude' do
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'text': Fixtures::TEXT,
        'applicantId': "",
        'date': Fixtures::DATE,
        'companyCif': ""
      }.to_json

      post_create_solicitude(body)

      created_solicitude = JSON.parse(last_response.body)

      id = {id: created_solicitude['creation_moment']}.to_json

      post '/api/retrieve-solicitude', id

      solicitude = JSON.parse(last_response.body)

      expect(solicitude['data']['text']).to eq(Fixtures::TEXT)
      expect(solicitude['data']['date']).to eq(Fixtures::DATE)
      expect(solicitude['data']['applicant_name']).to eq(Fixtures::APPLICANT_NAME)
      expect(solicitude['data']['applicant_surname']).to eq(Fixtures::APPLICANT_SURNAME)
      expect(solicitude['data']['applicant_email']).to eq(Fixtures::APPLICANT_EMAIL)
      expect(solicitude['data']['applicant_phonenumber']).to eq(Fixtures::APPLICANT_PHONENUMBER)
    end

    it "returns created solicitude that don't meets FEPRL", :meetsFeprl do
      solicitude_body = SolicitudeBuilder.default.with.companyEmployees(nil).build

      create_solicitude(solicitude_body)
      created_solicitude = JSON.parse(last_response.body)
      id = {id: created_solicitude['creation_moment']}.to_json
      post '/api/retrieve-solicitude', id
      solicitude = JSON.parse(last_response.body)

      expect(solicitude['data']['meetsFeprl']).to be false
    end

    it "returns created solicitude that meets FEPRL", :meetsFeprl do
      solicitude_body = SolicitudeBuilder.default.with.text('solicitud 1').build

      create_solicitude(solicitude_body)
      created_solicitude = JSON.parse(last_response.body)
      id = {id: created_solicitude['creation_moment']}.to_json
      post '/api/retrieve-solicitude', id
      solicitude = JSON.parse(last_response.body)

      expect(solicitude['data']['meetsFeprl']).to be true
    end

    it "by domain" do
      solicitude_one = SolicitudeBuilder.default.with.user_id('user@domain.com').build
      solicitude_two = SolicitudeBuilder.default.with.user_id('user@domain.com').build
      solicitude_three = SolicitudeBuilder.default.with.user_id('user@ccoo.com').build
      create_solicitude(solicitude_one)
      create_solicitude(solicitude_two)
      create_solicitude(solicitude_three)
      email = {user_id: "user@domain.com"}.to_json

      post '/api/retrieve-solicitudes-by-domain', email
      solicitudes = JSON.parse(last_response.body)

      expect(solicitudes['data'].length).to eq(2)
    end

    it "by domain less mine" do
      solicitude_one = SolicitudeBuilder.default.with.user_id('user@domain.com').build
      solicitude_two = SolicitudeBuilder.default.with.user_id('user@domain.com').build
      solicitude_three = SolicitudeBuilder.default.with.user_id('anotheruser@domain.com').build
      create_solicitude(solicitude_one)
      create_solicitude(solicitude_two)
      create_solicitude(solicitude_three)
      email = {user_id: "user@domain.com"}.to_json

      post '/api/retrieve-solicitudes-by-domain-less-mine', email
      solicitudes = JSON.parse(last_response.body)

      expect(solicitudes['data'].length).to eq(1)
    end
  end

  context 'update solicitude' do

    before(:each) do
      post 'fixtures/clean'
    end

    it 'endpoint update solicitude ' do
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'text': Fixtures::TEXT,
        'date': Fixtures::DATE,
        'applicantId': "",
        'companyName': Fixtures::COMPANY_NAME,
  			'companyCif': Fixtures::COMPANY_CIF
      }.to_json

      post_create_solicitude(body)
      created_solicitude = JSON.parse(last_response.body)

      creation_moment = created_solicitude['creation_moment']
      numeration = created_solicitude['numeration']


      update_solicitude = {
  			'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
  			'text': Fixtures::TEXT_2,
  			'date': Fixtures::DATE,
  			'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantId': "",
  			'companyCif': Fixtures::COMPANY_CIF,
        'creation_moment': creation_moment,
        'numeration': numeration
  		}.to_json

      post '/api/update-solicitude', update_solicitude
      response = JSON.parse(last_response.body)

      expect(response['text']).to eq(Fixtures::TEXT_2)
    end

    it 'only can updated by the owner' do
      original_text = 'original_text'
      body = {
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'text': original_text,
        'date': Fixtures::DATE,
        'applicantId': "123456789",
        'companyName': Fixtures::COMPANY_NAME,
        'companyCif': Fixtures::COMPANY_CIF,
        'user_id': 'user@email.com',
        'companyId': '123456789',
        'shared_origin': nil
      }.to_json
      post_create_solicitude(body)
      solicitude_with_changes = JSON.parse(last_response.body)
      solicitude_with_changes['text'] = 'edited_text'
      solicitude_with_changes['user_id'] = 'anotheruser@email.com'
      solicitude_with_changes['applicantId'] = '12345678'

      post '/api/update-solicitude', solicitude_with_changes.to_json

      response = JSON.parse(last_response.body)
      expect(response['text']).to eq(original_text)
    end

    context 'memento' do
      it 'retrieves company in latest state' do
        first_solicitude = SolicitudeBuilder.default.with.company_name(Fixtures::COMPANY_NAME).build
        first_creation_moment = create_solicitude(first_solicitude)
        wait_for_new_edition_moment
        second_solicitude = SolicitudeBuilder.default.with.company_name(Fixtures::COMPANY_NAME).build
        second_creation_moment = create_solicitude(second_solicitude)
        wait_for_new_edition_moment

        same_company_with_new_data = {
          'companyName': Fixtures::COMPANY_NAME_2,
          'companyId': Fixtures::COMPANY_ID
        }
        post '/api/update-company', same_company_with_new_data.to_json
        second_solicitude = SolicitudeBuilder.default.with.company_name(Fixtures::COMPANY_NAME).creation_moment(second_creation_moment).build
        update_solicitude(second_solicitude)

        first_solicitude = retrieve_solicitude(first_creation_moment)
        second_solicitude = retrieve_solicitude(second_creation_moment)
        expect(first_solicitude['data']['company_name']).to eq(Fixtures::COMPANY_NAME)
        expect(second_solicitude['data']['company_name']).to eq(Fixtures::COMPANY_NAME_2)
      end
    end
  end

  context 'Solicitude from subject'do

    it 'includes destination solicitude reference in origin subject', :wip do
      solicitude = create_solicitude_one()
      subject = Subjects::Service.create(solicitude['creation_moment'], nil, nil, nil, nil, nil, nil, 'numeration')
      subject_id = subject["id"]
      source_solicitude_user_id = solicitude['user']['user_id']
      source_solicitude_text = "Source solicitude text"
      istas_user_id = "user@istas.ccoo.es"
      istas_domain = "istas.ccoo.es"
      expected_source = 'Telemática'

      istas_solicitude_data = {
        "text" => source_solicitude_text,
        "origin_email" => source_solicitude_user_id,
        "destination_email"=> istas_user_id,
        "subject_id" => subject_id
      }

      subject = post_create_solicitude_from_subject(istas_solicitude_data)

      expect(subject['shared_destination']['id']).not_to be_empty
    end

    it 'includes origin subject reference in destination solicitude', :wip do
      solicitude = create_solicitude_one()
      subject = Subjects::Service.create(solicitude['creation_moment'], nil, nil, nil, nil, nil, nil, 'numeration')
      subject_id = subject["id"]
      source_solicitude_user_id = solicitude['user']['user_id']
      source_solicitude_text = "Source solicitude text"
      istas_user_id = "user@istas.ccoo.es"
      istas_domain = "istas.ccoo.es"
      expected_source = 'Telemática'

      istas_solicitude_data = {
        "text" => source_solicitude_text,
        "origin_email" => source_solicitude_user_id,
        "destination_email" => istas_user_id,
        "subject_id" => subject_id
      }

      subject = post_create_solicitude_from_subject(istas_solicitude_data)
      solicitude = retrieve_solicitude(subject['shared_destination']['id'])

      expect(solicitude['data']['shared_origin']['id']).to eq(subject['id'])
    end
  end

  private

  def create_solicitude(solicitude)
    post_create_solicitude(solicitude.to_json)
    response = JSON.parse(last_response.body)

    response['creation_moment']
  end


  def update_solicitude(solicitude)
    post '/api/update-solicitude', solicitude.to_json
  end

  def wait_for_new_edition_moment
    sleep 1
  end

  def retrieve_solicitude(creation_moment)
    post '/api/retrieve-solicitude', { 'id' => creation_moment }.to_json

    JSON.parse(last_response.body)
  end

  def in_microseconds
    return '%Q'
  end

  def in_english_format
    return '%Y-%m-%d'
  end

  def post_create_solicitude(body_created)
    post '/api/create-solicitude', body_created
  end

  def post_delete_solicitude(id)
    post '/api/delete-solicitude', id
  end

  def post_create_solicitude_from_subject(data)
    post '/api/create-solicitude-from-subject', data.to_json

    JSON.parse(last_response.body)
  end

  def retrieve_date(information)
    return information['date']
  end

  def retrieve_first_date(response)
    retrieve_date(response['data'][0])
  end

  def retrieve_second_date(response)
    retrieve_date(response['data'][1])
  end

  def retrieve_creation_moment(information)
    return information['creation_moment']
  end

  def retrieve_first_creation_moment(response)
    retrieve_creation_moment(response['data'][0])
  end

  def retrieve_second_creation_moment(response)
    retrieve_creation_moment(response['data'][1])
  end

  def create_subject_with_solicitude
    solicitude = create_solicitude_one()
    subject = Subjects::Service.create(solicitude['creation_moment'], nil, nil, nil, nil, nil, nil, 'numeration')

    subject
  end

  def create_solicitude_with_attachments
    solicitude = create_solicitude_one()
    subject = Subjects::Service.create(solicitude['creation_moment'], nil, nil, nil, nil, nil, nil, 'numeration')
    Attachments::Service.create({"name"=>"file_name.txt", "file"=>"base64"}, subject['id'])

    subject
  end

  def create_solicitude_one
    body = {
      'applicantName': Fixtures::APPLICANT_NAME,
      'applicantSurname': Fixtures::APPLICANT_SURNAME,
      'applicantEmail': Fixtures::APPLICANT_EMAIL,
      'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
      'text': Fixtures::TEXT,
      'date': Fixtures::DATE,
      'applicantId': "",
      "companyName": Fixtures::COMPANY_NAME,
      "companyCif": Fixtures::COMPANY_CIF,
      "companyId": Fixtures::COMPANY_ID,
      "user_id": Fixtures::ANOTHER_USER_ID
    }.to_json

    post_create_solicitude(body)
    created_solicitude = JSON.parse(last_response.body)
    created_solicitude
  end

  def solicitude_with_subjects_as_payload()
    payload = SolicitudeBuilder.default.build
    subject = {
      "solicitudeId": '',
      "topics": Fixtures::SUBJECT_TOPICS,
      "analysis": Fixtures::SUBJECT_PROPOSAL_ANALYSIS,
      "proposal": Fixtures::SUBJECT_PROPOSAL,
      "description": Fixtures::SUBJECT_PROPOSAL_DESCRIPTION,
      "reason": Fixtures::SUBJECT_CLOSE_REASON,
      "comments": Fixtures::SUBJECT_CLOSE_COMMENTS,
      "subjectId": '',
      "created": '',
      "project": {},
      "files": []
    }
    payload["subjects"] = [subject, subject]

    payload
  end
end
