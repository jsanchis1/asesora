require_relative './fixtures'

class SolicitudeBuilder
  def self.default
    new()
  end

  def initialize
    @solicitude = {
      applicantId: "",
      applicantName: Fixtures::APPLICANT_NAME,
      applicantSurname: Fixtures::APPLICANT_SURNAME,
      applicantEmail: Fixtures::APPLICANT_EMAIL,
      applicantPhonenumber: Fixtures::APPLICANT_PHONENUMBER,
      applicantCcaa: Fixtures::APPLICANT_CCAA,
      text: Fixtures::TEXT,
      date: Fixtures::DATE,
      companyName: Fixtures::COMPANY_NAME,
      companyCif: Fixtures::COMPANY_CIF,
      companyEmployees: Fixtures::COMPANY_EMPLOYEES,
      companyCnae: Fixtures::COMPANY_CNAE,
      user_id: Fixtures::USER_ID,
      companyId: Fixtures::COMPANY_ID
    }
  end

  def with
    self
  end

  def text(value)
    @solicitude[:text] = value
    self
  end

  def company_name(value)
    @solicitude[:companyName] = value
    self
  end

  def date(value)
    @solicitude[:date] = value
    self
  end

  def companyEmployees(value)
    @solicitude[:companyEmployees] = value
    self
  end

  def creation_moment(value)
    @solicitude[:creation_moment] = value
    self
  end

  def user_id(value)
    @solicitude[:user_id] = value
    self
  end

  def numeration(value)
    @solicitude[:numeration] = value
    self
  end

  def build
    @solicitude
  end
end
