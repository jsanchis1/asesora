# encoding: UTF-8
require_relative '../../system/services/companies/service'
require_relative '../../system/services/applicant/service'
require_relative '../../system/services/solicitudes/service'
require_relative '../../system/services/subjects/service'
require_relative '../../system/services/catalogs/service'
require_relative '../../system/actions/retrieve_solicitude'
require_relative '../../spec/infrastructure/sftp_client_test'

require 'mongo'

class Fixtures
  CREATION_MOMENT = '1234567890'
  SOLICITUDES_COUNT = 4
  SOLICITUDES_COUNT_FOR_DEFAULT_COMPANY = 3
  SUBJECTS_COUNT = 2
  DATE = '2018-04-25'
  CREATED = '2019-01-26'
  CREATED_TIME = '12:12'
  SOURCE = {'text' => 'Telefonico', 'value' => '01'}
  TEXT = 'La primera solicitud'
  DATE_1 = '2018-04-25'
  TEXT_1 = 'La primera solicitud'
  APPLICANT_NAME = 'Piedad'
  APPLICANT_SURNAME = 'Alarcón'
  APPLICANT_SURNAME_WITHOUT_ACCENT = 'Alarcon'
  APPLICANT_EMAIL = 'piedadalarcon@noname.es'
  APPLICANT_CCAA = { 'text' => 'Baleares', 'value' => '01' }
  APPLICANT_CCAA_2 = { 'text' => 'Cataluña', 'value' => '02' }
  APPLICANT_PHONENUMBER = '000111000'
  COMPANY_ID = 'secure_random_id'
  COMPANY_NAME = 'De Rape SL'
  COMPANY_CIF = 'A98005978'
  COMPANY_EMPLOYEES = '34'
  COMPANY_CP = '46028'
  COMPANY_CNAE = '931 - Actividades deportivas'
  TEXT_2 = 'La primera solicitud'
  APPLICANT_NAME_2 = 'Piedad'
  APPLICANT_SURNAME_2 = 'Romero'
  APPLICANT_EMAIL_2 = 'hidalgoromero@noname.es'
  APPLICANT_PHONENUMBER_2 = '000222000'
  COMPANY_NAME_2 = 'Campigue SAL'
  COMPANY_CIF_2 = 'H15103393'
  COMPANY_EMPLOYEES_2 = '34'
  COMPANY_CNAE_2 = '870 - Asistencia en establecimientos residenciales'
  COMPANY_CIF_3 = 'U7053991A'
  SUBJECT_PROPOSAL = 'Propuestas de actuación'
  SUBJECT_PROPOSAL_2 = 'Otras Propuestas de actuación'
  SUBJECT_PROPOSAL_DESCRIPTION = 'Descripción de la propuesta'
  SUBJECT_PROPOSAL_DESCRIPTION_2 = 'Descripción de la propuesta 2'
  SUBJECT_PROPOSAL_ANALYSIS = 'Análisis de la solicitud'
  SUBJECT_PROPOSAL_ANALYSIS_2 = 'Otros Análisis de la solicitud'
  SUBJECT_TOPICS = 'Temas del caso'
  SUBJECT_TOPICS_2 = 'Más temas del caso 2'
  SUBJECT_CLOSE_REASON = 'A reason'
  SUBJECT_CLOSE_COMMENTS = 'A comment'
  SUBJECT_NUMERATION = 'domain.com-1'
  PROJECT = { 'text' => 'Proyecto 1', 'value' => 'Proyecto 1' }
  USER_ID = ''
  ANOTHER_USER_ID = 'user@domain.com'
  DOMAIN = 'domain'
  AUTHORIZED_USERS = ["authorized_email@gmail.com","another_authorized_email@gmail.com","mgquiros@istas.ccoo.es","asesora.authorized@gmail.com", 'user@domain.com']
  NUMERATION = 'domain-1'
  FILE = {'name' => "attach", 'file' => "SG9sYSBsb2xv"}
  FILE_CONTENT = "Hola lolo"
  FILES = [{'name' => "attach", 'file' => "SG9sYSBsb2xv"}, {'name' => "attach_file", 'file' => "SG9sYSBsb2xv"}]
  ORGANIZATIONS = 'Confederación Sindical de Comisiones Obreras - ESTATAL'

  def pristine
    clean_collections
    insert_solicitudes
  end

  def clean_collections
    drop_solicitudes
    drop_companies
    drop_applicant
    drop_subjects
    drop_organizations
    drop_attachments
  end

  def drop_sftp
    STFPClientTest.new.drop
  end

  def reset_authorized_users_catalog
    Catalogs::Service.update_authorized_users(AUTHORIZED_USERS)
  end

  def drop_subjects
    client['subjects'].drop()
  end

  def drop_organizations
    client['organizations'].drop()
  end

  def drop_attachments
    client['attachments'].drop()
  end

  def drop_solicitudes
    client['solicitudes'].drop()
  end

  def drop_companies
    client['companies'].drop()
    client['company_memento'].drop()
  end

  def drop_applicant
    client['applicant'].drop()
  end

  private

  def insert_solicitudes()
    first_company = create_company(COMPANY_NAME, COMPANY_CIF, COMPANY_EMPLOYEES, COMPANY_CNAE, COMPANY_CP)
    first_applicant = create_applicant(APPLICANT_NAME, APPLICANT_SURNAME, APPLICANT_EMAIL, APPLICANT_PHONENUMBER, APPLICANT_CCAA)
    first_solicitude = create_solicitude(first_applicant, first_company, USER_ID, '', '-1', ORGANIZATIONS)
    create_justified_subject(first_solicitude, '-2')

    solicitude_without_domain = create_solicitude(first_applicant, first_company, 'user@', '', '-2', ORGANIZATIONS)
    create_closed_subject(solicitude_without_domain, '-1')

    second_company = create_company(COMPANY_NAME_2, COMPANY_CIF_2, COMPANY_EMPLOYEES_2, COMPANY_CNAE_2, COMPANY_CP)
    second_applicant = create_applicant(APPLICANT_NAME_2, APPLICANT_SURNAME_2, APPLICANT_EMAIL_2, APPLICANT_PHONENUMBER_2, APPLICANT_CCAA_2)
    second_solicitude = create_solicitude(second_applicant, second_company, USER_ID, '', '-3', ORGANIZATIONS)
    create_closed_subject(second_solicitude, '-3')

    third_solicitude = create_solicitude(second_applicant, first_company, ANOTHER_USER_ID, DOMAIN, NUMERATION, ORGANIZATIONS)

    [first_solicitude, second_solicitude, third_solicitude, solicitude_without_domain]
  end

  def create_company(name, cif, employees, cnae, cp)
    ::Companies::Service.create(name, cif, employees, cnae, cp)
  end

  def create_applicant(name, surname, email, phonenumber, ccaa)
    ::Applicant::Service.create(name, surname, email, phonenumber, ccaa)
  end

  def create_solicitude(applicant, company, user_id, domain, numeration, organizations)
    shared_origin = nil
    solicitude = ::Solicitudes::Service.create(DATE_1, TEXT_1, SOURCE, applicant["id"], company["id"], user_id, domain, numeration, organizations, shared_origin)
    full_solicitude = ::Actions::RetrieveSolicitude.do(id: solicitude["creation_moment"])

    full_solicitude
  end

  def create_justified_subject(solicitude, numeration)
    Subjects::Service.create(solicitude['creation_moment'], ["Adaptación puesto"], SUBJECT_PROPOSAL_DESCRIPTION, SUBJECT_PROPOSAL_ANALYSIS, [{"id"=>"01.01", "name"=>"Agentes químicos"}], CREATED, PROJECT, numeration)
  end

  def create_closed_subject(solicitude, numeration)
    subject = Subjects::Service.create(solicitude['creation_moment'], ["Adaptación puesto"], SUBJECT_PROPOSAL_DESCRIPTION, SUBJECT_PROPOSAL_ANALYSIS, [{"id"=>"01.01", "name"=>"Agentes químicos"}], CREATED, CREATED_TIME, PROJECT, numeration)

    Subjects::Service.close(subject, subject['id'], { 'text' => 'Consulta puntual', 'value' => '01' }, 'comments', '', CREATED, CREATED_TIME)
  end

  def client
    mongo_uri = ENV['MONGODB_URI']
    mongo_user = ENV['MONGODB_USER']
    mongo_pssw = ENV['MONGODB_PSSW']
    ddbb_name = ENV['DDBB_NAME']
    Mongo::Logger.logger.level = Logger::INFO

    @client ||= Mongo::Client.new(mongo_uri,
      {
        max_pool_size: 5,
        user: mongo_user,
        password: mongo_pssw,
        database: ddbb_name
      }
    )
  end
end
