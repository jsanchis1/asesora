require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'


describe 'CCAA' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  it 'retrieves complete catalog' do
    post '/api/ccaa', {}
    catalog = JSON.parse(last_response.body)

    expect(catalog['data']['name']).to eq('ccaa')
    expect(catalog['data']['content'].size).to eq(20)
  end
end
