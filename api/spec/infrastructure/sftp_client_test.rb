require_relative '../../system/infrastructure/clients'

class STFPClientTest < Infrastructure::Clients::SFTP
  EXCEPTIONS = ['Dockerfile']

  def list_files
    files = []

    client do |sftp|
      sftp.dir.foreach('/files') { |file| files.push(file.name) }
    end

    files
  end

  def drop
    client do |sftp|
      sftp.dir.foreach('/files') do |file|
        name = file.name

        sftp.remove("/files/#{name}") unless is_exception?(name)
      end
    end
  end

  private

  def is_exception?(file)
    EXCEPTIONS.include?(file)
  end
end
