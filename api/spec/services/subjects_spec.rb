require_relative  '../../system/services/subjects/service'
require_relative './../fixtures/fixtures'
require_relative '../../system/config/i18n'

describe 'Subject service' do
  before(:each) do
    Fixtures.new.drop_subjects
  end

  after do
    Fixtures.new.drop_subjects
  end

  it "retrieve subject" do
    solicitude_id = Fixtures::CREATION_MOMENT
    created = Fixtures::CREATED
    createdTime = Fixtures::CREATED_TIME

    subject = Subjects::Service.create(solicitude_id, 'proposal', 'description', 'analysis', 'topics', created, createdTime, 'project', 'numeration')

    result = Subjects::Service.retrieve(subject["id"])
    expect(result["solicitude_id"]).to eq(solicitude_id)
    expect(result["created"]).to eq(created)
  end

  it "retrieves old subjects with its creation moment as created" do
    subject = Subjects::Service.create('solicitude_id', 'proposal', 'description', 'analysis', 'topics', nil, nil, 'project', 'numeration')

    result = Subjects::Service.retrieve(subject["id"])

    subject_creation_moment = parse_to_date(result["id"])
    expect(result["created"]).to eq(subject_creation_moment)
  end

  it "retrieves subjects in ascendent order" do
    solicitude_id = Fixtures::CREATION_MOMENT
    proposal_two = Fixtures::SUBJECT_PROPOSAL_2

    Subjects::Service.create(solicitude_id, 'proposal', 'description', 'analysis', 'topics', nil, nil, 'project', 'domain-1')
    second_subject = Subjects::Service.create(solicitude_id, proposal_two, 'description', 'analysis', 'topics', nil, nil, 'project', 'domain-2')

    retrieved_subjects = Subjects::Service.all_by(solicitude_id)
    expect(retrieved_subjects[0]["proposal"]).to eq(proposal_two)
  end

  it "search matches from criterial" do
    criteria = 'Propó'
    first_subject = Subjects::Service.create('solicitude_id', 'proposal', 'description', 'analysis', 'topics', 'created', 'createdTime', 'project', 'numeration')
    second_subject = Subjects::Service.create('solicitude_id', 'proposal', 'description', 'analysis', 'topics', 'created', 'createdTime', 'project', 'numeration')
    Subjects::Service.create('solicitude_id', 'pro', 'description', 'analysis', 'topics', 'created', 'createdTime', 'project', 'numeration')

    result = Subjects::Service.filter_by(criteria)

    expect(result).to eq([first_subject, second_subject])
  end

  def parse_to_date(id)
    date_in_seconds = (id.to_f / 1000).to_s
    parsed_date = Date.strptime(date_in_seconds, '%s')

    parsed_date.strftime('%F')
  end
end
