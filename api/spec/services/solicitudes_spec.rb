require_relative  '../../system/services/solicitudes/service'
require_relative './../fixtures/fixtures'

describe 'Solicitude service' do
  before(:each) do
    Fixtures.new.drop_solicitudes
  end

  after do
    Fixtures.new.drop_solicitudes
  end

  it "knows when company hasn't solicitudes" do

    result = Solicitudes::Service.times_company("non_existing_cif")

    expect(result).to eq(0)
  end

  it 'knows how many solicitudes has one company' do
    company = 'company_id'
    Solicitudes::Service.create('01/01/01', 'text', 'source', 'applicant', company, 'user_id', 'domain', nil, nil, nil)
    Solicitudes::Service.create('01/01/01', 'text', 'source', 'applicant', company, 'user_id', 'domain', nil, nil, nil)

    times = Solicitudes::Service.times_company(company)

    expect(times).to eq(2)
  end

  it 'update edition moment' do
    solicitude = Solicitudes::Service.create('01/01/01', 'text', 'source', 'applicant', 'company', 'user_id', 'domain', nil, nil, nil)

    updated_solicitude = Solicitudes::Service.update('01/01/01', 'text', 'source', 'applicant', 'company', 'user_id', 'domain', 'numeration', 'organizations', 'shared_origin',  solicitude['creation_moment'])

    expect(updated_solicitude['edition_moment']).to be >= solicitude['edition_moment']
  end

  it 'return all solicitudes by user in ascendent order' do
    Solicitudes::Service.create('01/01/01', 'text', 'source', 'applicant_one', 'company', 'user_id', 'domain', 'domain-1', 'organizations', nil)
    Solicitudes::Service.create('01/01/02', 'text', 'source', 'applicant_two', 'company', 'user_id', 'domain', 'domain-2', 'organizations', nil)

    all_solicitudes_by_user = Solicitudes::Service.all_by_user('user_id')

    expect(all_solicitudes_by_user[0]['applicant']).to eq('applicant_two')
  end
end
