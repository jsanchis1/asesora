require_relative '../../system/services/organizations/service'
require_relative '../fixtures/fixtures'

describe 'Organizations' do
  before(:each) do
    Fixtures.new.drop_organizations
  end

  after do
    Fixtures.new.drop_organizations
  end

  it 'retrieves the next subject numeration' do
    email = 'user@domain.com'
    Organizations::Service.create(email)
    numeration = 'domain.com-1'

    next_numeration = Organizations::Service.next_subject_numeration(email)

    expect(next_numeration).to eq(numeration)
  end
end
