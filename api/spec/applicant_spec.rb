require 'net/http'
require 'rspec'
require 'json'
require 'rack/test'
require 'date'

require_relative '../system/services/applicant/service'
require_relative './fixtures/asesora_with_fixtures'
require_relative './fixtures/fixtures'

describe 'Solicitude Applicant' do
  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

context 'of the applicant' do
    before(:each) do
      post 'fixtures/clean'
    end

    it 'search matches by criterial including owner' do
      create_solicitude_for_applicant
      create_solicitude_for_another_applicant

      matches_body = {
        'applicantName': '',
        'applicantSurname': Fixtures::APPLICANT_SURNAME_WITHOUT_ACCENT,
        'applicantEmail': '',
        'applicantPhonenumber': '',
        'applicantCcaa': "",
        'user_id': "asesora@gmail.com"
      }.to_json
      post '/api/applicant-matches', matches_body
      response = JSON.parse(last_response.body)

      expect(response['data'][0]['phonenumber']).to eq(Fixtures::APPLICANT_PHONENUMBER)
      expect(response['data'].length).to eq(1)
    end

    it 'update applicant data' do
        first_body = {
          'date': Fixtures::DATE,
          'applicantName': Fixtures::APPLICANT_NAME,
          'applicantSurname': Fixtures::APPLICANT_SURNAME,
          'applicantEmail': Fixtures::APPLICANT_EMAIL,
          'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
          'applicantCcaa': "",
          'text': Fixtures::TEXT,
          'applicantId': "",
          'companyCif': ""
          }.to_json

        post_create_solicitude(first_body)
        response = JSON.parse(last_response.body)
        applicant_id = response['applicant']

        second_body = {
          'applicantName': Fixtures::APPLICANT_NAME,
          'applicantSurname': Fixtures::APPLICANT_SURNAME,
          'applicantEmail': Fixtures::APPLICANT_EMAIL,
          'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER_2,
          'applicantCcaa': "",
          'applicantId': applicant_id,
          }.to_json

        post '/api/update-applicant', second_body
        response = JSON.parse(last_response.body)
        applicant_phone = response['phonenumber']

        expect(applicant_phone).to eq(Fixtures::APPLICANT_PHONENUMBER_2)
    end

    it 'deletes applicant' do
      solicitude = {
        'date': Fixtures::DATE,
        'applicantName': Fixtures::APPLICANT_NAME,
        'applicantSurname': Fixtures::APPLICANT_SURNAME,
        'applicantEmail': Fixtures::APPLICANT_EMAIL,
        'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
        'applicantCcaa': "",
        'text': Fixtures::TEXT,
        'applicantId': "",
        'companyCif': ""
        }.to_json
      post_create_solicitude(solicitude)
      response = JSON.parse(last_response.body)
      applicant_id = response['applicant']

      before_delete = Applicant::Service.retrieve(applicant_id)
      delete_applicant(applicant_id)
      after_delete = Applicant::Service.retrieve(applicant_id)

      expect(before_delete).not_to eq(after_delete)
    end
  end

  private

  def create_solicitude_for_applicant
    body = {
      'applicantName': Fixtures::APPLICANT_NAME,
      'applicantSurname': Fixtures::APPLICANT_SURNAME,
      'applicantEmail': Fixtures::APPLICANT_EMAIL,
      'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
      'applicantCcaa': "",
      'text': Fixtures::TEXT,
      'applicantId': "",
      'date': Fixtures::DATE,
      'companyCif': "",
      'user_id': "asesora@gmail.com"
      }.to_json
    post_create_solicitude(body)
  end

  def create_solicitude_for_another_applicant
    body = {
      'applicantName': Fixtures::APPLICANT_NAME_2,
      'applicantSurname': Fixtures::APPLICANT_SURNAME,
      'applicantEmail': Fixtures::APPLICANT_EMAIL_2,
      'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER_2,
      'applicantCcaa': "",
      'text': Fixtures::TEXT,
      'applicantId': "",
      'date': Fixtures::DATE,
      'companyCif': "",
      'user_id': "asesora_other@gmail.com"
      }.to_json
    post_create_solicitude(body)
  end

  def post_create_solicitude(body_created)
    post '/api/create-solicitude', body_created
  end

  def delete_applicant(applicant_id)
      Applicant::Service.delete(applicant_id)
  end
end
