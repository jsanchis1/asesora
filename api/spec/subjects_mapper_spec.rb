require_relative '../system/actions/subjects_mapper'

describe 'Dictionary of filters' do
  it 'filtering by other organizations shows only an especific fields' do
    subject = all_data_subject

    response = SubjectsMapper.all_less_my_domain_less([subject])

    expect(response).not_to be_empty
  end
end

def all_data_subject
  subject = {
    "solicitude_date"=>"2018-04-25",
    "solicitude_text"=>"La primera solicitud",
    "solicitude_source"=>{"text"=>"Telefonico", "value"=>"01"},
    "solicitude_applicant"=>"a5ba2fb0-9fce-453c-98ff-d654e4badcea",
    "solicitude_creation_moment"=>"1562755699232",
    "solicitude_edition_moment"=>1562755699,
    "solicitude_company"=>"03163fdb-db27-4173-b5a7-fa6c15e16885",
    "solicitude_user"=>{"user_id"=>"", "domain"=>"domain"},
    "solicitude_numeration"=>"-2",
    "solicitude_organization"=>nil,
    "solicitude_shared_origin"=>nil,
    "applicant_name"=>"Piedad",
    "applicant_surname"=>"Alarcón",
    "applicant_email"=>"piedadalarcon@noname.es",
    "applicant_phonenumber"=>"000111000",
    "applicant_ccaa"=>{"text"=>"Baleares", "value"=>"01"},
    "applicant_id"=>"a5ba2fb0-9fce-453c-98ff-d654e4badcea",
    "company_name"=>"De Rape SL",
    "company_cif"=>"A98005978",
    "company_employees"=>"34",
    "company_cnae"=>"931 - Actividades deportivas",
    "company_id"=>"03163fdb-db27-4173-b5a7-fa6c15e16885",
    "subject_proposal"=>"Propuestas de actuación",
    "subject_project"=>{"text"=>"Proyecto 1", "value"=>"Proyecto 1"},
    "subject_description"=>"Descripción de la propuesta",
    "subject_analysis"=>"Análisis de la solicitud",
    "subject_topics"=>"Temas del caso",
    "subject_solicitude_id"=>"1562755699232",
    "subject_reason"=>nil,
    "subject_comments"=>nil,
    "subject_closed"=>nil,
    "subject_id"=>"1562755699236",
    "subject_created"=>"2019-07-10",
    "subject_createdTime"=>"12:12",
    "subject_numeration"=>"-2",
    "subject_shared_destination"=>nil
    }

    subject
end
