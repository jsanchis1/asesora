require_relative '../system/infrastructure/clients'

class FillOtherOrganization
  class << self
    def do
      p 'Filling solicitudes with empty organization...'
      solicitudes.each do |solicitude|
        solicitude['organization'] = ''

        client[:solicitudes].find_one_and_replace({ creation_moment: solicitude['creation_moment'] }, solicitude)
      end
      p 'Done!!!'
    end

    private

    def solicitudes
      p 'Getting all solicitudes...'
      client[:solicitudes].find()
    end

    def client
      @client ||= Infrastructure::Clients.mongo

      @client
    end
  end
end
