require_relative '../system/actions/retrieve-subjects-by-domain'
require_relative '../system/actions/retrieve_subjects_by_text'
require_relative '../system/actions/retrieve_all_counselings_by_user'

module Endpoints
  class Subjects
    def self.define_endpoints(api)
      api.post '/api/retrieve-subjects-by-domain' do
        params = JSON.parse(request.body.read)

        subjects = Actions::RetrieveSubjectsByDomain.do(params['user_id'])

        {data: subjects}.to_json
      end

      api.post '/api/retrieve-subjects-by-domain-less' do
        params = JSON.parse(request.body.read)

        subjects = Actions::RetrieveSubjectsByDomain.less_do(params['user_id'])

        {data: subjects}.to_json
      end

      api.post '/api/retrieve-subjects-less-my-domain' do
        params = JSON.parse(request.body.read)

        subjects = Actions::RetrieveSubjectsByDomain.less_mine_do(params['user_id'])

        {data: subjects}.to_json
      end

      api.post '/api/retrieve-all-subjects' do
        params = JSON.parse(request.body.read)

        subjects = Actions::RetrieveSubjectsByDomain.all_do(params['user_id'])

        {data: subjects}.to_json
      end

      api.post '/api/retrieve-subjects-by-text' do
        params = JSON.parse(request.body.read)

        subjects_list = Actions::RetrieveSubjectsByText.do(params['payload'])

        {data: subjects_list}.to_json
      end

      api.post '/api/retrieve-subjects-by-user-csv' do
        params = JSON.parse(request.body.read)

        user_id = params['user_id'] || 'asesora@gmail.com'

        counselings = Actions::RetrieveAllCounselingsByUser.by_user(user_id)

        { data: counselings }.to_json
      end

      api.post '/api/retrieve-subjects-by-domain-less-csv' do
        params = JSON.parse(request.body.read)

        user_id = params['user_id'] || 'asesora@gmail.com'

        counselings = Actions::RetrieveAllCounselingsByUser.by_domain_less(user_id)

        { data: counselings }.to_json
      end

      api.post '/api/retrieve-subjects-by-domain-csv' do
        params = JSON.parse(request.body.read)

        user_id = params['user_id'] || 'asesora@gmail.com'

        counselings = Actions::RetrieveAllCounselingsByUser.by_domain(user_id)

        { data: counselings }.to_json
      end

      api.post '/api/retrieve-subjects-less-my-domain-csv' do
        params = JSON.parse(request.body.read)

        user_id = params['user_id'] || 'asesora@gmail.com'

        counselings = Actions::RetrieveAllCounselingsByUser.less_my_domain(user_id)

        { data: counselings }.to_json
      end

      api.post '/api/retrieve-all-subjects-csv' do
        params = JSON.parse(request.body.read)

        user_id = params['user_id'] || 'asesora@gmail.com'

        counselings = Actions::RetrieveAllCounselingsByUser.all_subjects(user_id)

        { data: counselings }.to_json
      end
    end
  end
end
