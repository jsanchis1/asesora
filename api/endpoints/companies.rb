require_relative '../system/actions/update_company'
require_relative '../system/actions/retrieve_solicitudes'
require_relative '../system/actions/retrieve_company'
require_relative '../system/actions/count_company_in_solicitudes'

module Endpoints
  class Companies
    def self.define_endpoints(api)
      api.post '/api/update-company' do
        params = JSON.parse(request.body.read)
        data = {
            company_name: params['companyName'],
            company_cif: params['companyCif'],
            company_employees: params['companyEmployees'],
            company_cnae: params['companyCnae'],
            company_cp: params['companyCp'],
            company_id: params['companyId']
        }
        updated = Actions::UpdateCompany.do(data)
        return {}.to_json if updated.nil?

        updated.to_json
      end

      api.post '/api/company-matches' do
        params = JSON.parse(request.body.read)

        criteria = {
          name: params['companyName'],
          cnae: params['companyCnae']
        }
        user_id = params['user_id']
        companies = Actions::RetrieveSolicitudes.do_companies(criteria, user_id)

        {data: companies}.to_json
      end

      api.post '/api/duplicated-company' do
        params = JSON.parse(request.body.read)

        company = Actions::RetrieveCompany.do(cif: params['cif'])

        company.to_json
      end

      api.post '/api/count-company-in-solicitudes' do
        params = JSON.parse(request.body.read)

        times = Actions::CountCompanyInSolicitudes.do(cif: params['id'])

        times.to_json
      end
    end
  end
end
