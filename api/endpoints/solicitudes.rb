require_relative '../system/actions/create_solicitude'
require_relative '../system/actions/create_solicitude_from_subject'
require_relative '../system/actions/retrieve_solicitude'
require_relative '../system/actions/retrieve_solicitudes'
require_relative '../system/actions/update_solicitude'
require_relative '../system/actions/delete_solicitude'
require_relative '../system/actions/retrieve_cnae'
require_relative '../system/actions/retrieve_ccaa'
require_relative '../system/actions/retrieve_organizations_catalog'
require_relative '../system/actions/retrieve_close_reasons'
require_relative '../system/actions/create_subject'
require_relative '../system/actions/update_subject'
require_relative '../system/actions/close_subject'
require_relative '../system/actions/retrieve_subjects'
require_relative '../system/actions/retrieve_topics'
require_relative '../system/actions/retrieve_proposals'
require_relative '../system/actions/retrieve_source_formats'
require_relative '../system/actions/delete_subject'
require_relative '../system/actions/retrieve_all_solicitudes_by_user'
require_relative '../system/actions/retrieve_all_subjects_by_user'
require_relative '../system/actions/retrieve_all_subjects_by_domain'
require_relative '../system/actions/retrieve_all_counselings_by_user'
require_relative '../system/actions/retrieve_projects'
require_relative '../system/actions/remove_closed_status'
require_relative '../system/actions/update_catalog'
require_relative '../system/actions/add_to_catalog'
require_relative '../system/actions/authorize_email'


module Endpoints
  class Solicitudes
    def self.define_endpoints(api)
      api.post '/api/create-solicitude' do
        params = JSON.parse(request.body.read)
        data = {
          text:params['text'],
          name:params['applicantName'],
          surname:params['applicantSurname'],
          email:params['applicantEmail'],
          phonenumber:params['applicantPhonenumber'],
          ccaa:params['applicantCcaa'],
          id:params['applicantId'],
          date:params['date'],
          time:params['time'],
          source:params['source'],
          company_name:params['companyName'],
          company_cif:params['companyCif'],
          company_employees:params['companyEmployees'],
          company_cnae:params['companyCnae'],
          company_cp: params['companyCp'],
          company_id: params['companyId'],
          organization: params['organization'],
          subjects: params['subjects']
        }
        data[:user_id] = params['user_id'] || 'asesora@gmail.com'
        data[:domain] = params['domain'] ||'Domain'

        created = Actions::CreateSolicitude.do(data)
        created.to_json
      end

      api.post '/api/create-solicitude-from-subject' do
        params = JSON.parse(request.body.read)

        data = {
          text: params['text'],
          email: params['origin_email'],
          user_id: params['destination_email'],
          shared_origin: params['subject_id']
        }

        solicitude = Actions::CreateSolicitudeFromSubject.do(data)
        solicitude.to_json
      end

      api.post '/api/check-authorized-email' do
        params = JSON.parse(request.body.read)
        response = Actions::AuthorizeEmail.do_email(params)
        response.to_json
      end

      api.post '/api/retrieve-solicitude' do
        params = JSON.parse(request.body.read)

        solicitude = Actions::RetrieveSolicitude.do(id: params['id'])
        {data: solicitude}.to_json
      end

      api.post '/api/retrieve-solicitudes-by-domain' do
        params = JSON.parse(request.body.read)

        solicitudes = Actions::RetrieveSolicitudes.do_by_domain(user_id: params['user_id'])
        {data: solicitudes}.to_json
      end

      api.post '/api/retrieve-solicitudes' do
        retrieve_solicitudes = Actions::RetrieveSolicitudes.do()

        list_solicitudes = retrieve_solicitudes

        {data: list_solicitudes}.to_json
      end

      api.post '/api/update-solicitude' do
        params = JSON.parse(request.body.read)

        data = {
          date:params['date'],
          time:params['time'],
          text:params['text'],
          source:params['source'],
          id:params['applicantId'],
          company_cif:params['companyCif'],
          creation_moment:params['creation_moment'],
          company_id: params['companyId'],
          numeration: params['numeration'],
          organization: params['organization'],
          shared_origin: params['shared_origin']
        }
        data[:user_id] = params['user_id'] || 'asesora@gmail.com'
        data[:domain] = params['domain'] ||'Domain'

        updated = Actions::UpdateSolicitude.do(data)
        return {}.to_json if updated.nil?
        updated.to_json
      end

      api.post '/api/delete-solicitude' do
        params = JSON.parse(request.body.read)

        response = Actions::DeleteSolicitude.do(id: params['id'])
        return status 500 if response == "500"
        status 200
      end

      api.post '/api/cnae' do
        cnae_catalog = Actions::RetrieveCnae.do()

        {data: cnae_catalog}.to_json
      end

      api.post '/api/ccaa' do
        ccaa_catalog = Actions::RetrieveCcaa.do()

        {data: ccaa_catalog}.to_json
      end

      api.post '/api/organizations-catalog' do
        organizations_catalog = Actions::RetrieveOrganizationsCatalog.do()

        {data: organizations_catalog}.to_json
      end

      api.post '/api/source_formats' do
        source_formats_catalog = Actions::RetrieveSourceFormats.do()

        {data: source_formats_catalog}.to_json
      end

      api.post '/api/close-reasons' do
        close_reasons_catalog = Actions::RetrieveCloseReasons.do()

        {data: close_reasons_catalog}.to_json
      end

      api.post '/api/create-subject' do
        params = JSON.parse(request.body.read)
        data = {
          solicitude_id: params['solicitudeId'],
          proposal: params['proposal'],
          description: params['description'],
          analysis: params['analysis'],
          topics: params['topics'],
          created: params['created'],
          createdTime: params['createdTime'],
          project: params['project'],
          files: params['files']
        }

        solicitude_subject = Actions::CreateSubject.do(data)

        solicitude_subject.to_json
      end

      api.post '/api/update-subject' do
        params = JSON.parse(request.body.read)
puts params
        data = {
          id: params['subjectId'],
          solicitude_id: params['solicitudeId'],
          proposal: params['proposal'],
          description: params['description'],
          analysis: params['analysis'],
          topics: params['topics'],
          created: params['created'],
          createdTime: params['createdTime'],
          project: params['project'],
          numeration: params['numeration'],
          files: params['files']
        }

        updated = Actions::UpdateSubject.do(data)
        return {}.to_json if updated.nil?
        updated.to_json
      end

      api.post '/api/close-subject' do
        params = JSON.parse(request.body.read)

        data = {
          solicitude_id: params['solicitudeId'],
          id: params['subjectId'],
          proposal: params['proposal'],
          description: params['description'],
          analysis: params['analysis'],
          topics: params['topics'],
          reason: params['reason'],
          comments: params['comments'],
          closed: params['closed'],
          created: params['created'],
          createdTime: params['createdTime'],
          project: params['project'],
          numeration: params['numeration'],
          files: params['files']
        }

        updated = Actions::CloseSubject.do(data)

        return {}.to_json if updated.nil?
        updated.to_json
      end

      api.post '/api/retrieve-subjects' do
        params = JSON.parse(request.body.read)

        subjects = Actions::RetrieveSubjects.do(solicitude_id: params['solicitudeId'])
        {data: subjects}.to_json
      end

      api.post '/api/delete-subject' do
        params = JSON.parse(request.body.read)

        response = Actions::DeleteSubject.do(id: params['subjectId'])
        return status 500 if response == "500"
        status 200
        {data: {id: params['subjectId']}}.to_json
      end

      api.post '/api/retrieve-solicitudes-by-user' do
        params = JSON.parse(request.body.read)

        data = {
          user_id: params['user_id'] || 'asesora@gmail.com',
          domain: params['domain'] || 'domain'
        }

        solicitudes = Actions::RetrieveAllSolicitudesByUser.do(data)

        {data: solicitudes}.to_json
      end

      api.post '/api/retrieve-solicitudes-by-domain-less-mine' do
        params = JSON.parse(request.body.read)

        user = {
          user: params['user_id']
        }

        solicitudes = Actions::RetrieveSolicitudes.do_domain_less(user)

        {data: solicitudes}.to_json
      end

      api.post '/api/retrieve-subjects-by-user' do
        params = JSON.parse(request.body.read)

        data = {
          user_id: params['user_id'] || 'asesora@gmail.com',
          domain: params['domain'] || 'domain'
        }

        subjects = Actions::RetrieveAllSubjectsByUser.do(data)

        {data: subjects}.to_json
      end

      api.post '/api/topics' do
        topics_catalog = Actions::RetrieveTopics.do()
        {data: topics_catalog}.to_json
      end

      api.post '/api/proposals' do
        proposals_catalog = Actions::RetrieveProposals.do()
        {data: proposals_catalog}.to_json
      end

      api.post '/api/projects' do
        projects_catalog = Actions::RetrieveProjects.do()
        {data: projects_catalog}.to_json
      end

      api.post '/api/update-catalog' do
        params = JSON.parse(request.body.read)
        data = {
          new_value: params['newValue'],
          old_value: params['oldValue'],
          catalog: params['catalog']
        }

        updated_catalog = Actions::UpdateCatalog.do(data)

        {data: updated_catalog}.to_json
      end


      api.post '/api/add-item-to-catalog' do
        params = JSON.parse(request.body.read)

        data = {
          new_value: params['newValue'],
          catalog: params['catalog']
        }

        updated_catalog = Actions::AddItemToCatalog.do(data)

        {data: updated_catalog}.to_json
      end



      api.post '/api/retrieve-counselings-by-user' do
        params = JSON.parse(request.body.read)

        options = {
          user_id: params['user_id'] || 'asesora@gmail.com',
          domain: params['domain'] || 'domain'
        }

        counselings = Actions::RetrieveAllCounselingsByUser.do(options)

        { data: counselings }.to_json
      end

      api.post '/api/remove-closed-status' do
        params = JSON.parse(request.body.read)
        data = {
          subjectId: params['id']
        }

        subject = Actions::RemoveClosedStatus.do(data)

        return {}.to_json if subject.nil?
        subject.to_json
      end
    end
  end
end
