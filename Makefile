start:
	docker-compose up --build

down:
	docker-compose down

start-linux:
	docker-compose -f docker-compose.yml -f docker-compose-linux.yml up --build

build-app:
	docker-compose exec app npm run build

build-watch-app:
	docker-compose exec app npm run build-watch

run-test-api:
	docker-compose exec api rake test

run-test-app:
	docker-compose exec app npm run test-all

run-all-test:
	xhost local:root
	docker-compose exec app npm run test-all
	docker-compose exec api bundle exec rake test
	docker-compose exec app npm run build-e2e
	docker-compose exec e2e npm run test

run-test-e2e:
	xhost local:root
	docker-compose exec app npm run build-e2e
	docker-compose exec e2e npm run test

run-test-e2e-graphic:
	xhost local:root
	docker-compose exec app npm run build-e2e
	docker-compose exec e2e npm run test-graphic

load-fixtures:
	curl -i -d "" http://localhost:4567/fixtures/pristine
